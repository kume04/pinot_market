import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _4276dce4 = () => interopDefault(import('..\\pages\\category.vue' /* webpackChunkName: "pages/category" */))
const _395faefd = () => interopDefault(import('..\\pages\\code.vue' /* webpackChunkName: "pages/code" */))
const _1ca69dfb = () => interopDefault(import('..\\pages\\common.vue' /* webpackChunkName: "pages/common" */))
const _a6271f6e = () => interopDefault(import('..\\pages\\complete.vue' /* webpackChunkName: "pages/complete" */))
const _159b0b12 = () => interopDefault(import('..\\pages\\complete_coopang.vue' /* webpackChunkName: "pages/complete_coopang" */))
const _5454d923 = () => interopDefault(import('..\\pages\\cur_delivery.vue' /* webpackChunkName: "pages/cur_delivery" */))
const _41c73c52 = () => interopDefault(import('..\\pages\\cur_facility.vue' /* webpackChunkName: "pages/cur_facility" */))
const _01d94224 = () => interopDefault(import('..\\pages\\cur_warehouse.vue' /* webpackChunkName: "pages/cur_warehouse" */))
const _15c8e18d = () => interopDefault(import('..\\pages\\custom_menu.vue' /* webpackChunkName: "pages/custom_menu" */))
const _82e2fd98 = () => interopDefault(import('..\\pages\\dashboard.vue' /* webpackChunkName: "pages/dashboard" */))
const _55046c72 = () => interopDefault(import('..\\pages\\dashboard_schedule.vue' /* webpackChunkName: "pages/dashboard_schedule" */))
const _3f3a19bb = () => interopDefault(import('..\\pages\\delete.vue' /* webpackChunkName: "pages/delete" */))
const _23faf4f8 = () => interopDefault(import('..\\pages\\delivery.vue' /* webpackChunkName: "pages/delivery" */))
const _704f89ba = () => interopDefault(import('..\\pages\\dev_log.vue' /* webpackChunkName: "pages/dev_log" */))
const _5f0f94cb = () => interopDefault(import('..\\pages\\employer.vue' /* webpackChunkName: "pages/employer" */))
const _49162e9a = () => interopDefault(import('..\\pages\\facility.vue' /* webpackChunkName: "pages/facility" */))
const _5ec930e7 = () => interopDefault(import('..\\pages\\inquiry.vue' /* webpackChunkName: "pages/inquiry" */))
const _99690dba = () => interopDefault(import('..\\pages\\item.vue' /* webpackChunkName: "pages/item" */))
const _142a2994 = () => interopDefault(import('..\\pages\\label.vue' /* webpackChunkName: "pages/label" */))
const _7c397e76 = () => interopDefault(import('..\\pages\\location.vue' /* webpackChunkName: "pages/location" */))
const _074e87e4 = () => interopDefault(import('..\\pages\\log.vue' /* webpackChunkName: "pages/log" */))
const _eb366730 = () => interopDefault(import('..\\pages\\notice.vue' /* webpackChunkName: "pages/notice" */))
const _27a01b68 = () => interopDefault(import('..\\pages\\partner.vue' /* webpackChunkName: "pages/partner" */))
const _94fc0d8e = () => interopDefault(import('..\\pages\\project.vue' /* webpackChunkName: "pages/project" */))
const _182fd932 = () => interopDefault(import('..\\pages\\project_coopang.vue' /* webpackChunkName: "pages/project_coopang" */))
const _561fc247 = () => interopDefault(import('..\\pages\\project_detail.vue' /* webpackChunkName: "pages/project_detail" */))
const _353ce55e = () => interopDefault(import('..\\pages\\project_detail copy.vue' /* webpackChunkName: "pages/project_detail copy" */))
const _8179a940 = () => interopDefault(import('..\\pages\\project_mng.vue' /* webpackChunkName: "pages/project_mng" */))
const _be77a2e4 = () => interopDefault(import('..\\pages\\project_mng_coopang.vue' /* webpackChunkName: "pages/project_mng_coopang" */))
const _347d94a8 = () => interopDefault(import('..\\pages\\project copy.vue' /* webpackChunkName: "pages/project copy" */))
const _89cb4714 = () => interopDefault(import('..\\pages\\question.vue' /* webpackChunkName: "pages/question" */))
const _e3464cfe = () => interopDefault(import('..\\pages\\quick_menu.vue' /* webpackChunkName: "pages/quick_menu" */))
const _3067ebce = () => interopDefault(import('..\\pages\\userList.vue' /* webpackChunkName: "pages/userList" */))
const _5966709c = () => interopDefault(import('..\\pages\\userRightControl.vue' /* webpackChunkName: "pages/userRightControl" */))
const _c43dbc3a = () => interopDefault(import('..\\pages\\warehouse.vue' /* webpackChunkName: "pages/warehouse" */))
const _00066a90 = () => interopDefault(import('..\\pages\\warehouse_etc.vue' /* webpackChunkName: "pages/warehouse_etc" */))
const _2e1b6ec1 = () => interopDefault(import('..\\pages\\warehouse_location.vue' /* webpackChunkName: "pages/warehouse_location" */))
const _5c2a7ada = () => interopDefault(import('..\\pages\\warehouse_product.vue' /* webpackChunkName: "pages/warehouse_product" */))
const _0841b472 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/category",
    component: _4276dce4,
    name: "category"
  }, {
    path: "/code",
    component: _395faefd,
    name: "code"
  }, {
    path: "/common",
    component: _1ca69dfb,
    name: "common"
  }, {
    path: "/complete",
    component: _a6271f6e,
    name: "complete"
  }, {
    path: "/complete_coopang",
    component: _159b0b12,
    name: "complete_coopang"
  }, {
    path: "/cur_delivery",
    component: _5454d923,
    name: "cur_delivery"
  }, {
    path: "/cur_facility",
    component: _41c73c52,
    name: "cur_facility"
  }, {
    path: "/cur_warehouse",
    component: _01d94224,
    name: "cur_warehouse"
  }, {
    path: "/custom_menu",
    component: _15c8e18d,
    name: "custom_menu"
  }, {
    path: "/dashboard",
    component: _82e2fd98,
    name: "dashboard"
  }, {
    path: "/dashboard_schedule",
    component: _55046c72,
    name: "dashboard_schedule"
  }, {
    path: "/delete",
    component: _3f3a19bb,
    name: "delete"
  }, {
    path: "/delivery",
    component: _23faf4f8,
    name: "delivery"
  }, {
    path: "/dev_log",
    component: _704f89ba,
    name: "dev_log"
  }, {
    path: "/employer",
    component: _5f0f94cb,
    name: "employer"
  }, {
    path: "/facility",
    component: _49162e9a,
    name: "facility"
  }, {
    path: "/inquiry",
    component: _5ec930e7,
    name: "inquiry"
  }, {
    path: "/item",
    component: _99690dba,
    name: "item"
  }, {
    path: "/label",
    component: _142a2994,
    name: "label"
  }, {
    path: "/location",
    component: _7c397e76,
    name: "location"
  }, {
    path: "/log",
    component: _074e87e4,
    name: "log"
  }, {
    path: "/notice",
    component: _eb366730,
    name: "notice"
  }, {
    path: "/partner",
    component: _27a01b68,
    name: "partner"
  }, {
    path: "/project",
    component: _94fc0d8e,
    name: "project"
  }, {
    path: "/project_coopang",
    component: _182fd932,
    name: "project_coopang"
  }, {
    path: "/project_detail",
    component: _561fc247,
    name: "project_detail"
  }, {
    path: "/project_detail%20copy",
    component: _353ce55e,
    name: "project_detail copy"
  }, {
    path: "/project_mng",
    component: _8179a940,
    name: "project_mng"
  }, {
    path: "/project_mng_coopang",
    component: _be77a2e4,
    name: "project_mng_coopang"
  }, {
    path: "/project%20copy",
    component: _347d94a8,
    name: "project copy"
  }, {
    path: "/question",
    component: _89cb4714,
    name: "question"
  }, {
    path: "/quick_menu",
    component: _e3464cfe,
    name: "quick_menu"
  }, {
    path: "/userList",
    component: _3067ebce,
    name: "userList"
  }, {
    path: "/userRightControl",
    component: _5966709c,
    name: "userRightControl"
  }, {
    path: "/warehouse",
    component: _c43dbc3a,
    name: "warehouse"
  }, {
    path: "/warehouse_etc",
    component: _00066a90,
    name: "warehouse_etc"
  }, {
    path: "/warehouse_location",
    component: _2e1b6ec1,
    name: "warehouse_location"
  }, {
    path: "/warehouse_product",
    component: _5c2a7ada,
    name: "warehouse_product"
  }, {
    path: "/",
    component: _0841b472,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
