const common_code = {
  data () {
    return {
      common_code: [],     
      
    }
  },
  mounted(){
    this.$axios.get(this.$URL.COMMON_CODE).then(res=>{
      this.common_code = res.data
    })
  }
}

export default common_code;