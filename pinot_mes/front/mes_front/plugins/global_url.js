import Vue from 'vue'
export default ({ app }, inject) => {
    console.log(process.env.NODE_ENV)
    if( process.env.NODE_ENV === "development" ){
        inject('URL', Vue.observable({ 
            LOG : "http://localhost:5010/api/log",
            LOGIN : 'http://localhost:5010/auth/login' ,
            LOGOUT : 'http://localhost:5010/auth/logout' ,
            REGISTER : "http://localhost:5010/auth/register",
            USER_LIST : "http://localhost:5010/api/users",
            USER_RIGHT : "http://localhost:5010/api/users/rights",
            USER : "http://localhost:5010/api/users", 
            COMMON_CODE : "http://localhost:5010/api/common_code", 
            QUICK_MENU : "http://localhost:5010/api/quick_menus",             
            PROJECT: "http://localhost:5001/api/projects", 
            INQUIRY: "http://localhost:5001/api/inquiries", 
            PARTNER: "http://localhost:5002/api/partners", 
            LOCATION: "http://localhost:5003/api/locations", 
            ITEM: "http://localhost:5003/api/items", 
            ITEM_CATEGORY: "http://localhost:5003/api/item_categories",
            TRANSACTION: "http://localhost:5003/api/transactions",  
            QUESTION: "http://localhost:5004/api/questions",  
            NOTICE: "http://localhost:5006/api/notices",  
            FACILITY: "http://localhost:5009/api/facilities",  
        }))
    }else{
        inject('URL', Vue.observable({ 
            LOG : "http://13.124.52.145:5010/api/log",
            LOGIN : 'http://13.124.52.145:5010/auth/login' ,
            LOGOUT : 'http://13.124.52.145:5010/auth/logout' ,
            REGISTER : "http://13.124.52.145:5010/auth/register",
            USER_LIST : "http://13.124.52.145:5010/api/users",
            USER_RIGHT : "http://13.124.52.145:5010/api/users/rights",
            USER : "http://13.124.52.145:5010/api/users", 
            COMMON_CODE : "http://13.124.52.145:5010/api/common_code", 
            QUICK_MENU : "http://13.124.52.145:5010/api/quick_menus", 
            PROJECT: "http://13.124.52.145:5001/api/projects", 
            INQUIRY: "http://13.124.52.145:5001/api/inquiries", 
            PARTNER: "http://13.124.52.145:5002/api/partners", 
            LOCATION: "http://13.124.52.145:5003/api/locations", 
            ITEM: "http://13.124.52.145:5003/api/items", 
            ITEM_CATEGORY: "http://13.124.52.145:5003/api/item_categories",
            TRANSACTION: "http://13.124.52.145:5003/api/transactions",   
            QUESTION: "http://13.124.52.145:5004/api/questions",  
            NOTICE: "http://13.124.52.145:5006/api/notices",  
            FACILITY: "http://13.124.52.145:5009/api/facilities",  
        }))
    }
    
}
