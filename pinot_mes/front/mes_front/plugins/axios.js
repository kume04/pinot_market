import Vue from 'vue'

function getCookie(cookieName){
    let cookieValue=null;
    if(document.cookie){
        const array=document.cookie.split((escape(cookieName)+'='));
        if(array.length >= 2){
            const arraySub=array[1].split(';');
            cookieValue=unescape(arraySub[0]);
        }
    }
    return cookieValue;
}
export default function ({ store, app: { $axios }, redirect }) {
    $axios.onRequest((config) => {     
      // check if the user is authenticated
      if (getCookie("platform_token")) {
        // set the Authorization header using the access token
        config.headers.Authorization = 'Bearer ' + getCookie("platform_token")
      }  
      return config
    })
    $axios.onError(e=>{
        try{
            const code = e.response.status
            if( code === 403){
                alert("유효하지 않은 권한 입니다. 로그인페이지로 이동합니다.")
                // location.href="/system_login"
            }
        }
        catch(e){
            
        }
        
    })
  }


