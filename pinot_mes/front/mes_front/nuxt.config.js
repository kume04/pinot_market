export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: '피노 스마트 관리시스템',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'ant-design-vue/dist/antd.css',     
    'quill/dist/quill.core.css',
    // for snow theme
    'quill/dist/quill.snow.css',
    // for bubble theme
    'quill/dist/quill.bubble.css',
    'static/global.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/antd-ui',
    { src: '~plugins/quill', ssr: false },
    '@/plugins/axios','@/plugins/global_url',
    { src: '~/plugins/underscore', ssr: false },
    { src: '~/plugins/tui-grid',    mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: 'AIzaSyDIlVokHX5dcoB1THNZqHintsQVb5ackio',
          authDomain: 'onthegoods-7ba81.firebaseapp.com',
          projectId: 'onthegoods-7ba81',
          storageBucket: 'onthegoods-7ba81.appspot.com',
          messagingSenderId: '485542521528',
          appId: '1:485542521528:web:d07a5e1174200ace4c224d',
          measurementId: 'G-RR5NZC30T2'
        },
        services: {
          auth: false, // Just as example. Can be any other service.
          database: true,
          messaging: true,
          storage:true
        }
      }
    ]
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  }, 

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config,ctx){ },
    
    vendor: ['@toast-ui/vue-grid'],
    
  
  },
  dev: process.env.NODE_ENV !== 'production',
  server: {
        port: 80,
        host: '0.0.0.0'
  },
  // router:{
  //   base: "/system_login"
  // }
}
 