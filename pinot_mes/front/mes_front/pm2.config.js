module.exports = {
    apps : [{
      name   : "smart_factory",
      script : "npm",
      args: "start",
      env_production: {
        PORT: 80,
        NODE_ENV: "production",
        HOST: '0.0.0.0',
      }
    }]
  }