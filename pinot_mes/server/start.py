import subprocess, sys

# window case
app_0 = subprocess.Popen([
        "powershell.exe",
        "Stop-Process -Id (Get-NetTCPConnection -LocalPort 5000).OwningProcess -Force \n",      
        "Stop-Process -Id (Get-NetTCPConnection -LocalPort 5001).OwningProcess -Force \n",       
    ], stdout=sys.stdout)
app_0.communicate()