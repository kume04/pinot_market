from flask_restx import Api
from flask import Blueprint
from .notice import ns as NoticeNamespace


blueprint = Blueprint(
    "api",
    __name__,
    url_prefix="/api"
)

api = Api(
    blueprint,
    title="Notice",
    version="1.0",
    doc="/docs",
    description="Notice application"
)


# TODO: add namespace to Blueprint
api.add_namespace(NoticeNamespace)