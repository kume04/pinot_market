from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from Notice.models.notice import Notice as NoticeModel

ns = Namespace(
    "notices",
    description="공지사항 API"
)
# questions 조회
notice = ns.model("notice", {    
    "id" : fields.Integer(required=True, description="공지사항 아이디"),
    "title" :  fields.String(description="공지사항 제목"),  
    "created_by": fields.String(required=False, description="최초등록자"),
    "created_at": fields.String(description="최초등록일자"),
    "content": fields.String(description="공지사항 내용")
})

# 거래처 입력
post_parser = reqparse.RequestParser()
post_parser.add_argument("id", type=int, required=False,  help=" 아이디")

# /api/partners
@ns.route("")
class PartnerList(Resource):
    @ns.marshal_list_with(notice, skip_none=True)
    def get(self):
        ''' 공지사항 복수 조회 '''
        
        data = NoticeModel.query.filter(NoticeModel.is_deleted==False).order_by(NoticeModel.id.desc()).all()
        
        return data
    
    @ns.marshal_list_with(notice, skip_none=True)
    def post(self):
        ''' 문의 생성 '''
        notice_data = NoticeModel(**request.get_json())
        g.db.add(notice_data)
        g.db.commit()
        print(request.get_json())
        return  201


# /api/partners/1
@ns.route("/<int:id>")
@ns.param("id", "거래처 아이디")
class Partner(Resource):
    @ns.marshal_list_with(notice, skip_none=True)
    def get(self, id):
        ''' 문의 단수 조회 '''
        data = NoticeModel.query.get_or_404(id)
        return data

    
    @ns.marshal_list_with(notice, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 문의  info update '''
        # 업데이트에 성공하면 업데이트 된 정보 반환
        print(request.get_json())
        data = NoticeModel.query.get_or_404(id)
        print(data)
        origin_data = request.get_json()
        for item in origin_data.keys():
            print(item)
            print( origin_data[item])
            
            setattr(data, item, origin_data[item])
        print(data.__dict__)
        obj = g.db.merge(data)
        g.db.object_session(obj)
        g.db.commit()
        return 200

    
    # @ns.expect(post_parser)
    def delete(self, id):
        ''' 거래처 Safe delete ''' 
        data = NoticeModel.query.filter(NoticeModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return 200