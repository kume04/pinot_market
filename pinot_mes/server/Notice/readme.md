# Flask Partnership Application
## Things Todo
- [ ] Partner Registration
- [ ] Partner Information Update
- [ ] Partner Delete(Safe Delete)


# Prequisite
## Windows
```
#in development mode
set FLASK_ENV=development #Auto Reload
set FLASK_APP=Partnership 
```
```
#in production mode
set FLASK_ENV=production 
set FLASK_APP=Partnership 
```

## MAC
```
confirm to psycopg2  and  brew install psycopg2-binary
brew install postgresql
export LDFLAGS="-L/usr/local/opt/openssl/lib"
export CPPFLAGS="-I/usr/local/opt/openssl/include"
pip3 install psycopg2
and 
pip3 install -r requirements.txt
```
```
#in development mode
export FLASK_ENV=development FLASK_APP=Partnership
```
```
#in production mode
export FLASK_ENV=production FLASK_APP=Partnership 
```