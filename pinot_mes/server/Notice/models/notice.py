from Notice import db
from sqlalchemy import func

class Notice(db.Model):    
    id =  db.Column(db.Integer ,primary_key=True) 
    title =   db.Column(db.String, nullable=True )
    is_deleted = db.Column(db.Boolean , nullable=True , default=False)
    created_by = db.Column(db.String, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    content =  db.Column(db.Text, nullable=True )
 
