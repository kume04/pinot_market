from sqlalchemy import func
from Question import db
class Question(db.Model):    
    id =  db.Column(db.Integer ,primary_key=True) 
    title =   db.Column(db.String, nullable=True )
    attached_file_0 = db.Column(db.String, nullable=True)
    status =   db.Column(db.String, nullable=True )
    answered_at =  db.Column(db.DateTime, nullable=True)
    co_name = db.Column(db.String, nullable=True)
    person = db.Column(db.String, nullable=True)
    contact = db.Column(db.String, nullable=True)
    cellphone = db.Column(db.String, nullable=True)
    email = db.Column(db.String, nullable=True)
    fax = db.Column(db.String, nullable=True)
    is_deleted = db.Column(db.Boolean , nullable=True , default=False)
    note = db.Column(db.String, nullable=True)
    created_by = db.Column(db.String, nullable=True)

    created_at = db.Column(db.DateTime, nullable=True)
    content =  db.Column(db.Text, nullable=True )
 
