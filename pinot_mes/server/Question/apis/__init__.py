from flask_restx import Api
from flask import Blueprint
from .question import ns as QuestionNamespace


blueprint = Blueprint(
    "api",
    __name__,
    url_prefix="/api"
)

api = Api(
    blueprint,
    title="Question",
    version="1.0",
    doc="/docs",
    description="Question application"
)


# TODO: add namespace to Blueprint
api.add_namespace(QuestionNamespace)