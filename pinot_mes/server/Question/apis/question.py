from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from Question.models.question import Question as QuestionModel

ns = Namespace(
    "questions",
    description="문의 API"
)
# questions 조회
question = ns.model("question", {    
    "id" : fields.Integer(required=True, description="문의 아이디"),
    "title" :  fields.String(description="문의 제목"),
    "attached_file_0" :  fields.String(required=False,description="문의 첨부 파일"),
    "status" :  fields.String(required=False,  description="문의 진행 상태"),
    "answered_at" : fields.String(required=False,description="문의 응답일"),
    "co_name": fields.String(required=False, description="회사"),
    "person": fields.String(required=False, description="담당자"),
    "contact": fields.String(required=False, description="연락처"),
    "cellphone": fields.String(required=False, description="휴대폰"),
    "email": fields.String(required=False, description="이메일"),
    "fax": fields.String(required=False, description="팩스"),
    "is_deleted": fields.Boolean(required=True, description="삭제여부"),
    "note": fields.String(required=False, description="비고"),
    "created_by": fields.String(required=False, description="최초등록자"),
    "created_at": fields.String(description="최초등록일자"),
    "content": fields.String(description="문의 내용")
})

# 거래처 입력
post_parser = reqparse.RequestParser()
post_parser.add_argument("id", type=int, required=False,  help=" 아이디")

# /api/partners
@ns.route("")
class PartnerList(Resource):
    @ns.marshal_list_with(question, skip_none=True)
    def get(self):
        ''' 문의 복수 조회 '''
        
        data = QuestionModel.query.filter(QuestionModel.is_deleted==False).order_by(QuestionModel.id.desc()).all()
        
        return data
    
    @ns.marshal_list_with(question, skip_none=True)
    def post(self):
        ''' 문의 생성 '''
        question_data = QuestionModel(**request.get_json())
        g.db.add(question_data)
        g.db.commit()
        print(request.get_json())
        return  201


# /api/partners/1
@ns.route("/<int:id>")
@ns.param("id", "거래처 아이디")
class Partner(Resource):
    @ns.marshal_list_with(question, skip_none=True)
    def get(self, id):
        ''' 문의 단수 조회 '''
        data = QuestionModel.query.get_or_404(id)
        return data

    
    @ns.marshal_list_with(question, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 문의  info update '''
        # 업데이트에 성공하면 업데이트 된 정보 반환
        print(request.get_json())
        data = QuestionModel.query.get_or_404(id)
        print(data)
        origin_data = request.get_json()
        for item in origin_data.keys():
            print(item)
            print( origin_data[item])
            
            setattr(data, item, origin_data[item])
        print(data.__dict__)
        obj = g.db.merge(data)
        g.db.object_session(obj)
        g.db.commit()
        return 200

    
    # @ns.expect(post_parser)
    def delete(self, id):
        ''' 거래처 Safe delete '''
        data = QuestionModel.query.filter(QuestionModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return 200