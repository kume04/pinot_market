from flask import Blueprint, render_template, redirect, flash, url_for, session, request, g
from flask import jsonify, make_response
from AuthAndControl.forms.auth_form import LoginForm, RegisterForm
from AuthAndControl.models.user import User as UserModel
from AuthAndControl.models.log import Log as LogModel
from werkzeug import security
import jwt
import io, base64
from PIL import Image
import random, time
import datetime 
import boto3
import io, os

NAME = "auth"
bp = Blueprint(NAME, __name__, url_prefix="/auth")

@bp.before_app_request  # app전체에 전달
def before_app_request():
    g.user = None
    user_id = session.get("user_id")
    if user_id:
        user = UserModel.fine_one_by_user_id(user_id)
        if user:
            g.user = user
        else:
            session.pop("user_id", None)

@bp.route("/login", methods=["POST"])
def login():
    form = LoginForm()
    # POST, validate OK!

    if form.validate_on_submit():
        user_id = form.data.get("user_id")
        password = form.data.get("password")
        user = UserModel.fine_one_by_user_id(user_id)    
        if user:
            if not security.check_password_hash(user.password.strip(), password): #  FIXME: .strip =>  postgresSQL 에서 왜 이상하게 비밀번호에 공백이 붙는거지.
                return make_response(jsonify({"message":"입력 정보가 정확하지 않습니다."}), 400)
            else:
                session["user_id"] = user.user_id
                log = LogModel(
                    user_name = user.user_name,
                    user_type = "회원 로그인",
                    etc = request.user_agent.string,                    
                )
                print(log)
                g.db.add(log)
                g.db.commit() 
                return make_response(
                    jsonify({"message":"로그인 성공",  "user_id": user.id,
                        'Authorization': jwt.encode({'user_name': user.user_name}, 
                        "secret", algorithm="HS256").decode("UTF-8"),}), 
                    200)
        else:
            return make_response(jsonify({"message":"일치하는 아이디가  없습니다."}), 400)
    else:
        return make_response(jsonify({"message":"입력 정보가 정확하지 않습니다."}), 400)


@bp.route("/logout")
def logout():
    session.pop("user_id", None)
    user = UserModel.query.filter(UserModel.id==int(request.args.get("id"))).first()
    log = LogModel(
        user_name = user.user_name,
        user_type = "회원 로그아웃",
        etc = request.user_agent.string,                    
    )
    print(log)
    g.db.add(log)
    g.db.commit() 
    return ""




@bp.route("/test_base_64", methods=["POST"])
def test_base_64():
    temp_name =  str(datetime.datetime.now().timestamp()).split(".")[0]+"."+request.get_json()["type"]
    img = Image.open(io.BytesIO(base64.decodebytes(bytes(request.get_json()["data"], "utf-8"))))
    img.save(temp_name)
    pil_image =  Image.open(temp_name)
    in_mem_file = io.BytesIO()
    pil_image.save(in_mem_file, format=pil_image.format)
    in_mem_file.seek(0)
    s3 = boto3.client('s3',	
                aws_access_key_id = "AKIAY4U2LMJHWN2L2D6D",
            	aws_secret_access_key = "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")
    s3.put_object(
	    Bucket = "pinot-mes",
    	Body = in_mem_file,
    	Key = "/mes/project/"+temp_name,
    	ContentType = "image/"+request.get_json()["type"])
    os.remove(temp_name)
    return  make_response(jsonify({"data":"https://pinot-mes.s3.ap-northeast-2.amazonaws.com//mes/project/"+temp_name}), 200) 