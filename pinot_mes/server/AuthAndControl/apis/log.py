from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from AuthAndControl.models.log import Log  as CompanyModel

ns = Namespace(
    "log",
    description="로깅 api"
)

log =  ns.model("log", {
    "id": fields.Integer(required=True, description="id"),
    "user_name": fields.String(required=False, ),
    "user_type": fields.String(required=False, ),
    "etc": fields.String(required=False, ),
    "created_at": fields.String(description="최초등록일자"),
  
})


@ns.route("")
class CompanyList(Resource):
    @ns.marshal_list_with(log, skip_none=True)
    def get(self):
        ''' 입점사 복수 조회 '''
        data = CompanyModel.query.order_by( CompanyModel.id.desc() ).all()
        return data
 