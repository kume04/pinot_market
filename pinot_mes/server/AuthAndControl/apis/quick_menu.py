import re
from flask import g
from flask.globals import request
from flask_restx import Namespace, Resource, fields, Model
from AuthAndControl.models.quick_menu import QuickMenu as QuickMenuModel
# from AuthAndControl.models.quick_menu import QuickMenu as QuickMenuModel

from flask import jsonify, make_response
from werkzeug import security
from flask import abort
import jwt
from .auth_decorators import login_required
from flask_restx import reqparse

ns = Namespace(
    "quick_menus",
    description="메뉴 관련 API"
)

quick_menu = ns.model("quick_menu", {
    "id": fields.Integer(required=True, description="메뉴 고유 번호"),
    "menu_name": fields.String(reqired=False, description="메뉴 이름"),
    "menu_url": fields.String(reqired=False, description="메뉴 url"),
    "index": fields.Integer( required=False, default=0),
    "is_deleted": fields.Boolean(required=False),
    "is_external": fields.Boolean(required=False),
    "created_at": fields.String(required=False) 

})
parser = reqparse.RequestParser()
parser.add_argument("id", type=int)

# /api/quick_menus
@ns.route("")
class UserList(Resource):
    @ns.marshal_list_with(quick_menu, skip_none=True)
    # @login_required
    def get(self):
        ''' 메뉴 복수 조회 '''
        data = QuickMenuModel.query.filter(QuickMenuModel.is_deleted==False).order_by(QuickMenuModel.index.asc()).all()
        return data


    @ns.doc(responses={200: '메뉴가 등록되었습니다.'})
    @ns.doc(responses={400: '입력 정보가 올바르지 않습니다.'})
    # @login_required
    def post(self):
        item = QuickMenuModel()
        print(request.get_json())
        for key in request.get_json():
            print(key)
            print(request.get_json()[key])
            setattr(item, key, request.get_json()[key])
        g.db.add(item)
        g.db.commit()
        return 201                 
           

# /api/quick_menus/1
@ns.route("/<int:id>")
@ns.param("id", "메뉴 고유 번호")
class User(Resource):    
    @ns.marshal_list_with(quick_menu, skip_none=True)
    # @login_required
    def get(self, id):
        ''' 메뉴 단수 조회 '''     
        data = QuickMenuModel.query.get_or_404(id)
        return data
    
    @ns.marshal_list_with(quick_menu, skip_none=True)
    # @login_required
    def put(self,id):
        ''' 메뉴 information update '''
        # 업데이트에 성공하였으면 업데이트된 정보 반환
        data = QuickMenuModel.query.get_or_404(id)
        for item in request.form:
            setattr(data, item, request.form[item])
        g.db.commit()
        return data
    

    @ns.marshal_list_with(quick_menu, skip_none=True)
    # @login_required
    def delete(self,id):
        ''' 메뉴 Safe delete '''
        model = QuickMenuModel.query.filter( QuickMenuModel.id==id ).first()
        model.is_deleted = True
        g.db.commit()       
        return model
    
