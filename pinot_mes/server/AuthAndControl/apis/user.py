import re
from flask import g
from flask.globals import request
from flask_restx import Namespace, Resource, fields, Model
from AuthAndControl.models.user import User as UserModel
from AuthAndControl.forms.auth_form import LoginForm, RegisterForm
from flask import jsonify, make_response
from werkzeug import security
from flask import abort
import jwt
from .auth_decorators import login_required
from flask_restx import reqparse

ns = Namespace(
    "users",
    description="유저 관련 API"
)

user = ns.model("user", {
    "id": fields.Integer(required=True, description="유저 고유 번호"),
    "user_id": fields.String(required=True, description="유저 아이디"),
    "user_name": fields.String(required=True, description="유저 이름"),
    "user_address": fields.String(required=True, description="유저 주소"),
    "user_type" : fields.String(description="유저 유형"),
    "partner_name" : fields.String(description="거래처 이름"),
    "user_email" : fields.String(required=True, description="유저 이메일"),
    "user_phone" : fields.String(required=True, description="유저 연락처"),
    "etc" : fields.String( description="기타"),
    "department_code" : fields.String( description="부서")
    # password 는 화면에 뿌려줄 필요가 없으므로 
})
right_model = ns.model('user right', {
'question': fields.Boolean,
'project_mng': fields.Boolean,
'delivery': fields.Boolean,
'project_detail': fields.Boolean,
'complete': fields.Boolean,
'warehouse_etc': fields.Boolean,
'warehouse_product': fields.Boolean,
'item': fields.Boolean,
'partner': fields.Boolean,
'facility': fields.Boolean,
'location': fields.Boolean,
'label': fields.Boolean,
'code': fields.Boolean,
'notice': fields.Boolean,
'userList': fields.Boolean,
'userRightControl': fields.Boolean,
'menu': fields.Boolean,
'log': fields.Boolean,
'project': fields.Boolean,
'cur_delivery': fields.Boolean,
'cur_facility': fields.Boolean,
'cur_warehouse': fields.Boolean,
 })


user_right = ns.model("user", {
    "id": fields.Integer(required=True, description="유저 고유 번호"),
    "user_id": fields.String(required=True, description="유저 아이디"),
    "user_name": fields.String(required=True, description="유저 이름"),
    "user_address": fields.String(required=True, description="유저 주소"),
    "user_type" : fields.String(description="유저 유형"),
    "partner_name" : fields.String(description="거래처 이름"),
    "user_email" : fields.String(required=True, description="유저 이메일"),
    "user_phone" : fields.String(required=True, description="유저 연락처"),
    "etc" : fields.String( description="기타"),
    "department_code" : fields.String( description="부서"),
    "right": fields.Nested(right_model, required=False, description="유저 권한"),
    # password 는 화면에 뿌려줄 필요가 없으므로 
})

parser = reqparse.RequestParser()
parser.add_argument("id", type=int)

# /api/users
@ns.route("")
class UserList(Resource):
    @ns.marshal_list_with(user, skip_none=True)
    # @login_required
    def get(self):
        ''' 유저 복수 조회 '''
        data = UserModel.query.filter(UserModel.is_deleted!=True).all()
        return data

    # 유저를 새로 등록한 경우에는  mashaling을 할필요가 없음
    # TODO: MASHALING 이 뭔지 의미 체크
    # @ns.marshal_list_with(user, skip_none=True)
    @ns.doc(responses={200: '회원등록되었습니다.'})
    @ns.doc(responses={400: '입력 정보가 올바르지 않습니다.'})
    # @login_required
    def post(self):
        ''' 유저 등록 '''        
        #Copy and modify user Registration
        form = RegisterForm()
        print(request.data)

        if form.validate_on_submit(): # FIXME: 여기 뭔가 이상한것 같음. 밸리데이션을 해줬는데 다시 입력을 해줘야 하나?

            user_id = form.data.get("user_id")
            user_name = form.data.get("user_name")
            password = form.data.get("password")           
            user_type = form.data.get("user_type")
            partner_name = form.data.get("partner_name")
            user_email = form.data.get("user_email")
            user_phone = form.data.get("user_phone")
            user_address = form.data.get("user_address")
            user = UserModel.fine_one_by_user_id(user_id)
            if user:
                abort(409, '이미 등록되어있는 아이디 입니다.') # FIXME: ENGLISH TRANSLATION              
            else:
                usermodel = UserModel(
                        user_id = user_id,
                        user_name = user_name,
                        password = security.generate_password_hash(password), 
                        user_type = user_type , partner_name=partner_name, user_email=user_email, user_phone=user_phone,
                        user_address = user_address,
                        right = {
                                    'question': False,
                                    'project_mng': False,
                                    'delivery': False,
                                    'project_detail': False,
                                    'complete': False,
                                    'warehouse_etc': False,
                                    'warehouse_product': False,
                                    'item': False,
                                    'partner': False,
                                    'facility': False,
                                    'location': False,
                                    'label': False,
                                    'code': False,
                                    'notice': False,
                                    'userList': False,
                                    'userRightControl': False,
                                    'menu': False,
                                    'log': False,
                                    'project': False,
                                    'cur_delivery': False,
                                    'cur_facility': False,
                                    'cur_warehouse': False,
                                }
                    )              
                g.db.add(usermodel)
                g.db.commit()       
                # TODO: 등록이 되면 jwt 토큰 첨부하여 반환
                return {
                    'Authorization': jwt.encode({'user_name': user_name}, "secret", algorithm="HS256").decode("UTF-8"),
                    "en":{"message":"register success."}, "ko":{"message":"회원등록되었습니다."}  # str으로 반환하여 return
                }, 200                
        else:
            abort(400, '입력 정보가 올바르지 않습니다.') # FIXME: ENGLISH TRANSLATION                   
           

# /api/users/1
@ns.route("/<int:id>")
@ns.param("id", "유저 고유 번호")
class User(Resource):    
    @ns.marshal_list_with(user_right, skip_none=True)
    # @login_required
    def get(self, id):
        ''' 유저 단수 조회 '''     
        data = UserModel.query.get_or_404(id)
        return data
    
    @ns.marshal_list_with(user, skip_none=True)
    # @login_required
    def put(self,id):
        ''' 유저 information update '''
        # 업데이트에 성공하였으면 업데이트된 정보 반환
        data = UserModel.query.get_or_404(id)
        for item in request.get_json():
            setattr(data, item, request.get_json()[item])
        g.db.commit()
        return data
    

    @ns.marshal_list_with(user, skip_none=True)
    # @login_required
    def delete(self,id):
        ''' 유저 Safe delete '''
        model = UserModel.query.filter( UserModel.id==id ).first()
        model.is_deleted = True
        g.db.commit()       
        return model
    
  
# /api/users/1
@ns.route("/<int:id>/rights")
@ns.param("id", "유저 고유 번호")
class UserRight(Resource):    
    @ns.marshal_list_with(user_right, skip_none=True)
    # @login_required
    def get(self, id):
        ''' 유저 권한 조회 '''     
        data = UserModel.query.get_or_404(id)
        return data
    
    @ns.marshal_list_with(user_right, skip_none=True)
    @login_required
    def put(self,id):
        ''' 유저 권한 update '''
        # 업데이트에 성공하였으면 업데이트된 정보 반환
        data = UserModel.query.get_or_404(id)
        right_data = request.get_json()["right"]
        setattr(data, "right", right_data)
        g.db.commit()
        return data



@ns.route("/rights")
class UserRight(Resource):    
    @ns.marshal_list_with(user_right, skip_none=True)
    # @login_required
    def get(self):
        ''' 유저복수 권한 조회 '''     
        data = UserModel.query.filter(UserModel.is_deleted!=True).order_by(UserModel.id).all()
        return data

    @ns.marshal_list_with(user_right, skip_none=True)
    # @login_required
    def post(self):
        ''' 유저복수 권한 수정 '''             
             
        for item in request.get_json():
            user = UserModel.query.get_or_404(item["id"])
            del item['id'] 
            del item['user_id'] 
            del item['user_name'] 
            del item['created_at'] 
            user.right = item
            g.db.commit()
        data = UserModel.query.order_by(UserModel.id).all()
        return data
