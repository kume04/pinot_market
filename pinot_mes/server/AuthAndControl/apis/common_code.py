from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from sqlalchemy import null
from AuthAndControl.models.log import Log  as CompanyModel
from AuthAndControl.models.common_code import CommonCode as CommonCodeModel

ns = Namespace(
    "common_code",
    description="공통코드 api"
)

common_code =  ns.model("common_code", {
    "id": fields.Integer(required=True, description="id"),
    "type": fields.String(required=True, ),
    "name": fields.String(required=False, ),
    "value": fields.String(required=False, ),
    "etc": fields.String(required=False, ),
    "is_deleted": fields.Boolean(description="삭제여부"),
})


@ns.route("")
class CommonCodeList(Resource):
    @ns.marshal_list_with(common_code, skip_none=False)
    def get(self):
        ''' 공통코드 복수 조회 '''
        data = CommonCodeModel.query.filter(CommonCodeModel.is_deleted==False).order_by( CommonCodeModel.type.desc(), CommonCodeModel.name.desc() ).all()
        return data

    @ns.marshal_list_with(common_code, skip_none=True)
    # @login_required
    def post(self):
        ''' 공통코드 등록 '''         
        data = CommonCodeModel(**request.get_json())
        data.id = None
        g.db.add(data)    
        g.db.commit()
        data = CommonCodeModel.query.order_by(CommonCodeModel.id).all()
        return data

    
   
    

@ns.route("/<int:id>")
@ns.param("id", "아이디")
class CommonCodeDetail(Resource):    

    @ns.marshal_list_with(common_code, skip_none=True)
    # @login_required
    def delete(self,id):
        ''' 공통코드 Safe delete '''
        model = CommonCodeModel.query.filter( CommonCodeModel.id==id ).first()
        model.is_deleted = True
        g.db.commit()       
        return model

    @ns.marshal_list_with(common_code, skip_none=True)
    # @login_required
    def put(self,id):
        ''' 공통코드 information update '''
        # 업데이트에 성공하였으면 업데이트된 정보 반환
        data = CommonCodeModel.query.get_or_404(id)
        for item in request.get_json():
            setattr(data, item, request.get_json()[item])
        g.db.commit()
        return data
    