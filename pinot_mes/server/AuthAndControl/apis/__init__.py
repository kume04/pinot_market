from flask_restx import Api
from flask import Blueprint
from .user import ns as UserNamespace
from .quick_menu import ns as QuickMenuNamespace
from .log import ns as LogNamespace
from .common_code import ns as CommonCodeNamespace
blueprint = Blueprint(
    "api",
    __name__,
    url_prefix="/api"
)

api = Api(
    blueprint,
    title="AuthAndControl",
    version="1.0",
    doc="/docs",
    description="user auth/check and right controll application"
)


# TODO: add namespace to Blueprint
api.add_namespace(UserNamespace)
api.add_namespace(QuickMenuNamespace)
api.add_namespace(LogNamespace)
api.add_namespace(CommonCodeNamespace)