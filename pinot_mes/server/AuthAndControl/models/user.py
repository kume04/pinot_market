from AuthAndControl import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String(20), unique=True, nullable=False)
    user_name = db.Column(db.String(20), nullable=False)
    password = db.Column(db.String(256), nullable=False)
    user_type = db.Column(db.String(20), nullable=True)
    partner_name = db.Column(db.String(20), nullable=True)
    user_email = db.Column(db.String(50), nullable=False)
    user_phone = db.Column(db.String(20), nullable=False)
    user_address = db.Column(db.String(200), nullable=True)
    is_deleted = db.Column(db.Boolean, nullable=False, default=False)
    right  = db.Column(db.JSON, nullable=True)
    etc = db.Column(db.String(), nullable=True)
    department_code= db.Column(db.String(20), nullable=True)
    created_at = db.Column(db.DateTime, nullable=True,) 

    @classmethod
    def fine_one_by_user_id(cls, user_id):
        return User.query.filter_by(user_id=user_id).first()