from AuthAndControl import db

class QuickMenu(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    menu_url = db.Column(db.String(20), nullable=True)
    menu_name = db.Column(db.String(200), nullable=True)
    index = db.Column(db.Integer, default=0)
    is_deleted = db.Column(db.Boolean, nullable=False, default=False)
    is_external = db.Column(db.Boolean, nullable=False, default=False)
    created_at = db.Column(db.DateTime, nullable=True) 

