from AuthAndControl import db
from sqlalchemy import func

class Log(db.Model):
    __tablename__ = "log_data"
    id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String(), nullable=True)
    user_type = db.Column(db.String(),nullable=True)
    etc = db.Column(db.String(), nullable=True)
    created_at = db.Column(db.DateTime, nullable=True, default=func.now()) 
