from pickle import FALSE
from AuthAndControl import db

class CommonCode(db.Model):
    __tablename__ = "common_code"
    id = db.Column(db.Integer, primary_key=True)    
    type = db.Column(db.String(20), nullable=False)
    name = db.Column(db.String(20), nullable=False)
    value = db.Column(db.String, nullable=False)
    etc = db.Column(db.String(500), nullable=True)
    is_deleted =db.Column(db.Boolean,  default=False)
