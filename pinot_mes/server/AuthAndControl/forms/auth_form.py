from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField ,BooleanField
from wtforms.validators import DataRequired, EqualTo

class LoginForm(FlaskForm):
    class Meta:
        csrf = False
    user_id = StringField("User ID", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])

class RegisterForm(LoginForm):
    class Meta:
        csrf = False
    password = PasswordField(
        "Password",
        validators=[DataRequired(), EqualTo(
            "repassword",
            message="비밀번호가 일치하지 않습니다."
        )]
    )
    repassword = PasswordField(
        "Confirm Password",
        validators=[DataRequired()]
    )

    user_name = StringField("유저 이름", validators=[DataRequired()])
    user_type = StringField("유저 유형",)
    partner_name = StringField("거래처 이름",)
    user_email = StringField("유저 이메일",)
    user_phone = StringField("유저 연락처",validators=[DataRequired()])
    is_deleted = BooleanField("사용 제한",)