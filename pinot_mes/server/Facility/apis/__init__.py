from flask_restx import Api
from flask import Blueprint
from .facility import ns as FacilityNamespace


blueprint = Blueprint(
    "api",
    __name__,
    url_prefix="/api"
)

api = Api(
    blueprint,
    title="Facility",
    version="1.0",
    doc="/docs",
    description="Facility application"
)


# TODO: add namespace to Blueprint
api.add_namespace(FacilityNamespace)