from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from Facility.models.facility import Facility as FacilityModel

ns = Namespace(
    "facilities",
    description="설비 API"
)

schedule = ns.model("schedule",{
    "id" : fields.Integer(required=True, description="설비 아이디"),
    "date": fields.DateTime(decription="날짜"),
    "time": fields.String(decription="시간대")
})

# facility 조회
facility = ns.model("facility", {    
    "id" : fields.Integer(required=True, description="설비 아이디"),
    "created_by": fields.String(required=False, description="최초등록자"),
    "created_at": fields.String(description="최초등록일자"),
    "type" :   fields.String(description="장비분류"),
    "name" :   fields.String(description="장비모델명"),
    "index" :  fields.Integer(description="장비순서"),
    "is_deleted" : fields.Boolean(default=False),
    "created_by" : fields.String(description="등록자"),
    "created_at" : fields.DateTime(description="등록날짜"),
    "content" :  fields.String(description="기초셋팅/장비이력"),
    "location" : fields.String(description="장비위치"),
    "vender" : fields.String(description="구입처"),
    "as_person" : fields.String(description="AS담당자"),
    "as_tel" : fields.String(description="AS연락처"),
    "schedule" : fields.List(fields.Nested(schedule)),
})

# 거래처 입력
post_parser = reqparse.RequestParser()
post_parser.add_argument("id", type=int, required=False,  help=" 아이디")

# /api/partners
@ns.route("")
class PartnerList(Resource):
    @ns.marshal_list_with(facility, skip_none=True)
    def get(self):
        ''' 설비 복수 조회 '''
        
        data = FacilityModel.query.filter(FacilityModel.is_deleted==False).order_by(FacilityModel.id.desc()).all()
        
        return data
    
    @ns.marshal_list_with(facility, skip_none=True)
    def post(self):
        ''' 문의 생성 '''
        notice_data = FacilityModel(**request.get_json())
        g.db.add(notice_data)
        g.db.commit()
        return  201


# /api/partners/1
@ns.route("/<int:id>")
@ns.param("id", " 아이디")
class Partner(Resource):
    @ns.marshal_list_with(facility, skip_none=True)
    def get(self, id):
        ''' 설비 단수 조회 '''
        data = FacilityModel.query.get_or_404(id)
        return data

    
    @ns.marshal_list_with(facility, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 설비  info update '''
        # 업데이트에 성공하면 업데이트 된 정보 반환
        data = FacilityModel.query.get_or_404(id)
        origin_data = request.get_json()
        for item in origin_data.keys():
            setattr(data, item, origin_data[item])
        obj = g.db.merge(data)
        g.db.object_session(obj)
        g.db.commit()
        return 200

    
    # @ns.expect(post_parser)
    def delete(self, id):
        ''' 설비 Safe delete ''' 
        data = FacilityModel.query.filter(FacilityModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return 200