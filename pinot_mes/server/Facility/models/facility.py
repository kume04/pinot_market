from Facility import db
from sqlalchemy import func

class Facility(db.Model):    
    id =  db.Column(db.Integer ,primary_key=True) 
    type =   db.Column(db.String, nullable=True )
    name =   db.Column(db.String, nullable=True )
    index =  db.Column(db.Integer, nullable=True )
    is_deleted = db.Column(db.Boolean , nullable=True , default=False)
    created_by = db.Column(db.String, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    content =  db.Column(db.Text, nullable=True )
    location = db.Column(db.String, nullable=True)
    vender = db.Column(db.String, nullable=True)
    as_person = db.Column(db.String, nullable=True)
    as_tel = db.Column(db.String, nullable=True)
    # schedule = db.relationship(
    #     'FacilitySchedule',
    #     backref='schedule',
    #     cascade='all',
    #     order_by='desc(FacilitySchedule.id)',
    #     primaryjoin="Facility.id == FacilitySchedule.facility_id"
    # )
class FacilitySchedule(db.Model):
    id = db.Column(db.Integer ,primary_key=True) 
    facility_id =   db.Column(db.Integer, db.ForeignKey('facility.id'))
    date = db.Column(db.DateTime, nullable=True)
    time = db.Column(db.String, nullable=True )
