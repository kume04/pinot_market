from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField ,BooleanField, IntegerField, DateTimeField, TextField
from wtforms.validators import DataRequired, EqualTo

class ProjectForm(FlaskForm):
    class Meta:
        csrf = False
    id = IntegerField("id")
    ordered_at = StringField("ordered_at")
    orderer = StringField("orderer")
    receiver = StringField("receiver")
    project_name = StringField("project_name", validators=[DataRequired()])
    count = IntegerField("count")
    expected_time = DateTimeField("expected_time")
    final_due = DateTimeField("final_due")
    worker = StringField("worker")
    content =  TextField("content")
    facility = StringField("facility", )
    design = BooleanField("design", )
    produce = BooleanField("produce",)
    complete = BooleanField("complete", )
    delivery_method = StringField("delivery_method",)
    real_final_due = StringField("real_final_due", )
    created_at = DateTimeField("created_at")
    fault_etc = StringField("fault_etc")
    etc = StringField("etc")
    
    is_deleted =  BooleanField("is_deleted", default=False )

# class RegisterForm(LoginForm):
#     class Meta:
#         csrf = False
#     password = PasswordField(
#         "Password",
#         validators=[DataRequired(), EqualTo(
#             "repassword",
#             message="비밀번호가 일치하지 않습니다."
#         )]
#     )
#     repassword = PasswordField(
#         "Confirm Password",
#         validators=[DataRequired()]
#     )

#     user_name = StringField("유저 이름", validators=[DataRequired()])
#     user_type = StringField("유저 유형",)
#     partner_name = StringField("거래처 이름",)
#     user_email = StringField("유저 이메일",)
#     user_phone = StringField("유저 연락처",validators=[DataRequired()])
#     is_deleted = BooleanField("사용 제한",)