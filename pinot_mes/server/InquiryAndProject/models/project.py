from InquiryAndProject import db



class Project(db.Model):
    id = db.Column(db.Integer,  primary_key=True) 
    inquiry_id = db.Column(db.Integer, db.ForeignKey('inquiry.id'))
    ordered_at =db.Column(db.DateTime, nullable=True) 
    orderer =db.Column(db.String(100), nullable=True)
    receiver = db.Column(db.String(100), nullable=True) 
    project_name =db.Column(db.String(100), nullable=True) 
    count =db.Column(db.Integer, nullable=True)
    expected_time = db.Column(db.DateTime(), nullable=True)
    final_due =db.Column(db.DateTime(20), nullable=True) 
    worker =db.Column(db.String(20), nullable=True)
    content =db.Column(db.Text, nullable=True)  
    facility =db.Column(db.String(100), nullable=True) 
    estimate =db.Column(db.Boolean, nullable=True, default=False)
    design =db.Column(db.Boolean, nullable=True, default=False) 
    produce =db.Column(db.Boolean, nullable=True, default=False) 
    produce_complete = db.Column(db.Boolean, nullable=True, default=False)
    delivery =db.Column(db.Boolean, nullable=True, default=False)

    complete =db.Column(db.Boolean, nullable=True, default=False ) 
    delivery_method =db.Column(db.String(20), nullable=True) 
    real_final_due =db.Column(db.DateTime, nullable=True) 
    created_at =db.Column(db.DateTime, nullable=True,) 
    fault_etc =db.Column(db.String(100), nullable=True)
    delivery_etc =db.Column(db.String(100), nullable=True)
    etc =db.Column(db.String(100), nullable=True) 
    is_deleted =db.Column(db.Boolean, nullable=True, default=False )  

    package_type = db.Column(db.String(100), nullable=True) 
    design_type = db.Column(db.String(100), nullable=True) 
    project_method = db.Column(db.String(100), nullable=True) 
    project_type = db.Column(db.String(100), nullable=True) 
    estimated_price =  db.Column(db.String(100), nullable=True) 
    project_file_address = db.Column(db.String(200), nullable=True) 
    machine = db.Column(db.String(200), nullable=True) 
    confirm_data = db.Column(db.String(200), nullable=True) 
    deliver = db.Column(db.String(), nullable=True) 
    deliver_number = db.Column(db.String(), nullable=True) 
    @classmethod
    def fine_one_by_project_id(cls, project_id):
        return Project.query.filter_by(id=project_id).first()