from InquiryAndProject import db

class Inquiry(db.Model):
    id = db.Column(db.Integer,  primary_key=True) 
    is_urgent = db.Column(db.String,  nullable = True)
    received_channel =  db.Column(db.String,  nullable = True)
    received_at =  db.Column(db.DateTime,  nullable = True)
    received_worker =  db.Column(db.String,  nullable = True)
    client_company_name =  db.Column(db.String,  nullable = True)
    client_person =  db.Column(db.String,  nullable = True)
    client_person_tel =  db.Column(db.String,  nullable = True)
    is_prepayed_deliver =  db.Column(db.String,  nullable = True)
    delivery_method =  db.Column(db.String,  nullable = True)
    domain_location =  db.Column(db.String,  nullable = True)
    sender =  db.Column(db.String,  nullable = True)
    receiver =  db.Column(db.String,  nullable = True)
    receiver_address =  db.Column(db.String,  nullable = True)
    inquiry_end_due =  db.Column(db.DateTime,  nullable = True)
    inquiry_final_due =  db.Column(db.DateTime,  nullable = True)
    content =  db.Column(db.Text,  nullable = True)
    inquiry_status =  db.Column(db.String,  nullable = True)
    pay_method = db.Column(db.String,  nullable = True)
    receipt_method = db.Column(db.String,  nullable = True)
    coopang = db.Column(db.String,  nullable = True)
    project = db.relationship(
        'Project',
        backref='inquiry',
        cascade='all',
        order_by='desc(Project.id)',
        primaryjoin="Inquiry.id == Project.inquiry_id"
    )
    is_deleted = db.Column(db.Boolean,  default=False)
    @classmethod
    def fine_one_by_inquiry_id(cls, inquiry_id):
        return Inquiry.query.filter_by(id=inquiry_id).first()

