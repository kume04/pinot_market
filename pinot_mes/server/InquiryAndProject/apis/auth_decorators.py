import jwt
from flask import abort
from flask.globals import request
def login_required(func):
    def wrapper(self,  *args, **kwargs):
        try:
            access_token = request.headers.get('Authorization', None)
            token = str.replace(str(access_token), 'Bearer ', '')
            payload = jwt.decode(token, 'secret' ,algorithm='HS256')
        except jwt.exceptions.DecodeError:          
            return  abort(403, '유효하지 않은 토큰.')
        return func(self,  *args, **kwargs)
    return wrapper