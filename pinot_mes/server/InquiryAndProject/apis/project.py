import re
from flask import g
from flask.globals import request
from flask_restx import Namespace, Resource, fields, Model
from InquiryAndProject.models.project import Project as ProjectModel
from InquiryAndProject.models.inquiry import Inquiry as InquiryModel
from InquiryAndProject.forms.project_form import ProjectForm
from flask import jsonify, make_response
from werkzeug import security
from flask import abort
import jwt
from .auth_decorators import login_required
from flask_restx import reqparse
from datetime import datetime
import pytz

ns = Namespace(
    "projects",
    description="프로젝트 관련 API"
)

project = ns.model("project", {
    "id": fields.Integer(required=True, description="고유 번호"),
    "ordered_at": fields.DateTime(required=False, description="주문일"),
    "orderer": fields.String(required=True, description="주문자명"),
    "receiver" : fields.String(description="접수자"),
    "project_name" : fields.String(description="프로젝트 명"),
    "count" : fields.String(required=True, description="수량"),
    "expected_time" : fields.DateTime(required=False, description="예상시간"),
    "final_due" : fields.DateTime(required=False, description="마감일"),
    "worker" : fields.String(required=False, description="작업자"),
    "machine" : fields.String(required=False, description="가동장비"),
    "design" : fields.Boolean(required=False, description="디자인"),
    "deliver" : fields.Boolean(required=False, description="운송회사"),
    "deliver_number" : fields.Boolean(required=False, description="송장번호"),
    "delivery" : fields.Boolean(required=False, description="배송"),
    "produce" : fields.Boolean(required=False, description="생산"),
    "complete" : fields.Boolean(required=False, description="완료"),
    "delivery_method" : fields.String(required=False, description="배송방법"),
    "real_final_due" : fields.DateTime(required=False, description="납기일"),
    "created_at" : fields.DateTime(required=False, description="생성일"),
})

project_detail = ns.model("project_detail", {
    "id": fields.Integer(required=True, description="유저 고유 번호"),
    "ordered_at": fields.DateTime(required=False, description="주문일"),
    "orderer": fields.String(required=False, description="주문자명"),
    "receiver" : fields.String(description="접수자"),
    "project_name" : fields.String(description="프로젝트 명"),
    "content" : fields.String(description="내용"),
    "etc" : fields.String(description="주의사항"),
    "fault_etc" : fields.String(description="불량내역"),
    "delivery_etc" : fields.String(description="특이사항"),
    "count" : fields.String(required=False, description="수량"),
    "expected_time" : fields.DateTime(required=False, description="예상시간"),
    "final_due" : fields.DateTime(required=False, description="마감일"),
    "worker" : fields.String(required=False, description="작업자"),
    "machine" : fields.String(required=False, description="가동장비"),
    "design" : fields.Boolean(required=False, description="디자인"),
    "deliver" : fields.String(required=False, description="운송회사"),
    "deliver_number" : fields.String(required=False, description="송장번호"),
    "produce" : fields.Boolean(required=False, description="생산"),
    "produce_complete" : fields.Boolean(required=False, description="생산 완료"),
    "delivery" : fields.Boolean(required=False, description="배송"),
    "complete" : fields.Boolean(required=False, description="완료"),
    "confirm_data" :  fields.String(required=False, description="확인일자"),
    "delivery_method" : fields.String(required=False, description="배송방법"),
    "real_final_due" : fields.DateTime(required=False, description="납기일"),
    "created_at" : fields.DateTime(required=False, description="생성일"),
    "package_type" : fields.String(required=False, description="포장형태"),
    "design_type" : fields.String(required=False, description="시안 형태"),
    "project_method" : fields.String(required=False, description="프로젝트 종류"),
    "project_type" : fields.String(required=False, description="작업 형태"),
    "project_file_address" : fields.String(required=False, description="원본 데이터 위치"),
})

parser = reqparse.RequestParser()
parser.add_argument("id", type=int)

# /api/users
@ns.route("")
class ProjectList(Resource):
    @ns.marshal_list_with(project, skip_none=True)
    @login_required
    def get(self):
        ''' 프로젝트 조회(미완료) '''
        data = ProjectModel.query.filter(ProjectModel.complete==False).filter(ProjectModel.is_deleted==False).all()
        return data

    @ns.marshal_list_with(project, skip_none=True)
    @ns.expect(project)
    @ns.doc(responses={200: '주문/프로젝트가 등록되었습니다.'})
    @ns.doc(responses={400: '입력 정보가 올바르지 않습니다.'})
    @login_required
    def post(self):
        ''' 주문/프로젝트 등록 '''        
        #Copy and modify user Registration
        data = ProjectForm()
        if data.validate_on_submit():
            form_data = (request.form.to_dict())
            form_data["created_at"] = datetime.now( pytz.timezone('Asia/Seoul')  )
            project = ProjectModel( **form_data )
            g.db.add(project)
            g.db.commit()
        project =  ProjectModel.query.get_or_404(project.id)
        return project

@ns.route("/<int:id>")
class Project(Resource):
    @ns.marshal_list_with(project_detail, skip_none=True)
    # @login_required
    def get(self,id):
        ''' 프로젝트 조회'''
        if request.args.get('type')=="working":
            data = ProjectModel.query.filter(ProjectModel.id==id).filter(ProjectModel.complete==False).first()
            return data
        else:
            data = ProjectModel.query.filter(ProjectModel.id==id).first()
            return data

    @ns.marshal_list_with(project_detail, skip_none=True)
    # @ns.expect(project_detail)
    @ns.doc(responses={200: '주문/프로젝트가 수정되었습니다.'})
    @ns.doc(responses={400: '입력 정보가 올바르지 않습니다.'})
    # @login_required
    def put(self,id):
        ''' 주문/프로젝트  update '''
        # 업데이트에 성공하였으면 업데이트된 정보 반환
        data = ProjectModel.query.get_or_404(id)
        for item in request.get_json():
            print(item)
            print( request.get_json()[item])
            setattr(data, item, request.get_json()[item])
        
        g.db.commit()
        return data


    
    @ns.marshal_list_with(project_detail, skip_none=True)
    # @login_required
    def delete(self,id):
        ''' 주문/프로젝트 Safe delete '''
        model = ProjectModel.query.filter( ProjectModel.id==id ).first()
        model.is_deleted = True
        g.db.commit()       
        return 200
    
