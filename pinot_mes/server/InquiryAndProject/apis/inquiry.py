import re
from flask import g
from flask.globals import request
from flask_restx import Namespace, Resource, fields, Model
from InquiryAndProject.models.project import Project as ProjectModel
from InquiryAndProject.models.inquiry import Inquiry as InquiryModel
from InquiryAndProject.forms.project_form import ProjectForm
from flask import jsonify, make_response
from werkzeug import security
from flask import abort
import jwt
from .auth_decorators import login_required
from flask_restx import reqparse
from datetime import datetime
import pytz
from .project import project, project_detail
import copy
import json
ns = Namespace(
    "inquiries",
    description="문의 관련 API"
)

inquiry = ns.model("inquiry", {
    "id" : fields.Integer(required=True , description="아이디"),
    "is_urgent" : fields.String( description="긴급 여부"),
    "received_channel" :  fields.String( description="접수 방법"),
    "received_at" :  fields.DateTime( description="접수 시간"),
    "client_company_name" :  fields.String( description="주문 고객 회사"),
    "client_person" :  fields.String( description="주문 고객"),
    "client_person_tel" :  fields.String( description="주문 고객 연락처"),
    "is_prepayed_deliver" :  fields.String( description="선물/착불 여부"),
    "delivery_method" :  fields.String( description="배송 방법"),
    "domain_location" :  fields.String( description="지녁"),
    "sender" :  fields.String( description="보내는 사람"),
    "receiver" :  fields.String( description="받는 사람"),
    "receiver_address" :  fields.String( description="배송지"),
    "inquiry_end_due" :  fields.DateTime( description="예상 마감일"),
    "inquiry_final_due" :  fields.DateTime( description="납기일"),
    "content" :  fields.String( description="내용"),
    "received_worker" :  fields.String( description="접수 담당자"),
    "project" : fields.List(fields.Nested(project_detail)),
    "is_deleted" : fields.Boolean( description="삭제여부"),
    "inquiry_status" :  fields.String( description="문의 상태"),
})

parser = reqparse.RequestParser()
parser.add_argument("id", type=int)

# /api/users
@ns.route("")
class InquiryList(Resource):
    @ns.marshal_list_with(inquiry, skip_none=False)
    # @login_required
    def get(self):
        ''' 주문 조회 '''
        if request.args.get('type')=="coopang":            
            data = InquiryModel.query.join(ProjectModel).filter(InquiryModel.is_deleted==False).filter(ProjectModel.produce_complete==False).filter(InquiryModel.coopang=="1").order_by(InquiryModel.id.desc()).all()
            return data
        if request.args.get('type')=="inquiry":            
            data = InquiryModel.query.filter(InquiryModel.is_deleted==False).filter(InquiryModel.coopang!="1").order_by(InquiryModel.id.desc()).all()
            return data
        if request.args.get('type')=="producing":      
            print("..asdfsdf.")      
            data = InquiryModel.query.join(ProjectModel).filter(ProjectModel.produce_complete==False).filter(InquiryModel.coopang == None).filter(ProjectModel.is_deleted==False).order_by(InquiryModel.id.desc()).all()
            return data
        if request.args.get('type')=="producing_coopang":            
            data = InquiryModel.query.join(ProjectModel).filter(ProjectModel.produce_complete==True).filter(InquiryModel.coopang=="1").filter(ProjectModel.is_deleted==False).order_by(InquiryModel.id.desc()).all()
            return data
        elif request.args.get('type')=="delivery":          
            data = InquiryModel.query.join(ProjectModel).filter(ProjectModel.produce_complete==True).filter(InquiryModel.coopang==None).filter(ProjectModel.complete!=True).filter(ProjectModel.is_deleted==False).order_by(InquiryModel.id.desc()).all()
            return data
        elif request.args.get('type')=="complete":
            data = InquiryModel.query.join(ProjectModel).filter(ProjectModel.produce_complete==True).filter(InquiryModel.coopang==None).filter(ProjectModel.is_deleted==False).order_by(InquiryModel.id.desc()).all()
            return data
        elif request.args.get('type')=="complete_coopang":
            data = InquiryModel.query.join(ProjectModel).filter(ProjectModel.complete==True).filter(InquiryModel.coopang=="1").filter(ProjectModel.is_deleted==False).order_by(InquiryModel.id.desc()).all()
            return data
        else:
            data = InquiryModel.query.all().filter(InquiryModel.is_deleted==False)
            return data

    @ns.marshal_list_with(inquiry, skip_none=True)
    @ns.expect(inquiry)
    @ns.doc(responses={200: '주문/프로젝트가 등록되었습니다.'})
    @ns.doc(responses={400: '입력 정보가 올바르지 않습니다.'})
    # @login_required
    def post(self):
        ''' 주문/프로젝트 등록 '''        
        #Copy and modify user Registration
        # data_origin = request.get_json()
        data_origin = json.loads(request.data, strict=True)
        inquiry_data = copy.deepcopy(data_origin)
        del inquiry_data['project'] 
        inquiry = InquiryModel(**inquiry_data)
        g.db.add(inquiry)
        g.db.commit()    
        if data_origin.get("project"):
            for item in data_origin.get("project"):                
                item["inquiry_id"] = inquiry.id             
                if "machine" in item:
                    item["machine"] = "/".join(item["machine"])
                print(item)
                project =  ProjectModel(**item)
                g.db.add(project)         
                g.db.commit()    
        return 200

@ns.route("/<int:id>")
class Inquiry(Resource):
    @ns.marshal_list_with(inquiry, skip_none=True)
    # @login_required
    def get(self,id):
        ''' 주문&프로젝트 조회'''
        if request.args.get('type')=="producing":
            print("heh")
            data = InquiryModel.query.filter(InquiryModel.id==id).filter(ProjectModel.complete==False).first()
            return data
        
        else:
            data = InquiryModel.query.filter(InquiryModel.id==id).first()
            return data


    @ns.marshal_list_with(inquiry, skip_none=True)
    @ns.expect(inquiry)
    @ns.doc(responses={200: '주문/프로젝트가 수정되었습니다.'})
    @ns.doc(responses={400: '입력 정보가 올바르지 않습니다.'})
    # @login_required
    def put(self,id):
        ''' 주문  update '''
        # 업데이트에 성공하였으면 업데이트된 정보 반환
        inquiry_data = InquiryModel.query.get_or_404(id)
        origin_data = request.get_json()
        project_removed_data =  copy.deepcopy(origin_data)
        del project_removed_data["project"]        
        for item in project_removed_data:         
            setattr(inquiry_data, item, project_removed_data[item])

        if "project_id" in origin_data:
            project_data = ProjectModel.query.get_or_404(origin_data["project_id"])
            for item in origin_data["project"][0].keys():
                setattr(project_data, item, origin_data["project"][0][item])
        
        g.db.commit()
        return 200 

    
    @ns.marshal_list_with(inquiry, skip_none=True)
    @login_required
    def delete(self,id):
        ''' 주문 Safe delete '''
        model = InquiryModel.query.filter( InquiryModel.id==id ).first()
        model.is_deleted = True
        g.db.commit()       
        return 200
    
