from flask_restx import Api
from flask import Blueprint
from .project import ns as ProjectNamespace
from .inquiry import ns as InquiryNamespace

blueprint = Blueprint(
    "api",
    __name__,
    url_prefix="/api"
)

api = Api(
    blueprint,
    title="Inquiry/Project 관리",
    version="1.0",
    doc="/docs",
    description="project controll application"
)


# TODO: add namespace to Blueprint
api.add_namespace(ProjectNamespace)
api.add_namespace(InquiryNamespace)