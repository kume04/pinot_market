from Partnership import db
from sqlalchemy import func

class Partner(db.Model):
    __tablename__ = 'partner'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String())
    co_name = db.Column(db.String(), nullable=False)
    business_type = db.Column(db.String())
    co_type = db.Column(db.String(), nullable=False)
    repre = db.Column(db.String())
    business_no = db.Column(db.String(), nullable=False)
    person = db.Column(db.String())
    contact = db.Column(db.String(), nullable=False)
    cellphone = db.Column(db.String())
    email = db.Column(db.String())
    address1 = db.Column(db.String(), nullable=False)
    address2 = db.Column(db.String(), nullable=False)
    is_deleted = db.Column(db.Boolean, nullable=False, default=False)
    note = db.Column(db.String(), nullable=False)
    created_by = db.Column(db.String())
    created_at = db.Column(db.DateTime(), default=func.now())
    fax = db.Column(db.String(), nullable=False)
 
    # @classmethod
    # def find_one_by_user_id(cls, user_id):
    #     return User.query.filter_by(user_id=user_id).first()