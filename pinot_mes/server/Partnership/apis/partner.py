from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from Partnership.models.partner import Partner as PartnerModel

ns = Namespace(
    "partners",
    description="거래처 API"
)
# partner 조회
partner = ns.model("partner", {
    
    "id" : fields.Integer(required=True, description="거래처 아이디"),
    "code": fields.String(required=False, description="거래처 코드"),
    "co_name": fields.String(description="거래처명"),
    "business_type": fields.String(required=False, description="거래유형"),
    "co_type": fields.String(required=False, description="사업자종류"),
    "repre": fields.String(required=False, description="대표자"),
    "business_no": fields.String(required=False, description="사업자번호"),
    "person": fields.String(required=False, description="담당자"),
    "contact": fields.String(required=False, description="연락처"),
    "cellphone": fields.String(required=False, description="휴대폰"),
    "email": fields.String(required=False, description="이메일"),
    "fax": fields.String(required=False, description="팩스"),
    "address1": fields.String(required=False, description="주소"),
    "address2": fields.String(required=False, description="상세주소"),
    "is_deleted": fields.Boolean(required=True, description="삭제여부"),
    "note": fields.String(required=False, description="비고"),
    "created_by": fields.String(required=False, description="최초등록자"),
    "created_at": fields.String(description="최초등록일자")
})

# 거래처 입력
post_parser = reqparse.RequestParser()
post_parser.add_argument("id", type=int, required=False,  help="거래처 아이디")
post_parser.add_argument("code", required=False,   help="거래처 코드")
post_parser.add_argument("co_name", help="거래처명")
post_parser.add_argument("business_type", required=False, help="거래유형")
post_parser.add_argument("co_type", required=False, help="사업자종류")
post_parser.add_argument("repre", required=False, help="대표자")
post_parser.add_argument("business_no", required=False, help="사업자번호")
post_parser.add_argument("person", required=False, help="담당자")
post_parser.add_argument("contact", required=False, help="연락처")
post_parser.add_argument("cellphone", required=False, help="휴대폰")
post_parser.add_argument("email", required=False, help="이메일")
post_parser.add_argument("address1", required=False, help="주소")
post_parser.add_argument("address2", required=False, help="상세주소")
post_parser.add_argument("note", required=False, help="비고")
post_parser.add_argument("created_by", required=False, help="최초등록자")

# /api/partners
@ns.route("")
class PartnerList(Resource):
    @ns.marshal_list_with(partner, skip_none=True)
    def get(self):
        ''' 거래처 복수 조회 '''
        print("??sdrf")  
        data = PartnerModel.query.filter(PartnerModel.is_deleted==False).all()
        
        return data
    
    @ns.expect(post_parser)
    @ns.marshal_list_with(partner, skip_none=True)
    def post(self):
        ''' 거래처 생성 '''
        args = post_parser.parse_args()
        partner = PartnerModel()
        for key, value in args.items() :
            setattr(partner, key, value)

        g.db.add(partner)
        g.db.commit()
        return partner, 201


# /api/partners/1
@ns.route("/<int:id>")
@ns.param("id", "거래처 아이디")
class Partner(Resource):
    @ns.marshal_list_with(partner, skip_none=True)
    def get(self, id):
        ''' 거래처 단수 조회 '''
        data = PartnerModel.query.get_or_404(id)
        return data

    
    @ns.marshal_list_with(partner, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 거래처 info update '''
        # 업데이트에 성공하면 업데이트 된 정보 반환
        # print(request)
        data = PartnerModel.query.get_or_404(id)
        origin_data = request.get_json()
        for item in origin_data.keys():
            print(origin_data[item])
            setattr(data, item, origin_data[item])
        
        g.db.commit()
        return data


    
    # @ns.expect(post_parser)
    def delete(self, id):
        ''' 거래처 Safe delete '''
        print("sldkjls;dkjf")
        data = PartnerModel.query.filter(PartnerModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return 200