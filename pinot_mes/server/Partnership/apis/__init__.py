from flask_restx import Api
from flask import Blueprint
from .partner import ns as PartnerNamespace


blueprint = Blueprint(
    "api",
    __name__,
    url_prefix="/api"
)

api = Api(
    blueprint,
    title="Partnership",
    version="1.0",
    doc="/docs",
    description="Partnership application"
)


# TODO: add namespace to Blueprint
api.add_namespace(PartnerNamespace)