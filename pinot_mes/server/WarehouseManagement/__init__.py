from flask import Flask, g
from flask import render_template
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

db = SQLAlchemy()

def create_app():
    print("run: create_app()")
    app = Flask(__name__)

    app.config["SECRET_KEY"] = "secretkey"
    app.config["SESSION_COOKIE_NAME"] = "mes_token"
    app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://pinot2:pinot2pinot2@pinot2.c228izlbsafj.ap-northeast-2.rds.amazonaws.com/mes"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["SWAGGER_UI_DOC_EXPANSION"] = "list"

    if app.config["DEBUG"]:
        app.config["SEND_FILE_MAX_AGE_DEFAULT"] = 1


    ''' CORS INIT '''
    CORS(app)

    ''' DB INIT '''
    db.init_app(app)
    # if app.config["SQLALCHEMY_DATABASE_URI"].startswith("sqlite"):
    #     migrate.init_app(app, db, render_as_batch=True)
    # else:
    #     migrate.init_app(app, db)

    ''' Route INIT '''
    # from AuthAndControl.routes import base_route, auth_route
    # app.register_blueprint(base_route.bp)
    # app.register_blueprint(auth_route.bp)

    ''' Restx INIT '''
    from WarehouseManagement.apis import blueprint as api
    app.register_blueprint(api)


    ''' REQUEST HOOK '''
    @app.before_request
    def before_request():
        g.db = db.session   # 내 요청 전에 db.session을 물려주고

    @app.teardown_request
    def teardown_request(exception):
        if hasattr(g, "db"):
            g.db.close()    # 요청이 끝날 때는 db.session을 닫아주는

    # @app.errorhandler(404)
    # def page_404(error):
    #     return render_template("/404.html"), 404

    return app
app = create_app()