from flask import g, request, jsonify
from flask_restx import Namespace, Resource, fields, reqparse
from WarehouseManagement.models.item_order import ItemOrder as ItemOrderModel
from WarehouseManagement.models.item import Item as ItemModel
from WarehouseManagement.apis.item import item
# api/item_orders
# docs에 표시되는 URL
ns = Namespace(
    "item_orders",
    description="아이템 발주 관리 API"
)

# /api/docs  item-GET 항목을 통해서 조회되는 부분
# Models 에 표시되는 사항
item_order = ns.model("item_order", {
    "id" : fields.Integer(required=False, primary_key=True),
    "item" : fields.Nested(item, skip_none=True),
    "count" : fields.String(description="주문 수량"),
    "title" : fields.String(description="발주 제목"),
    "contents" : fields.String(description="발주 내용"),
    "created_by" : fields.String(description="등록자"),
    "created_at" : fields.String(description="등록일자"),
    "is_deleted" : fields.String(description="삭제여부")
})


post_parser = reqparse.RequestParser()
post_parser.add_argument("id", required=False, help="ITEM ID")
post_parser.add_argument("type", required=False, help="ITEM ID")

# /api/item_orders
@ns.route("")
class ItemOrderlist(Resource):
    @ns.marshal_list_with(item_order, skip_none=True)
    def get(self):
        ''' 아이템 발주 복수 조회 '''
        
        data = []
        print( post_parser.parse_args()["type"])
        if post_parser.parse_args()["type"] == "자재":
            data = ItemOrderModel.query.join(ItemOrderModel.item).filter(ItemOrderModel.is_deleted == False).filter(ItemModel.type=="자재" ).all()
        elif post_parser.parse_args()["type"] == "제품":
            data = ItemOrderModel.query.join(ItemOrderModel.item).filter(ItemOrderModel.is_deleted == False).filter(ItemModel.type=="제품").all()
        else:
            print("d")
            data = ItemOrderModel.query.filter(ItemOrderModel.is_deleted == False).all()
        return data

    @ns.expect(post_parser)     # 입력 받는 form을 생성
    @ns.marshal_list_with(item_order, skip_none=True)
    def post(self):
        ''' 아이템 발주 생성 '''
        
        item = ItemOrderModel()
        for key in request.get_json():
            setattr(item, key, request.get_json()[key])

        g.db.add(item)
        g.db.commit()
        return item, 201

# /api/item_orders/1
@ns.route("/<int:id>")
@ns.param("id", "ITEM ORDER ID")
class ItemOrder(Resource):
    @ns.marshal_list_with(item_order, skip_none=True)
    def get(self, id):
        ''' 아이템 발주 단수 조회 '''
        data = ItemOrderModel.query.get_or_404(id)
        return data

    @ns.marshal_list_with(item_order, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 아이템 발주 update '''
        # 업데이트 성공하면 업데이트 된 정보 반환
       
        data = ItemOrderModel.query.get(id)
        for key in request.get_json():
            setattr(data, key, request.get_json()[key])

        g.db.commit()
        return data

    @ns.marshal_list_with(item_order, skip_none=True)
    def delete(self, id):
        ''' 아이템 발주 Safe delete '''
        data = ItemOrderModel.query.filter(ItemOrderModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return data

