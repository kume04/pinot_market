from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from WarehouseManagement.models.item import Item as ItemModel
from .transaction import transaction
# api/item
# docs에 표시되는 URL
ns = Namespace(
    "items",
    description="아이템 관리 API"
)

# /api/docs  item-GET 항목을 통해서 조회되는 부분
# Models 에 표시되는 사항
item = ns.model("item", {
    "id": fields.Integer(required=False, description="ITEM ID"),
    "type": fields.String(required=False, description="ITEM구분"),
    "name": fields.String(required=True, description="ITEM명칭"),
    "category_id_list": fields.String(required=False, description="카테고리코드"),
    "texture": fields.String(required=False, description="재질"),
    "specification_0": fields.String(required=False, description="규격1"),
    "specification_1": fields.String(required=False, description="규격2"),
    "specification_2": fields.String(required=False, description="규격3"),
    "unit": fields.String(required=False, description="단위"),
    "isLocMng": fields.Boolean(required=False, description="LOCATION관리여부"),
    "purchase_price": fields.Integer(required=False, description="구매단가"),
    "selling_price": fields.Integer(required=False, description="판매단가"),
    "safety_stock": fields.Integer(required=False, description="적정재고"),
    "note": fields.String(required=False, description="비고"),
    "created_by": fields.String(required=False, description="등록자"),
    "created_at": fields.String(description="등록일자"),
    "updated_by": fields.String(required=False, description="수정자"),
    "updated_at": fields.String(description="수정일자"),
    "is_deleted": fields.Boolean(required=True, description="삭제여부"),
    "transaction": fields.List(fields.Nested(transaction)),
    "has_barcode":fields.Boolean(required=False, description="바코드 관리 여부"),
    
})

# /api/docs/ 에서 item-POST 항목을 통해 create하는 부분
post_parser = reqparse.RequestParser()
post_parser.add_argument("type", required=False, help="ITEM구분")
post_parser.add_argument("name", required=False, help="ITEM명칭")
post_parser.add_argument("category_id_list", required=False, help="카테고리코드")
post_parser.add_argument("texture", required=False, help="재질")
post_parser.add_argument("specification_0", required=False, help="규격1")
post_parser.add_argument("specification_1", required=False, help="규격2")
post_parser.add_argument("specification_2", required=False, help="규격3")
post_parser.add_argument("unit", required=False, help="단위")
post_parser.add_argument("isLocMng", required=False, help="LOCATION관리여부")
post_parser.add_argument("purchase_price", required=False, help="구매단가")
post_parser.add_argument("selling_price", required=False, help="판매단가")
post_parser.add_argument("safety_stock", required=False, help="적정재고")
post_parser.add_argument("note", required=False, help="비고")
post_parser.add_argument("created_by", required=False, help="등록자")
post_parser.add_argument("created_at", required=False, help="등록일자")
post_parser.add_argument("updated_by", required=False, help="수정자")
post_parser.add_argument("updated_at", required=False, help="수정일자")
post_parser.add_argument("is_deleted", required=False, help="삭제여부")



# /api/items
@ns.route("")
class Itemlist(Resource):
    @ns.marshal_list_with(item, skip_none=False)
    def get(self):
        ''' 아이템 복수 조회 '''
        data = []
        if "type" in   request.args:
            if request.args["type"] == "자재":
                data = ItemModel.query.filter(ItemModel.is_deleted == False,ItemModel.type=="자재" ).all()
            elif request.args["type"] == "제품":           
                data = ItemModel.query.filter(ItemModel.is_deleted == False).filter(ItemModel.type.like("%제품%")).all()
            elif request.args["type"] == "기성제품":           
                data = ItemModel.query.filter(ItemModel.is_deleted == False).filter(ItemModel.type.like("%기성제품%")).all()
            elif request.args["type"] == "주문제품":           
                data = ItemModel.query.filter(ItemModel.is_deleted == False).filter(ItemModel.type.like("%주문제품%")).all()
        else: 
            data = ItemModel.query.filter(ItemModel.is_deleted == False ).all()
        return data

    @ns.expect(post_parser)     # 입력 받는 form을 생성
    @ns.marshal_list_with(item, skip_none=True)
    def post(self):
        ''' 아이템 생성 '''
        
        item = ItemModel()
        print(request.get_json())
        for key in request.get_json():
            setattr(item, key, request.get_json()[key])

        g.db.add(item)
        g.db.commit()
        return item, 201

# /api/items/1
@ns.route("/<int:id>")
@ns.param("id", "ITEM ID")
class Item(Resource):
    @ns.marshal_list_with(item, skip_none=True)
    def get(self, id):
        ''' 아이템 단수 조회 '''
        data = ItemModel.query.get_or_404(id)
        return data

    @ns.marshal_list_with(item, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 아이템 update '''
        # 업데이트 성공하면 업데이트 된 정보 반환
       
        data = ItemModel.query.get(id)
        for item in request.get_json():
            setattr(data, item, request.get_json()[item])

        g.db.commit()
        return data

    @ns.marshal_list_with(item, skip_none=True)
    def delete(self, id):
        ''' 아이템 Safe delete '''
        data = ItemModel.query.filter(ItemModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return data


# /api/items/transaction
@ns.route("/transaction")
class ItemTransaction(Resource):
    @ns.marshal_list_with(item, skip_none=True)
    def get(self):
        ''' 아이템 복수 조회 '''
        data = ItemModel.query.all()
        return data