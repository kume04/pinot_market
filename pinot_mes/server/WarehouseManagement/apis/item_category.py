from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from WarehouseManagement.models.item_category import ItemCategory as ItemCategoryModel

# api/item
# docs에 표시되는 URL
ns = Namespace(
    "item_categories",
    description="아이템 카테고리 관리 API"
)

# Models 에 표시되는 사항


item_category = ns.model("item_category", {
    "id": fields.Integer(required=False, description="ITEM ID"),
    "name": fields.String(required=True, description="ITEM명칭"),
    "parent_id": fields.Integer()
})


# /api/docs/ 에서 item-POST 항목을 통해 create하는 부분
post_parser = reqparse.RequestParser()
post_parser.add_argument("id", required=False, help="ITEM ID")

# /api/items
@ns.route("")
class Itemlist(Resource):
    @ns.marshal_list_with(item_category, skip_none=False)
    def get(self):
        ''' 아이템 카테고리 복수 조회 '''
        data = ItemCategoryModel.query.filter(ItemCategoryModel.is_deleted==False).all()
        return data

    @ns.expect(post_parser)     # 입력 받는 form을 생성
    @ns.marshal_list_with(item_category, skip_none=True)
    def post(self):
        ''' 아이템 카테고리 생성 '''
        item = ItemCategoryModel()
        for key in request.get_json():
            setattr(item, key, request.get_json()[key])

        g.db.add(item)
        g.db.commit()
        return item, 201

# /api/items/1
@ns.route("/<int:id>")
@ns.param("id", "ITEM ID")
class Item(Resource):
    @ns.marshal_list_with(item_category, skip_none=True)
    def get(self, id):
        ''' 아이템 카테고리 단수 조회 '''
        data = ItemCategoryModel.query.get_or_404(id)
        return data

    @ns.marshal_list_with(item_category, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 아이템 카테고리 update '''
        # 업데이트 성공하면 업데이트 된 정보 반환
       
        data = ItemCategoryModel.query.get(id)
        for key in request.get_json():
            setattr(data, key, request.get_json()[key])

        g.db.commit()
        return data

    @ns.marshal_list_with(item_category, skip_none=True)
    def delete(self, id):
        ''' 아이템 카테고리 Safe delete '''
        data = ItemCategoryModel.query.filter(ItemCategoryModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return data
