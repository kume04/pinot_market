from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from WarehouseManagement.models.transaction import Transaction as TransactionModel
from .location import location

# api

# docs에 표시되는 URL
ns = Namespace(
    "transactions",

    description="입출고 관리 API"
)

# /api/docs
# Models 에 표시되는 사항
transaction = ns.model("transaction", {
    "id": fields.Integer(required=False, description="입출고ID"),
    "type": fields.String(required=False, description="구분"),
    "kind": fields.String(required=False, description="유형"),
    "partner_code": fields.String(required=False, description="거래처코드"),
    "item_id": fields.Integer(required=False, description="ITEM코드"),
    "incoming_qtty": fields.Integer(required=False, description="입고수량"),
    "incoming_note": fields.String(required=False, description="입고검사 결과"),
    "incoming_order": fields.Boolean(required=False, description="입고검사 지시"),
    "outgoing_qtty": fields.Integer(required=False, description="출고수량"),
    "outgoing_order": fields.Boolean(required=False, description="출고완료 여부"),
    "outgoing_note": fields.String(required=False, description="출고 결과"),
    "note": fields.String(required=False, description="비고"),
    "created_by": fields.String(required=False, description="등록자"),
    "created_at": fields.String(description="등록일자"),
    "updated_by": fields.String(required=False, description="수정자"),
    "updated_at": fields.String(description="수정일자"),
    "is_deleted": fields.Boolean(required=True, description="삭제여부"),
   "partner_in" :  fields.String(description="구입처"),
   "partner_out" :  fields.String(description="반출처"),
   "price": fields.String(required=False, description="입/출고 가격"),
   
})

# /api/docs
post_parser = reqparse.RequestParser()
post_parser.add_argument("id", required=False, help="아이디")


# /api/transactions
@ns.route("")
class Transactionlist(Resource):
    @ns.marshal_list_with(transaction, skip_none=True)
    def get(self):
        ''' 입출고 복수 조회 '''
        data = TransactionModel.query.filter(TransactionModel.is_deleted==False).all()
        return data

    @ns.expect(post_parser)     # 입력 받는 form을 생성
    @ns.marshal_list_with(transaction, skip_none=True)
    def post(self):
        ''' 입출고 생성 '''
        item = TransactionModel()
        for key in request.get_json():
            setattr(item, key, request.get_json()[key])

        g.db.add(item)
        g.db.commit()
        return item, 201

# /api/transactions/1
@ns.route("/<int:id>")
@ns.param("id", "입출고ID")
class Item(Resource):
    @ns.marshal_list_with(transaction, skip_none=True)
    def get(self, id):
        ''' 입출고 단수 조회 '''
        data = TransactionModel.query.get_or_404(id)
        return data

    @ns.marshal_list_with(transaction, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 입출고 update '''
        # 업데이트 성공하면 업데이트 된 정보 반환
        print(request.data)
        data = TransactionModel.query.get(id)
        for key in request.get_json():
            setattr(data, key, request.get_json()[key])
        g.db.add(data)
        g.db.commit()
        return data

    @ns.marshal_list_with(transaction, skip_none=True)
    def delete(self, id):
        ''' 입출고 Safe delete '''
        data = TransactionModel.query.filter(TransactionModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return data
