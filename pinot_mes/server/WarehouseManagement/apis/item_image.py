from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from sqlalchemy.sql.expression import desc
from WarehouseManagement.models.item import Item as ItemModel
from WarehouseManagement.models.item_image import Itemimage as ItemimageModel

ns = Namespace(
    "item_images",
    description="아이템 이미지 관리 API"
)


item_image = ns.model("item_image", {
    "id" : fields.Integer(required=False, primary_key=True),
    "url" : fields.String(description="이미지url"),
    "is_deleted" : fields.Boolean(description="삭제여부"),
    "item_id" : fields.Integer(description="ITEMID")
})

post_parser = reqparse.RequestParser()
post_parser.add_argument("url", required=False, help="이미지url")

@ns.route("")
class Itemlist(Resource):
    @ns.marshal_list_with(item_image, skip_none=False)
    def get(self):
        ''' 아이템 이미지 복수 조회 '''
        data = ItemimageModel.query.all()
        return data

    @ns.expect(post_parser)     # 입력 받는 form을 생성
    @ns.marshal_list_with(item_image, skip_none=True)
    def post(self):
        ''' 아이템 이미지 생성 '''
        args = post_parser.parse_args()
        item = ItemimageModel()
        for key, value in args.items():
            setattr(item, key, value)

        g.db.add(item)
        g.db.commit()
        return item, 201

# /api/items/1
@ns.route("/<int:id>")
@ns.param("id", "ITEM ID")
class Item(Resource):
    @ns.marshal_list_with(item_image, skip_none=True)
    def get(self, id):
        ''' 아이템 이미지 단수 조회 '''
        data = ItemimageModel.query.get_or_404(id)
        return data

    @ns.marshal_list_with(item_image, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 아이템 이미지 update '''
        # 업데이트 성공하면 업데이트 된 정보 반환
        print(request.data)
        data = ItemimageModel.query.get(id)
        for item in request.form:
            setattr(data, item, request.form[item])

        g.db.commit()
        return data

    @ns.marshal_list_with(item_image, skip_none=True)
    def delete(self, id):
        ''' 아이템 이미지 Safe delete '''
        data = ItemimageModel.query.filter(ItemimageModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return data