from flask_restx import Api
from flask import Blueprint
from .item import ns as ItemNamespace
from .transaction import ns as TransactionNamespace
from .location import ns as LocationNamespace
from .item_category import ns as ItemCategoryNamespace
from .item_order import ns as ItemOrder
from .item_image import ns as ItemImageNamespace


blueprint = Blueprint(
    "api",
    __name__,
    url_prefix="/api"
)

api = Api(
    blueprint,
    title="Warehouse Management",
    version="1.0",
    doc="/docs",
    description="Warehouse Management application"
)


# TODO: add namespace to Blueprint
api.add_namespace(ItemNamespace)
api.add_namespace(ItemCategoryNamespace)
api.add_namespace(TransactionNamespace)
api.add_namespace(LocationNamespace)
api.add_namespace(ItemOrder)
api.add_namespace(ItemImageNamespace)