from flask import g, request
from flask_restx import Namespace, Resource, fields, reqparse
from WarehouseManagement.models.location import Location as LocationModel

# api
# docs에 표시되는 url
ns = Namespace(
    "locations",
    description="창고 관리 API"
)

# Models
location = ns.model("location", {
    "id": fields.Integer(required=False, description="창고ID"),
    "name": fields.String(required=True, description="창고명"),
    "note": fields.String(required=False, description="비고"),  
    "created_at": fields.String(description="등록일자"),  
    "is_deleted": fields.Boolean(required=True, description="삭제여부"),
    "category_id_list": fields.String(required=False, description="카테고리코드"),
})

post_parser = reqparse.RequestParser()
post_parser.add_argument("name", required=False, help="창고명")
post_parser.add_argument("note", required=False, help="비고")
post_parser.add_argument("created_by", required=False, help="등록자")
post_parser.add_argument("updated_by", required=False, help="수정자")

# /api/locations
@ns.route("")
class Locationlist(Resource):
    @ns.marshal_list_with(location, skip_none=True)
    def get(self):
        print("asdfasd")
        ''' 창고 복수 조회 '''
        data = LocationModel.query.filter(LocationModel.is_deleted !=True ).all()
        return data

    @ns.expect(post_parser)
    @ns.marshal_list_with(location, skip_none=True)
    def post(self):
        ''' 아이템 생성 '''        
        item = LocationModel()
        for key in request.get_json():
            setattr(item, key, request.get_json()[key])
        g.db.add(item)
        g.db.commit()
        return item, 201


# /api/locations/1
@ns.route("/<int:id>")
@ns.param("id", "창고ID")
class Location(Resource):
    @ns.marshal_list_with(location, skip_none=True)
    def get(self, id):
        ''' 창고 단수 조회 '''
        data = LocationModel.query.get_or_404(id)
        return data

    @ns.marshal_list_with(location, skip_none=True)
    @ns.expect(post_parser)
    def put(self, id):
        ''' 창고 업데이트 '''
        data = LocationModel.query.get(id)
        for item in request.get_json():
            setattr(data, item, request.get_json()[item])
        g.db.commit()
        return data

    @ns.marshal_list_with(location, skip_none=True)
    def delete(self, id):
        ''' 창고 safe delete '''
        data = LocationModel.query.filter(LocationModel.id == id).first()
        data.is_deleted = True
        g.db.commit()
        return data
