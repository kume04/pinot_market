from WarehouseManagement import db
from sqlalchemy import func
from .item import Item
class ItemOrder(db.Model):
    __tablename__ = 'item_order'
    id = db.Column(db.Integer, primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'), index=True, nullable=True)
    item = db.relationship('Item', backref='item_order',  primaryjoin="Item.id == ItemOrder.item_id")
    count = db.Column(db.Integer, default=0)
    title = db.Column(db.String(100), nullable=False)
    contents = db.Column(db.Text)
    created_by = db.Column(db.String(20))
    created_at = db.Column(db.DateTime(), default=func.now())
    is_deleted = db.Column(db.Boolean, nullable=False, default=False)