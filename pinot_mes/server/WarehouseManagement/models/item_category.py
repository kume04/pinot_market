from WarehouseManagement import db
from sqlalchemy import func

class ItemCategory(db.Model):
    __tablename__ = 'item_category'
    id = db.Column(db.Integer, primary_key=True)
    
    name = db.Column(db.String(20), nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('item_category.id'), index=True, nullable=True)
    parent = db.relationship(lambda: ItemCategory, remote_side=id, backref='sub_category')
    is_deleted = db.Column(db.Boolean, default=False)

 
  
   
    
     
       