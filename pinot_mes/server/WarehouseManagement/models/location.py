# item 창고 model
from WarehouseManagement import db
from sqlalchemy import func


class Location(db.Model):
    __tablename__ = 'location'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    note = db.Column(db.String(20))   
    created_at = db.Column(db.DateTime(), default=func.now())  
    is_deleted = db.Column(db.Boolean, nullable=False, default=False)
    category_id_list = db.Column(db.String, nullable=True)

