from WarehouseManagement import db
from sqlalchemy import func

class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(20))
    name = db.Column(db.String(20), nullable=False)
    category_id_list = db.Column(db.String, nullable=True)
    texture = db.Column(db.String(20), nullable=True)
    # standard = db.Column(db.String(20))
    specification_0 =  db.Column(db.String(20), nullable=True)
    specification_1 =  db.Column(db.String(20), nullable=True)
    specification_2 =  db.Column(db.String(20), nullable=True)
    unit = db.Column(db.String(20), nullable=True)
    isLocMng = db.Column(db.Boolean)
    has_barcode = db.Column(db.Boolean)
    purchase_price = db.Column(db.Integer, nullable=True)
    selling_price = db.Column(db.Integer, nullable=True)
    safety_stock= db.Column(db.Integer, nullable=True)
    note =  db.Column(db.String(20), nullable=True)
    created_by = db.Column(db.String(20))
    created_at = db.Column(db.DateTime(), default=func.now())
    updated_by = db.Column(db.String(20))
    updated_at = db.Column(db.DateTime(), default=func.now())
    is_deleted = db.Column(db.Boolean, nullable=False, default=False)
    
    transaction = db.relationship(
        'Transaction',
        backref='item',
        cascade='all',
        order_by='desc(Transaction.id)',
        primaryjoin="Item.id == Transaction.item_id"
    )
    