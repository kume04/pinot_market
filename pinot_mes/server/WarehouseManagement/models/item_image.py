from WarehouseManagement import db
from sqlalchemy import func

class Itemimage(db.Model):
    __tablename__ = "item_image"
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(200))
    is_deleted = db.Column(db.Boolean, default=False)
    item_id = db.Column(db.Integer, db.ForeignKey("item.id"), index=True)