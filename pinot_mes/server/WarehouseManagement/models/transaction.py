from WarehouseManagement import db
from sqlalchemy import func

class Transaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(20))
    kind = db.Column(db.String(20))
    partner_code = db.Column(db.String(20))
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'), index=True, nullable=True)
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'), index=True, nullable=True)
    incoming_qtty = db.Column(db.Integer)   
    outgoing_qtty = db.Column(db.Integer)  
    note = db.Column(db.String(20))
    created_by = db.Column(db.String(20))
    created_at = db.Column(db.DateTime(), default=func.now())
    updated_by = db.Column(db.String(20))
    updated_at = db.Column(db.DateTime(), default=func.now())
    is_deleted = db.Column(db.Boolean, nullable=False, default=False)
    partner_in =   db.Column(db.String(200))
    partner_out =   db.Column(db.String(200))
    price = db.Column(db.String(200))