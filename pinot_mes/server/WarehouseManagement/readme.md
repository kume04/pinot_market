# Flask WarehouseManagement Application
## Things Todo
- [ ] Item Registration
- [ ] Item Information Update
- [ ] Item Delete(Safe Delete)
- [ ] Transaction 
- [ ] Transaction Update


# Prequisite
## Windows
```
#in development mode
set FLASK_ENV=development #Auto Reload
set FLASK_APP=WarehouseManagement 
```
```
#in production mode
set FLASK_ENV=production 
set FLASK_APP=WarehouseManagement 
```

## MAC
```
confirm to psycopg2  and  brew install psycopg2-binary
brew install postgresql
export LDFLAGS="-L/usr/local/opt/openssl/lib"
export CPPFLAGS="-I/usr/local/opt/openssl/include"
pip3 install psycopg2
and 
pip3 install -r requirements.txt
```
```
#in development mode
export FLASK_ENV=development FLASK_APP=WarehouseManagement
```
```
#in production mode
export FLASK_ENV=production FLASK_APP=WarehouseManagement 
```