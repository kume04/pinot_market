from AuthAndControl import app as AuthAndControl
from InquiryAndProject import app as InquiryAndProject
from Partnership import app as Partnership
from WarehouseManagement import app as WarehouseManagement
from Question import app as Question
from Notice import app as Notice
from Facility import app as Facility
import threading

class Worker(threading.Thread):
    def __init__(self, app,port):
        super().__init__()
        self.app = app   
        self.port = port         # thread 이름 지정

    def run(self):
        self.app.run(host="0.0.0.0",port=self.port,use_reloader=False,threaded=True,debug=True)

APP_LIST = [ 
        { "app": AuthAndControl, "port":5010 },
        { "app": InquiryAndProject, "port":5001 },
        { "app": Partnership, "port":5002 },
        { "app": WarehouseManagement, "port":5003 },
        { "app": Question, "port":5004 },
        { "app": Notice, "port":5006 },
        { "app": Facility , "port":5009 }
    ]

for i in APP_LIST:

    t = Worker(i["app"],i["port"]) 
    t.start()
