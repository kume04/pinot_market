from django.contrib import admin
from .models import *
from django_json_widget.widgets import JSONEditorWidget
from django.contrib.postgres import fields
from prettyjson import PrettyJSONWidget
from django import forms
from django.db import models
from product.models import *
from shop_user.models import *

# Register your models here.


class Order(models.Model):
    ORDER_TYPE = (
        ("default", "주문 결제"),
        ("personal", "개인 결제")
    )
    order_type = models.CharField(choices=ORDER_TYPE, max_length=100)
    orderer = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField(blank=True, null=True)
    phone = models.CharField(max_length=100, blank=True, null=True)
    address_simple = models.CharField(max_length=300)
    address_detail = models.CharField(max_length=300)
    postal_code = models.CharField(max_length=100)
    ORDER_STATE = (
        ("cancel", "취소"),
        ("before-purchase", "결제 대기중"),
        ("pending", "배송 준비중"),
        ("delievering", "배송중"),
        ("complete", "배송완료"),
        ("refund-delievering", "환불-배송중"),
        ("refund-complete", "환불완료"),
        ("changing-delievering", "교환-배송중"),
        ("changing-complete", "교환완료")
    )
    status = models.CharField(choices=ORDER_STATE, max_length=100)
    used_point = models.IntegerField(default=0)
    total_price = models.IntegerField(default=0)
    iamport_id = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    custom_purchase = models.ForeignKey(
        "CustomPurchase", on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        if self.iamport_id is not None:
            return self.iamport_id
        else:
            return "개인 결제"


class OrderedProduct(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    order_product_detail = models.JSONField()
    delievery_type = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return "주문제품"

    def get_product_name(self):
        id = self.order_product_detail["original_product_id"]
        return ShopProduct.objects.get(id=id).name


class Refund(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateField(verbose_name="환불날짜",  auto_now_add=True)
    title = models.CharField(verbose_name="환불사유", max_length=200)
    price = models.IntegerField(verbose_name="공급가액")
    vat = models.IntegerField(verbose_name="부가세액")
    total = models.IntegerField(verbose_name="합계금액")


class CustomPurchase(models.Model):

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(verbose_name="개인 결제 이름", max_length=200)
    PURCHASE_STATE = (
        ("before-purchase", "결제 대기중"),
        ("before-send", "입금대기/확인중"),
        ("purchased", "결제완료"),
        ("creating", "제작중"),
        ("delievering", "배송중"),
        ("complete", "배송완료"),
    )
    thumbnail = models.ImageField(
        upload_to="custom_thumbnail/", blank=True, null=True)
    purchase_status = models.CharField(
        choices=PURCHASE_STATE, blank=True, null=True, max_length=30, default="before-purchase")
    price = models.IntegerField(verbose_name="공급가액")
    vat = models.IntegerField(verbose_name="부가세액")
    delivery_cost = models.IntegerField(verbose_name="배송비")
    total = models.IntegerField(verbose_name="합계금액")
    content = models.TextField(blank=True, null=True)
    created_at = models.DateField(verbose_name="입력 날짜",  auto_now_add=True)
    sender_name = models.CharField(max_length=50, blank=True, null=True,)
    sender_tel = models.CharField(max_length=50, blank=True, null=True,)
    sender_postal_code = models.CharField(
        max_length=50, blank=True, null=True,)
    sender_address_simple = models.CharField(
        max_length=500, blank=True, null=True,)
    sender_address_detail = models.CharField(
        max_length=500, blank=True, null=True,)
    deliver_type = models.CharField(max_length=50, blank=True, null=True,)
    comment = models.CharField(max_length=500, blank=True, null=True,)
    name = models.CharField(max_length=50, blank=True, null=True,)
    email = models.CharField(max_length=50, blank=True, null=True,)
    tel = models.CharField(max_length=50, blank=True, null=True,)
    phone = models.CharField(max_length=50, blank=True, null=True,)
    address_simple = models.CharField(max_length=50, blank=True, null=True,)
    address_detail = models.CharField(max_length=50, blank=True, null=True,)
    postal_code = models.CharField(max_length=50, blank=True, null=True,)
    iamport_id = models.CharField(max_length=50, blank=True, null=True,)
    payment_method = models.CharField(max_length=50, null=True, blank=True)
    proof_method = models.CharField(max_length=50, null=True, blank=True)
    purchased_at = models.DateField(verbose_name="결제 날짜",  auto_now_add=True)
