from django.contrib import admin
from .models import *
# Register your models here.
from django_summernote.admin import SummernoteModelAdmin


class JsonForm(forms.ModelForm):
    class Meta:
        model = OrderedProduct
        fields = '__all__'
        widgets = {
            'order_product_detail': JSONEditorWidget,
        }


@admin.register(Order)
class OrderAdmin(SummernoteModelAdmin):
    pass


@admin.register(OrderedProduct)
class OrderedProductAdmin(SummernoteModelAdmin):
    form = JsonForm


@admin.register(CustomPurchase)
class Admin(SummernoteModelAdmin):
    pass
