# Generated by Django 3.2.13 on 2022-05-29 16:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop_management', '0014_partnerinquiry_created_at'),
    ]

    operations = [
        migrations.RenameField(
            model_name='jumbotron',
            old_name='click_count',
            new_name='view_count',
        ),
    ]
