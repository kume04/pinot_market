"""pino_acryl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static
from . import views
urlpatterns = [
    path("", views.main),
    path("template", views.template),
    path("get_theme", views.get_theme),
    path("event", views.event),
    path("privacy", views.privacy),
    path("contract", views.contract),
    path("contact", views.contact),
    path("notice", views.notice),
    path("faq", views.faq),
    path("faq/<int:id>", views.faq_detail),
    path("event/detail/<int:id>", views.event_detail),
    path("notice/detail/<int:id>", views.notice_detail),
    path("partner", views.partner),
    path("get_zone", views.get_zone),
]
