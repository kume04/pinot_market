import math
from django.shortcuts import render
from product.models import *
from menu.models import *
from shop_management.models import *
from django.http import JsonResponse
from util import ecounterp
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.


def main(request):
    theme = Theme.objects.all()
    jumbotron = Jumbotron.objects.all().order_by("index")
    for jb in jumbotron:
        jb.view_count = jb.view_count+1
        jb.save()
    row_list = Category.objects.filter(parent_category=None)
    main_shortcut_list = MainShortcut.objects.all().order_by("index")
    return render(request, "shop_management/index.html",
                  {"main_shortcut_list": main_shortcut_list, "data": "data", "theme": theme, "row_list": row_list, "jumbotron": jumbotron})


def get_theme(request):
    theme_id = request.GET.get("id")
    product_list = ShopProduct.objects.filter(theme__id=theme_id)
    return render(request, "product/components/product.html", {"product_list": product_list})


def event(request):
    notices = Notice.objects.all().order_by("-id")
    return render(request, "shop_management/event.html", {"notices": notices})


def notice(request):
    per = request.GET.get("per")
    if per == "" or per == None:
        per = 20
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    per = int(per)
    page = int(page)
    total_page = math.ceil(Notice.objects.all().count() / per)
    notice_list = Notice.objects.all().order_by(
        "-created_at")[(page-1)*per:page*per]
    # if total_page == 1:

    return render(request, "shop_management/notice.html", {"notice_list": notice_list, "total_page": range(1, total_page+1)})


def notice_detail(request, id):

    notice = Notice.objects.get(id=id)
    notice.view_count = notice.view_count + 1
    notice.save()
    return render(request, "shop_management/notice_detail.html", {"notice": notice})


def faq(request):
    per = request.GET.get("per")
    if per == "" or per == None:
        per = 20
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    per = int(per)
    page = int(page)
    total_page = math.ceil(Faq.objects.all().count()/per)

    faq_list = Faq.objects.all().order_by("-created_at")[(page-1)*per:page*per]
    return render(request, "shop_management/faq.html", {"faq_list": faq_list, "total_page": range(1, total_page+1)})


def faq_detail(request, id):
    faq = Faq.objects.get(id=id)
    return render(request, "shop_management/faq_detail.html", {"faq": faq})


def privacy(request):
    return render(request, "shop_management/privacy.html")


def contract(request):
    return render(request, "shop_management/contract.html")


def contact(request):
    return render(request, "shop_management/contact.html")


def event_detail(request, id):
    notice = Notice.objects.get(id=id)
    return render(request, "shop_management/event_detail.html", {"notice": notice})


def template(request):
    return render(request, "shop_management/template.html")


def get_zone(request):
    ecounterp.write_order()
    # ecounterp.login()


def partner(request):
    if request.method == "GET":
        return render(request, "shop_management/partner.html",)
    else:
        print(request.FILES.get("file"))
        pi = PartnerInquiry()
        pi.company_name = request.POST.get("company_name")
        pi.manager_name = request.POST.get("manager_name")
        pi.tel = request.POST.get("tel")
        pi.email = request.POST.get("email")
        pi.title = request.POST.get("title")
        pi.contents = request.POST.get("contents")
        pi.file = request.FILES.get("file")
        pi.save()
        messages.info(request, '제휴 제안이 등록되었습니다. 검토후 연락드리겠습니다.')
        return HttpResponseRedirect('/')
