from django.contrib import admin
from .models import *
from django_summernote.admin import SummernoteModelAdmin
# Register your models here.

@admin.register(Notice)
class NoticeAdmin(SummernoteModelAdmin):
    pass

@admin.register(Jumbotron)
class JumbotronAdmin(SummernoteModelAdmin):
    pass

@admin.register(PartnerInquiry)
class PartnerInquiryAdmin(SummernoteModelAdmin):
    pass
