from pyexpat import model
from django.db import models
import datetime
from pytz import timezone as pytzone
# Create your models here.


class Notice(models.Model):
    thumbnail = models.FileField(upload_to="notice", blank=True, null=True)
    title = models.CharField(max_length=500)
    short_sub_title = models.CharField(max_length=600, blank=True, null=True)
    short = models.CharField(max_length=600, blank=True, null=True)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    view_count = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    def previous_notice(self):
        current_id = self.id
        prev = Notice.objects.filter(id__lt=current_id).order_by("-id").first()
        if prev:
            return {"title": prev.title, "id": prev.id}
        else:
            return {"title": "이전글이 없습니다.", "id": ""}

    def next_notice(self):
        current_id = self.id
        next = Notice.objects.filter(id__gt=current_id).order_by("id").first()
        if next:
            return {"title": next.title, "id": next.id}
        else:
            return {"title": "다음글이 없습니다.", "id": ""}

    def thumbnail_url(self):
        if self.thumbnail:
            return self.thumbnail.url
        else:
            return ""


class Inquiry(models.Model):
    title = models.CharField(max_length=500)
    author = models.CharField(max_length=500)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    phone = models.CharField(max_length=500)
    email = models.CharField(max_length=500)
    attached_file = models.FileField(upload_to="inquiry")
    is_locked = models.BooleanField(default=True)
    password = models.CharField(max_length=500)


class InquiryReply(models.Model):
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)


class SiteManagement(models.Model):
    company_logo = models.ImageField()
    company_info = models.TextField()
    caution = models.TextField()
    delivery_info = models.TextField()
    welecome_point = models.IntegerField()
    puchase_point = models.IntegerField()
    personal_info_regulation = models.TextField()
    agreement_regulation = models.TextField()


class Jumbotron(models.Model):
    name = models.CharField(max_length=500, blank=True, null=True)
    wide_img = models.ImageField(upload_to="jumbotron")
    small_img = models.ImageField(upload_to="jumbotron")
    mobile_img = models.ImageField(
        upload_to="jumbotron", blank=True, null=True)
    index = models.IntegerField(default=0)
    link = models.CharField(max_length=500, blank=True, null=True)
    view_count = models.IntegerField(default=0)

    def wide_img_url(self):
        if self.wide_img:
            return self.wide_img.url
        else:
            return ""

    def small_img_url(self):
        if self.small_img:
            return self.small_img.url
        else:
            return ""

    def mobile_img_url(self):
        if self.mobile_img:
            return self.mobile_img.url
        else:
            return ""

    def recent_click_count(self):
        target_date = datetime.datetime.now(
            pytzone('Asia/Seoul')) - datetime.timedelta(30)

        return self.jb.filter(created_at__gt=target_date).count()


class JumbotronClickCount(models.Model):
    jumbotron = models.ForeignKey(
        Jumbotron, on_delete=models.CASCADE, verbose_name="점보트론", blank=True, null=True, related_name="jb")
    ua = models.CharField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class Faq(models.Model):
    title = models.CharField(max_length=500)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    view_count = models.IntegerField(default=0)
    CATEGORY = (
        ("회원문의", "회원문의"),
        ("주문/결제", "주문/결제"),
        ("취소/교환/반품", "취소/교환/반품"),
        ("배송문의", "배송문의"),
        ("쿠폰/적립금", "쿠폰/적립금"),
        ("서비스 이용 및 기타", "서비스 이용 및 기타"),
    )
    category = models.CharField(
        choices=CATEGORY, max_length=30, blank=True, null=True)

    def __str__(self):
        return self.title

    def previous_faq(self):
        current_id = self.id
        prev = Faq.objects.filter(id__lt=current_id).order_by("-id").first()
        if prev:
            return {"title": prev.title, "id": prev.id}
        else:
            return {"title": "이전글이 없습니다.", "id": ""}

    def next_faq(self):
        current_id = self.id
        next = Faq.objects.filter(id__gt=current_id).order_by("id").first()
        if next:
            return {"title": next.title, "id": next.id}
        else:
            return {"title": "다음글이 없습니다.", "id": ""}


class PartnerInquiry(models.Model):
    company_name = models.CharField(max_length=100)
    manager_name = models.CharField(max_length=100)
    tel = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    title = models.CharField(max_length=300)
    contents = models.TextField()
    file = models.FileField(upload_to="partner_files/")
    created_at = models.DateTimeField(auto_now_add=True)
