from menu.models import *
from shop_admin.models import *
from shop_user.models import *


def categories_processor(request):
    # ip 저장

    AccessLog(
        ip=request.META.get("REMOTE_ADDR"), user_agent=request.META['HTTP_USER_AGENT'], target_url=request.path
    ).save()
    main_category = Category.objects.filter(
        parent_category=None).order_by("index")
    banner_list = Banner.objects.all()
    main_banner = Banner.objects.filter(type="common").first()

    return {'main_category': main_category, "banner_list": banner_list, "main_banner": main_banner}
