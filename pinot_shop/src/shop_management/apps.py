from django.apps import AppConfig


class ShopManagementConfig(AppConfig):
    name = 'shop_management'
