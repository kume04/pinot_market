from django.db import models

# Create your models here.

class SampleImageCategory0(models.Model):
    parent_category = models.ForeignKey(
        "SampleImageCategory0", blank=True, null=True, on_delete=models.CASCADE, verbose_name="부모 카테고리")
    name = models.CharField(max_length=100, blank=True,
                            null=True, verbose_name="카테고리 이름")
    thumbnail = models.ImageField(
        upload_to="sample_image_list",
        blank=True, null=True, verbose_name="카테고리 이미지")
    is_public = models.BooleanField(default=True)
    index = models.IntegerField(default=0)

    class Meta:
        ordering = ['parent_category_id', "index"]

    def __str__(self):
        if self.parent_category:
            return self.parent_category.name + ">" + self.name
        else:
            return self.name

    def html_name(self):
        if self.parent_category:
            return self.parent_category.name + "  >  " + self.name
        else:
            return self.name

    @property
    def image_url(self):
        if self.thumbnail and hasattr(self.thumbnail, 'url'):
            return self.thumbnail.url
        else:
            return ""

    @property
    def child_image(self):
        id_list = SampleImageCategory0.objects.filter(
            parent_category=self).values_list("id")
        image_list = SampleImage.objects.filter(category_id__in=id_list)
        return image_list[0:5]

    @property
    def image_count(self):
        return self.sampleimage_set.all().count()

class SampleImageCategory1(models.Model):
    parent_category = models.ForeignKey(
        "SampleImageCategory1", blank=True, null=True, on_delete=models.CASCADE, verbose_name="부모 카테고리")
    name = models.CharField(max_length=100, blank=True,
                            null=True, verbose_name="카테고리 이름")
    thumbnail = models.ImageField(
        upload_to="sample_image_list",
        blank=True, null=True, verbose_name="카테고리 이미지")
    is_public = models.BooleanField(default=True)
    index = models.IntegerField(default=0)

    class Meta:
        ordering = ['parent_category_id', "index"]

    def __str__(self):
        if self.parent_category:
            return self.parent_category.name + ">" + self.name
        else:
            return self.name

    def html_name(self):
        if self.parent_category:
            return self.parent_category.name + "  >  " + self.name
        else:
            return self.name

    @property
    def image_url(self):
        if self.thumbnail and hasattr(self.thumbnail, 'url'):
            return self.thumbnail.url
        else:
            return ""

    @property
    def child_image(self):
        id_list = SampleImageCategory1.objects.filter(
            parent_category=self).values_list("id")
        image_list = SampleImage.objects.filter(category_id__in=id_list)
        return image_list[0:5]

    @property
    def image_count(self):
        if self.sampleimagecategory1_set.all().count() > 0:
            id_list = []
            for item in self.category_set.all():
                for product in item.shopsampleimage_set.all().filter(is_show=True):
                    id_list.append(product.id)
            return len(set(id_list))
        else:
            return len(self.shopsampleimage_set.all().filter(is_show=True))



class SampleImage(models.Model):
    category0 = models.ForeignKey("SampleImageCategory0", blank=True, null=True, on_delete=models.CASCADE, verbose_name="이미지 형태 카테고리")
    category1 = models.ForeignKey("SampleImageCategory1", blank=True, null=True, on_delete=models.CASCADE, verbose_name="이미지 적용 형태 카테고리")

    image = models.FileField(upload_to="sample_image", blank=True, null=True)
    keyword = models.CharField( blank=True, null=True, max_length=100)
    view_count = models.IntegerField(default=0)
    sell_count= models.IntegerField(default=0)
    is_show= models.BooleanField(default=True)
    show_index = models.IntegerField(default=0)
    image_sample_id = models.CharField( blank=True, null=True, max_length=100)
    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
        else:
            return ""