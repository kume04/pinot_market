from django.shortcuts import render
from .models import *
from product.models import *
from django.db.models import Q
# Create your views here.
import math

def list(request, id=""):
    page=1
    if request.GET.get("page"):
        page = int(request.GET.get("page"))
    per_count = 20
    if request.GET.get("per_count"):
        per_count = int(request.GET.get("per_count"))
    order = "-id"
    if request.GET.get("order"):
        order = (request.GET.get("order"))
    category0_list = SampleImageCategory0.objects.all()
    category1_list = SampleImageCategory1.objects.filter(parent_category=None).order_by("index")
    category0 = request.GET.get("category0")
    category1 = request.GET.get("category1")
    category1_detail = request.GET.get("category1_detail")
    category1_detail_list = []
    # if category1:
    #     category1_detail_list = SampleImageCategory1.objects.filter(parent_category=category1).order_by("index")
    q = Q()
    if category0 :
        q &= Q(category0=category0)
    if category1 :
        q &= Q(category1=category1)
    # elif category1 :
    #     id_list = SampleImageCategory1.objects.filter(parent_category=category1).values_list("id")
    #     print(id_list)
    #     q |= Q(category1__in=id_list)
    print(q)
    img_list = SampleImage.objects.filter(q).order_by(order)[ (page-1)*per_count:page*per_count ]
    total = SampleImage.objects.filter(q).count()
    total_page = math.ceil(SampleImage.objects.filter(q).count() / per_count)+1
    product_id = request.GET.get("product_id")
    product = ""
    if product_id:
        product = ShopProduct.objects.get(id=product_id)

    return render(request, "sample_image/pages/list.html",
                  {"category1":category1,"category0":category0,"category1_detail":category1_detail,"category1_detail_list":category1_detail_list,
                      "category0_list":category0_list,"category1_list":category1_list,
                    "img_list":img_list,"product":product,"product_id":product_id,"order":order,"per_count":per_count,
                   "total":total,"total_page": range(1, total_page)})
