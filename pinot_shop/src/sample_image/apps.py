from django.apps import AppConfig


class SampleImageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sample_image'
