$(document).ready(function() {
    //출고 예상일 (현재 날짜 + 2일) 2022.08.06 변경  
    var duration = $("#data-duration").attr("data-duration")
    if(duration == "0"){
        duration=2
    }
    console.log(duration)
    var date = new Date();
    console.log(date)
    date.setDate(date.getDate() + parseInt(duration))
    console.log(date)
    var week = ['일', '월', '화', '수', '목', '금', '토'];
    var dayOfWeek = week[date.getDay()];
    if( dayOfWeek== "일" ){
        date.setDate( date.getDate() + 1 )
        dayOfWeek = "월"
    }else if(dayOfWeek== "토"){
        date.setDate( date.getDate() + 2 )
        dayOfWeek = "월"
    }
    $("#data-duration").text( (date.getMonth()+1) +"월"+date.getDate() +"일 ("+dayOfWeek+")요일" )

    // 썸네일 카루젤
    $(".thumb_img").on("click", function () {
        $(".cover_img").css("background-image", $(this).css("background-image"))
    })
    // 썸네일 카루젤 끝

    // share
    var share_btn_open = false
    $("#share_btn").on("click", function () {
        if(share_btn_open){
            $("#share_url").css("margin-left","0px")
            $("#share_kakao").css("margin-left","0px")
            share_btn_open = false
        }else{
            $("#share_url").css("margin-left","-140px")
            $("#share_kakao").css("margin-left","-70px")
            share_btn_open = true
        }
    })
    $("#share_url").on("click", function (){
        navigator.clipboard.writeText(location.href).then(function() {
          alert("상품 주소가 복사 되었습니다. 붙여넣기 해주세요");
        }, function(err) {
          console.error('Async: Could not copy text: ', err);
        });
    })
    $("#share_kakao").on("click", function (){
        // Kakao.init("3d39dda5a83d7291cf53108f9efa1777");
        // Kakao.Link.sendCustom({
        //     templateId: [templete id]
        // });
        // alert('공유되었습니다.')
        alert("카카오톡 공유는 준비중입니다.")
    })


    // 마우스 호버시 이미지 확대
    $(".main.cover_img").mousemove(function (e) {
        var x = e.pageX - $('.cover_img').offset().left - 100;
        var y = e.pageY - $('.cover_img').offset().top - 100;

        var image_x = (e.pageX - $('.cover_img').offset().left) * -2.5
        var image_y = (e.pageY - $('.cover_img').offset().top) * -2.5

        $("#zooming_window").css("display", "")
        $("#zooming_window").css("top", y)
        $("#zooming_window").css("left", x)
        $("#zooming_window").css("background-image", $(".cover_img").css("background-image"))
        $("#zooming_window").css("background-size", "1500px 1500px")
        $("#zooming_window").css("background-position", image_x + "px" + " " + image_y + "px")

    })
    $(".cover_img").mouseenter(function (e) {
        $("#zooming_window").css("display", "")
    })
    $(".cover_img").mouseout(function (e) {
        $("#zooming_window").css("display", "none")
    })
    // 마우스 호버시 이미지 확대 끝

    // 페이지네이션 초기화
    var pagination_num = 3
    var qna_list = []
    $(".pagination").eq(0).append("<ul class='flex justify-center mx-auto'></ul>")
    if( $(".qna_list > .qna_item").length > pagination_num){
        var temp = $(".qna_list > .qna_item").detach()
        qna_list = (qna_list.concat(temp))
        var page_num = Math.ceil(qna_list.length / pagination_num )
        for(var i = 1; i <= page_num; i++){
            // 페이지네이션 숫자 추가
            $(".pagination ul").eq(0).append("<li data-index='"+i+"' class='pagination_item cursor-pointer m-1 "+(i===1?"text-amber-500":"text-gray-500")+"'>"+i+"</li>")
        }
    }else{
        $(".pagination ul").eq(0).append("<li class='m-1 text-amber-500'>1</li>")
    }
    var qna_index = pagination_num
    while(qna_index>0){
         $(".qna_list ").prepend( qna_list[qna_index-1] )
        qna_index--
    }
    // 페이지네이션 초기화 끝

    // 페이지네이션 클릭시 화면 리렌더링
    $(document).on("click", ".pagination_item", function (){
        var page_index = parseInt($(this).attr("data-index"))
        qna_index = page_index * pagination_num
        $(".qna_list > .qna_item").detach()
        while(  qna_index > (page_index-1)*pagination_num  ){
            $(".qna_list ").prepend( qna_list[qna_index-1] )
            qna_index--
        }
        // 페이지네이션 색상 바꿔주기
        $(".pagination_item").removeClass("text-amber-500")
        $(".pagination_item").addClass("text-gray-500")
        $(this).removeClass("text-gray-500")
        $(this).addClass("text-amber-500")
    })
    // 페이지네이션 클릭시 화면 리렌더링 끝

    // 문의 내용 남기기
    $("#qna_submit").on("click", function () {
        if ($("#qna_title").val().trim() == "") {
            alert("문의 제목을 입력해주세요");
            return false
        }
        if ($("#qna_content").val().trim() == "") {
            alert("문의 내용을 입력해주세요");
            return false
        }
        $.ajax({
            type: "post",
            data: {
                title: $("#qna_title").val().trim(),
                content: $("#qna_content").val().trim(),
                id: $(".product_detail").eq(0).attr("data-id")
            },
            success: function (res) {
                alert("문의가 저장되었습니다. 빠른시일내에 연락드리겠습니다.")
                location.reload()
            }
        })
    })
    // 문의 내용 남기기 끝

    // 상품 문의 삭제
    $(document).on("click",".qna_delete", function (){
        var qna_id = $(this).attr("data-id")
        var product_id = $(".product_detail").eq(0).attr("data-id")
        $.ajax({
            url:"/question/"+product_id+"/"+qna_id,
            method:"delete",
            success:function(res){
                //TODO_SEHN: 모달 통일
                alert("문의가 정상적으로 삭제되었습니다.")
                location.reload()
            }
        })
    })
    // 상품 문의 삭제 끝

    //TODO_SEHN: 상품 문의 수정


    // 상품 문의 자세히 보기
    $(".qna_summary, .qna_title").on("click", function (e){
        $(this).parent().find(".qna_origin").removeClass("hidden")
        $(this).parent().find(".qna_summary").remove()
    })

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
    // setTimeout(function (){ $('#height').tooltip('show') } , 3000)
    $(document).on("blur","#width", function (){

        this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
        var dimension = ($("#width").attr("data-dimension"))
        var min_width = parseInt($("#width").attr("data-min-width"))
        var max_width = parseInt($("#width").attr("data-max-width"))

        console.log(parseInt($(this).val()))
        if( parseInt($(this).val()) < min_width ){
            $('#width').attr('title',"가로 입력 값은 "+min_width+"("+dimension+") 보다 커야 합니다.").attr('data-original-title',"가로는 "+min_width+"("+dimension+") 보다 커야 합니다.").tooltip('update')
              // $('[data-toggle="tooltip"]').tooltip()
            $('#width').tooltip('show')
            $('#width').val(min_width)
            setTimeout(function(){
                $('#width').tooltip('hide')
            },1000)
            
        }else if( parseInt($(this).val()) > max_width ) {
            $('#width').attr('title', "가로 입력 값은 " + max_width+"("+dimension+") 보다 작아야 합니다.").attr('data-original-title', "가로 입력 값은 " + max_width +"("+dimension+") 보다 작아야 합니다.").tooltip('update')
            // $('[data-toggle="tooltip"]').tooltip()
            $('#width').tooltip('show')
            $('#width').val(max_width)
            setTimeout(function(){
                $('#width').tooltip('hide')
            },1000)
        }else{
             $('#width').tooltip('hide')
        }
    })
   $(document).on("blur","#height", function (){
        this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
        var dimension = ($("#height").attr("data-dimension"))
        var min_height = parseInt($("#height").attr("data-min-height"))
        var max_height = parseInt($("#height").attr("data-max-height"))
        console.log(parseInt($(this).val()))
        if( parseInt($(this).val()) < min_height ){
            $('#height').attr('title',"세로 입력 값은 "+min_height+"("+dimension+") 보다 커야 합니다.").attr('data-original-title',"세로 입력 값은 "+min_height+"("+dimension+") 보다 커야 합니다.").tooltip('update')
              // $('[data-toggle="tooltip"]').tooltip()
            $('#height').tooltip('show')
            $('#height').val(min_height)
            setTimeout(function(){
                $('#height').tooltip('hide')
            },1000)
        }else if( parseInt($(this).val()) > max_height ) {
            $('#height').attr('title', "세로 입력 값은 " + max_height+"("+dimension+") 보다 작아야 합니다.").attr('data-original-title', "세로 입력 값은 " + max_height+"("+dimension+") 보다 작아야 합니다.").tooltip('update')
            // $('[data-toggle="tooltip"]').tooltip()
            $('#height').tooltip('show')
            $('#height').val(max_height)
            setTimeout(function(){
                $('#height').tooltip('hide')
            },1000)
        }else{
             $('#height').tooltip('hide')
        }
    })

    $(document).on("blur","#vertical", function (){
        this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
        var dimension = ($("#vertical").attr("data-dimension"))
        var min_vertical = parseInt($("#vertical").attr("data-min-vertical"))
        var max_vertical = parseInt($("#vertical").attr("data-max-vertical"))
        console.log(parseInt($(this).val()))
        if( parseInt($(this).val()) < min_vertical ){
            $('#vertical').attr('title',"높이 입력 값은 "+min_vertical+"("+dimension+") 보다 커야 합니다.").attr('data-original-title',"높이 입력 값은 "+min_vertical+"("+dimension+") 보다 커야 합니다.").tooltip('update')
              // $('[data-toggle="tooltip"]').tooltip()
            $('#vertical').tooltip('show')
            $('#vertical').val(min_vertical)
            setTimeout(function(){
                $('#vertical').tooltip('hide')
            },1000)
        }else if( parseInt($(this).val()) > max_vertical ) {
            $('#vertical').attr('title', "높이 입력 값은 " + max_vertical+"("+dimension+") 보다 작아야 합니다.").attr('data-original-title', "높이 입력 값은 " + max_vertical+"("+dimension+") 보다 작아야 합니다.").tooltip('update')
            // $('[data-toggle="tooltip"]').tooltip()
            $('#vertical').tooltip('show')
            $('#vertical').val(max_vertical)
            setTimeout(function(){
                $('#vertical').tooltip('hide')
            },1000)
        }else{
             $('#vertical').tooltip('hide')
        }
    })

})