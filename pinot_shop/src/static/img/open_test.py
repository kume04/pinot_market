import sys
import cv2

print("hello opencv" , cv2.__version__)

img = cv2.imread("product_detail.png")

if img is None:
    print("fail")
    sys.exit()

cv2.namedWindow("image")
cv2.imshow("image", img)
cv2.waitKey()

cv2.destroyAllWindows()