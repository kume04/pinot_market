from django.contrib import admin
from .models import *

# Register your models here.
from django_json_widget.widgets import JSONEditorWidget
from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget
from prettyjson import PrettyJSONWidget
from django import forms
from product.models import ShopProduct

# Register your models here.


class JsonForm(forms.ModelForm):
    class Meta:
        model = Basket
        fields = '__all__'
        widgets = {
            'product': JSONEditorWidget,
        }


@admin.register(Basket)
class BasketAdmin(admin.ModelAdmin):
    list_display = ["display_user"]
    form = JsonForm

    def display_user(self, obj):
        return obj.user

    def display_product(self, obj):
        return ShopProduct.objects.get(id=obj.product["original_product_id"]).name

    def display_total_price(self, obj):
        return str(obj.product["total_price"]) + "원"

    def display_print(self, obj):
        return str(obj.product["print_text"])

    def display_additional_product(self, obj):
        text = []
        try:
            for item in obj.product["additional_product"]:
                append_text = item["name"] + "*" + str(item["count"])
                text.append(append_text)
            if len(text) == 0:
                return "없음"
            else:
                return ",".join(text)
        except Exception as e:
            print(e)
            return "없음"

    def display_design(self, obj):
        return obj.product["design_text"]
        # return ""


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass
