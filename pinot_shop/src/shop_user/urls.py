from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('logout', views.logout_user),
    path('password_change/<str:token>', views.password_change),
    path("password_change_request", views.password_change_request),
    path('login', views.login_user),
    path('signup', views.signup),
    # path('signup/kakao',views.signup_kakao),
    path('signup/naver', views.signup_naver),
    path("duplicate_check", views.duplicate_check),
    path("email_duplicate_check", views.email_duplicate_check),
    path('mypage', views.mypage),
    path('find', views.find_account),
    path('refund', views.refund),
    path("order_history", views.order_history),
    path("delivery_list", views.delivery_list),
    path("address_list", views.address_list),
    path("ship_address_list", views.ship_address_list),
    path("modify_info", views.modify_info),
    path("modify_busi", views.modify_busi),
    path("ajax_password_change", views.ajax_password_change),
    path('basket', views.basket),
    path('design_list', views.design_list),
    path('payment', views.payment),
    path('ajax_payment', views.ajax_payment),
    path("custom_purchase", views.custom_purchase),
    path("custom_purchase_by_id/<int:id>", views.custom_purchase_by_id),
    path('mypage/add_basket', views.add_basket),
    path('mypage/modify_basket/<int:id>', views.modify_basket),
    path("delete_basket", views.delete_basket),
    path('inquiry_list', views.inquiry_list),
    path('add_inquiry', views.add_inquiry),
    path("resign", views.resign),
    path("custom_payment/<int:id>", views.custom_payment)
]
