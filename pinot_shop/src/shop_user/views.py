from datetime import datetime
from django.shortcuts import render, redirect
from django.http import JsonResponse
import json
from .models import *
from django.contrib.auth import login, authenticate, logout
from django.views.decorators.csrf import csrf_exempt
import random
import string
from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from review.models import *
from order.models import *
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from pytz import timezone as pytzone
# Create your views here.
import requests
import json
from django.db.models import Count, F, Value
import datetime
from dateutil.relativedelta import relativedelta


@login_required
def mypage(request):
    return render(request, "shop_user/mypage.html")


@login_required
def basket(request):

    basket = Basket.objects.filter(user=request.user).filter(is_private=False)
    return render(request, "shop_user/basket.html", {"basket": basket})


@login_required
def payment(request):
    basket = Basket.objects.filter(user=request.user)
    address_list = Address.objects.filter(user=request.user)
    ship_address_list = ShipAddress.objects.filter(user=request.user)
    return render(request, "shop_user/payment.html", {"basket": basket, "address_list": address_list, "ship_address_list": ship_address_list})


@login_required
def custom_payment(request, id):
    purchase = CustomPurchase.objects.get(id=id)
    address_list = Address.objects.filter(user=request.user)
    ship_address_list = ShipAddress.objects.filter(user=request.user)
    return render(request, "shop_user/custom_payment.html", {"purchase": purchase, "address_list": address_list, "ship_address_list": ship_address_list})


@login_required
@csrf_exempt
def delete_basket(request):
    # print(json.loads(request.body))
    id_list = ((request.POST.getlist('id[]')))
    Basket.objects.filter(id__in=id_list).delete()
    return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def add_basket(request):
    bs = Basket()
    bs.user_id = request.user.id
    bs.product = json.loads(request.body)
    bs.save()
    return JsonResponse({"result": bs.id})


@csrf_exempt
@login_required
def modify_basket(request, id=1):
    bs = Basket.objects.get(id=id)
    bs.file_0 = request.FILES.get("file_0")
    bs.file_1 = request.FILES.get("file_1")
    bs.file_2 = request.FILES.get("file_2")
    bs.created_at = timezone.now()
    bs.save()
    return JsonResponse({"result": ""})


@csrf_exempt
@login_required
def ajax_payment(request):
    if request.user.is_authenticated:
        p = Purchase()
        p.user = request.user
        p.sender_name = request.POST.get("sender_name")
        p.sender_tel = request.POST.get("sender_tel")
        p.sender_phone = request.POST.get("sender_phone")
        p.sender_postal_code = request.POST.get("sender_postal_code")
        p.sender_address_simple = request.POST.get("sender_address_simple")
        p.sender_address_detail = request.POST.get("sender_address_detail")
        p.comment = request.POST.get("comment")
        p.phone = request.POST.get("phone")
        p.address_simple = request.POST.get("address_simple")
        p.address_detail = request.POST.get("address_detail")
        p.postal_code = request.POST.get("postal_code")
        p.total_price = int(request.POST.get("total_price"))
        p.vat_price = int(request.POST.get("vat_price").replace(",", ""))
        p.supply_price = int(request.POST.get("supply_price").replace(",", ""))
        p.delivery_price = int(request.POST.get(
            "delivery_price").replace(",", ""))
        p.coupon_price = int(request.POST.get("coupon_price").replace(",", ""))

        p.iamport_id = request.POST.get("iamport_id")
        p.deliver_type = request.POST.get("deliver_type")
        p.name = request.POST.get("name")
        p.email = request.POST.get("email")
        p.tel = request.POST.get("tel")
        p.payment_method = request.POST.get("payment_method")
        p.proof_method = request.POST.get("proof_method")
        p.purchase_status = request.POST.get("purchase_status")

        p.save()
        u = User.objects.get(id=request.user.id)
        u.proof_email = request.POST.get("proof_email")
        u.tax_person = request.POST.get("tax_person")
        u.proof_tel = request.POST.get("proof_tel")
        u.proof_name = request.POST.get("proof_name")
        u.proof_company_name = request.POST.get("proof_company_name")
        u.proof_company_num = request.POST.get("proof_company_num")
        u.proof_file = request.FILES.get("proof_file")
        u.save()
        for item in Basket.objects.filter(user=request.user).filter(id__in=request.POST.get("id").split(",")):
            pItem = PurchaseItem()
            pItem.purchase = p
            pItem.product = item.product

            pItem.file_0 = item.file_0
            pItem.file_1 = item.file_1
            pItem.file_2 = item.file_2
            if item.product["original_product_id"]:
                ShopProduct.objects.filter(id=item.product["original_product_id"]).update(
                    sell_count=F('sell_count')+1)
                pItem.search_text = ShopProduct.objects.get(
                    id=item.product["original_product_id"]).name
            print(item.product["original_product_id"])

            pItem.save()

        #  pinot smart factory module 과 연계
        target_url = "http://13.124.52.145:5001/api/inquiries"
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        title = "[피노마켓|주문] "+User.objects.get(id=request.user.id).nickname+"님 "
        basket_count = Basket.objects.filter(user=request.user).filter(
            id__in=request.POST.get("id").split(",")).count()
        original_product_id = ""
        product_name = ""
        if Basket.objects.filter(user=request.user).filter(id__in=request.POST.get("id").split(",")).first().product["original_product_id"]:
            original_product_id = Basket.objects.filter(user=request.user).filter(
                id__in=request.POST.get("id").split(",")).first().product["original_product_id"]
            product_name = ShopProduct.objects.get(id=original_product_id).name
        else:
            product_name = Basket.objects.filter(user=request.user).filter(
                id__in=request.POST.get("id").split(",")).first().product["private_product_name"]
        if basket_count > 1:
            ShopProduct.objects.get(id=original_product_id).name
            title = title + product_name + " 외 " + str(basket_count - 1)+"건 주문"
        else:
            title = title + product_name + " 주문"

        data = {
            'client_company_name': "[피노마켓]"+User.objects.get(id=request.user.id).nickname,
            'client_person': "[피노마켓]"+User.objects.get(id=request.user.id).nickname,
            "client_person_tel": User.objects.get(id=request.user.id).phone,
            "content": "<span>피노마켓 주문</span>",
            "received_at": p.created_at,
            "project": [
                {
                    "project_name": title,
                    "content": "<span>피노마켓 관리화면 참고</span>",
                    "count": basket_count
                }
            ]
        }
        res = requests.post(target_url, data=json.dumps(
            data, default=str), headers=headers)

        Basket.objects.filter(user=request.user).filter(
            id__in=request.POST.get("id").split(",")).delete()

        return JsonResponse({"result": "ok"})


@csrf_exempt
def login_user(request):
    # print(request.method)

    if request.method == "POST":
        if request.POST.get("type") == "naver_login":
            if User.objects.filter(username=request.POST.get("email")).count() > 0:
                # 이미 네이버로 로그인한 아이디가 있다면
                user = User.objects.filter(username=request.POST.get("email"))
                user.update(etc_field=request.POST['token'])
                login(request, user[0])
                return JsonResponse({"result": "ok"})
            else:
                user = User(
                    sns_login_type="naver",
                    nickname=request.POST.get("name"),
                    username=request.POST.get("email"),
                    phone=request.POST.get("phone"),
                    etc_field=request.POST['token']
                ).save()
                login(request, user)
                return JsonResponse({"result": "first"})
        elif request.POST.get("type") == "kakao_login":
            if User.objects.filter(username=request.POST.get("email")).count() > 0:
                # 이미 카카오 로그인한 아이디가 있다면
                user = User.objects.filter(username=request.POST.get("email"))
                user.update(etc_field=request.POST['token'])
                login(request, user[0])
                return JsonResponse({"result": user[0].nickname})
            else:
                # 없는 경우 바로 db 생성하고 로그인
                user = User(
                    sns_login_type="kakao",
                    nickname=request.POST.get("name"),
                    username=request.POST.get("email"),
                    phone=request.POST.get("phone"),
                    etc_field=request.POST['token']
                ).save()
                login(request, user)
                return JsonResponse({"result": request.POST.get("name")})
        else:
            # 로그인 체크
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return JsonResponse({"result": "ok"})
            elif User.objects.filter(username=username).count() > 0:
                return JsonResponse({"result": "wrong"})
            else:
                return JsonResponse({"result": "none"})
    return render(request, "shop_user/login.html")


@csrf_exempt
def signup(request):
    if request.method == "POST":
        user = User(
            nickname=request.POST.get("name"),
            username=request.POST.get("id"),
            phone=request.POST.get("tel"),
            address_simple=request.POST.get("address_0"),
            address_detail=request.POST.get("address_1"),
            postal_code=request.POST.get("postal_code"),
            password=request.POST.get("password1"),
            favorite_product=request.POST.get("favorite_product"),
            proof_name=request.POST.get("proof_name"),
            company_type=request.POST.get("company_type"),
            proof_file=request.FILES.get("resume"),
            is_certified=False
        ).save()

        user = User.objects.get(username=request.POST.get("id"))
        user.set_password(request.POST.get("password1"))
        user.save()
        return redirect("/?signup=success")
    return render(request, "shop_user/signup.html")


@csrf_exempt
def signup_naver(request):
    if request.method == "POST":
        # 네이버 로그인이 처음이라면
        user = User(
            email=request.POST.get("email"),
            username=request.POST.get("id"),
            phone=request.POST.get("tel"),
            address_simple=request.POST.get("address_0"),
            address_detail=request.POST.get("address_1"),
            postal_code=request.POST.get("postal_code"),
            sns_login_type="naver",
            etc_field=request.POST['token']
        ).save()
        user = User.objects.get(username=request.POST.get("id"))
        login(request, user)
        return redirect("/?signup_naver=success")
    return render(request, "shop_user/signup_naver.html")


@csrf_exempt
def duplicate_check(request):
    username = request.GET.get("id")

    if User.objects.filter(username=username).count() > 0:
        return JsonResponse({"result": "duplicated"})
    else:
        return JsonResponse({"result": "not duplicated"})


@csrf_exempt
def email_duplicate_check(request):
    email = request.GET.get("email")
    if User.objects.filter(email=email).count() > 0:
        return JsonResponse({"result": "duplicated"})
    else:
        return JsonResponse({"result": "not duplicated"})


@csrf_exempt
def logout_user(request):
    logout(request)
    return redirect("/")


@csrf_exempt
def order_history(request):
    d1 = datetime.datetime.today()
    selected = request.GET.get("selected")
    date_from = request.GET.get("date_from")
    if date_from == "all":
        date_from = "2021-07-26"
        date_from = datetime.datetime.strptime(date_from, '%Y-%m-%d')
    if date_from == None or date_from == "-1m":
        date_from = d1 - relativedelta(months=1)
        selected = "2"
    if date_from == "-3m":
        date_from = d1 - relativedelta(months=3)
    if date_from == "-6m":
        date_from = d1 - relativedelta(months=6)
    date_to = request.GET.get("date_to")
    if date_to == None:
        date_to = str(datetime.datetime.now()).split(" ")[0]
    date_to = datetime.datetime.strptime(
        date_to, '%Y-%m-%d') + relativedelta(days=1)
    order_list = Purchase.objects.filter(user=request.user).filter(created_at__gte=date_from).filter(
        created_at__lte=date_to).filter(purchaseitem__search_text__icontains=request.GET.get("q", "")).order_by("-created_at")

    return render(request, "shop_user/order_history.html", {"order_list": order_list, "q": request.GET.get("q"), "selected": request.GET.get("selected")})


@csrf_exempt
@login_required
def delivery_list(request):
    d1 = datetime.datetime.today()
    selected = request.GET.get("selected")
    date_from = request.GET.get("date_from")
    if date_from == "all":
        date_from = "2021-07-26"
        date_from = datetime.datetime.strptime(date_from, '%Y-%m-%d')
    if date_from == None or date_from == "-1m":
        date_from = d1 - relativedelta(months=1)
        selected = "2"
    if date_from == "-3m":
        date_from = d1 - relativedelta(months=3)
    if date_from == "-6m":
        date_from = d1 - relativedelta(months=6)
    date_to = request.GET.get("date_to")
    if date_to == None:
        date_to = str(datetime.datetime.now()).split(" ")[0]
    date_to = datetime.datetime.strptime(
        date_to, '%Y-%m-%d') + relativedelta(days=1)
    delivery_list = Purchase.objects.filter(user=request.user).filter(created_at__gte=date_from).filter(
        created_at__lte=date_to).filter(purchaseitem__search_text__icontains=request.GET.get("q", "")).distinct().order_by("-created_at")
    print(delivery_list)
    return render(request, "shop_user/delivery_list.html", {"delivery_list": delivery_list, "q": request.GET.get("q"), "selected": selected})


@csrf_exempt
@login_required
def modify_info(request):
    if request.method == "POST":
        user = User.objects.get(id=request.user.id)
        user.nickname = request.POST.get("nickname")
        user.email = request.POST.get("email")
        user.phone = request.POST.get("tel")
        user.address_simple = request.POST.get("address_0")
        user.address_detail = request.POST.get("address_1")
        user.postal_code = request.POST.get("postal_code")
        user.proof_sns = request.POST.get("proof_sns")
        user.favorite_product = request.POST.get("favorite_product")
        user.proof_name = request.POST.get("proof_name")
        user.company_type = request.POST.get("company_type")
        print(request.POST.get("marketing_mail"))
        if request.POST.get("marketing_mail"):
            user.marketing_mail = True
        else:
            user.marketing_mail = False
        if request.POST.get("marketing_sms"):
            user.marketing_sms = True
        else:
            user.marketing_sms = False

        if request.FILES.get("change_resume"):
            user.proof_file = request.FILES.get("change_resume")
        user.save()
        return redirect("/account/modify_info?modify=success")
    return render(request, "shop_user/modify_info.html")


@csrf_exempt
@login_required
def modify_busi(request):
    if request.method == "POST":
        user = User.objects.get(id=request.user.id)
        user.proof_sns = request.POST.get("proof_sns")
        user.favorite_product = request.POST.get("favorite_product")
        user.proof_name = request.POST.get("proof_name")
        user.proof_email = request.POST.get("proof_email")
        user.tax_person = request.POST.get("tax_person")
        user.tax_person_tel = request.POST.get("tax_person_tel")
        user.company_type = request.POST.get("company_type")

        user.marketing_mail = request.POST.get("marketing_mail")
        user.marketing_sms = request.POST.get("marketing_sms")
        print(request.POST.get("marketing_mail"))
        if request.FILES.get("change_resume"):
            user.proof_file = request.FILES.get("change_resume")
        user.save()
        return redirect("/account/modify_busi?modify=success")
    return render(request, "shop_user/modify_busi.html")


@csrf_exempt
def ajax_password_change(request):
    if request.method == "POST":
        # 로그인 체크
        user_id = request.POST['id']
        user = User.objects.get(id=user_id)

        if user is not None:
            token = PasswordChangeToken()
            token.user = user
            token.token = "".join(random.choice(
                string.ascii_uppercase + string.digits) for _ in range(10))
            token.save()
            htmly = render_to_string(
                'shop_user/components/password_change_email.html', {"token": token.token})
            print(mail.send_mail("[피노]비밀번호 변경 안내", strip_tags(
                htmly), ' pinot <market@pinotmarket.co.kr>', [user.username], html_message=htmly))

            return JsonResponse({"result": "ok"})
        else:
            return JsonResponse({"result": "none"})


@csrf_exempt
def password_change(request, token):
    print(datetime.datetime.now(pytzone('Asia/Seoul')))
    if (datetime.datetime.now(pytzone('Asia/Seoul')) - PasswordChangeToken.objects.get(token=token).created_at).days > 30:
        user = PasswordChangeToken.objects.get(token=token).user
        return render(request,  "shop_user/password_change.html", {"token": token, "user": user, "fresh": False})
    else:
        user = PasswordChangeToken.objects.get(token=token).user
        return render(request,  "shop_user/password_change.html", {"token": token, "user": user, "fresh": True})


@csrf_exempt
def password_change_request(request):

    user = PasswordChangeToken.objects.get(
        token=request.POST.get("token")).user

    user.set_password(request.POST.get("password"))
    user.save()
    return JsonResponse({"result": "ok"})


@csrf_exempt
def find_account(request):
    if request.method == "POST":
        if request.POST.get("type") == "find":
            # 아이디 찾기
            print(User.objects.filter(nickname=request.POST.get("name")))
            print(User.objects.filter(nickname=request.POST.get(
                "name")).filter(phone=request.POST.get("phone")))
            if User.objects.filter(nickname=request.POST.get("name")).filter(phone=request.POST.get("phone")).count() > 0:
                if User.objects.filter(nickname=request.POST.get("name")).filter(phone=request.POST.get("phone")).first().sns_login_type == "naver":
                    # 네이버로 로그인 한 경우
                    return JsonResponse({"result": "해당 계정은 <b>네이버로그인</b>을 하였습니다. 네이버 로그인을 해주세요."})
                else:
                    username = User.objects.filter(nickname=request.POST.get("name")).filter(
                        phone=request.POST.get("phone")).first().username
                    username = username.split(
                        "@")[0][0:2]+(len(username.split("@")[0])-2)*"*" + "@" + username.split("@")[1]
                    return JsonResponse({"result": "가입된 아이디는&nbsp;&nbsp;<b>"+username+"</b>&nbsp;&nbsp;입니다."})
            else:
                return JsonResponse({"result": "해당 정보와 일치하는 계정이 없습니다."})
        else:
            # 비밀번호 변경 이메일
            if User.objects.filter(nickname=request.POST.get("name")).filter(username=request.POST.get("email")).count() > 0:
                if User.objects.filter(nickname=request.POST.get("name")).filter(username=request.POST.get("email")).first().sns_login_type == "naver":
                    # 네이버로 로그인 한 경우
                    return JsonResponse({"result": "해당 계정은 <b>네이버로그인</b>을 하였습니다. 네이버 로그인을 해주세요."})
                else:
                    user = User.objects.filter(nickname=request.POST.get("name")).filter(
                        username=request.POST.get("email")).first()
                    user_email = user.username
                    token = PasswordChangeToken()
                    token.user = user
                    token.token = "".join(random.choice(
                        string.ascii_uppercase + string.digits) for _ in range(10))
                    token.save()
                    htmly = render_to_string(
                        'shop_user/components/password_change_email.html', {"token": token.token})
                    print(mail.send_mail("[피노]비밀번호 변경 안내", strip_tags(
                        htmly), 'pinot <market@pinotmarket.co.kr>', [user.username], html_message=htmly))
                    return JsonResponse({"result": "비밀번호 변경에 대한 안내를 <b>등록된 이메일</b>로 보냈습니다. 확인해주세요.", "data-id": token.token[::-1]})
            else:
                return JsonResponse({"result": "해당 정보와 일치하는 계정이 없습니다."})
    else:
        return render(request, "shop_user/find_account.html")


@csrf_exempt
@login_required
def inquiry_list(request):
    inqury_list = Inquiry.objects.all().order_by("-created_at")
    return render(request, "shop_user/inquiry_list.html", {"inqury_list": inqury_list})


@csrf_exempt
def add_inquiry(request):
    Inquiry(
        author=request.user,
        title=request.POST.get("title"),
        content=request.POST.get("content"),
        inquiry_attached_file=request.FILES.get("inquiry_attached_file"),
        reply="답변 준비중입니다.",
        status=0
    ).save()
    return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def design_list(request):
    date_from = request.GET.get("date_from")
    if date_from == "all":
        date_from = "2021-07-26"
    if date_from == None:
        date_from = "2021-07-26"

    date_from = datetime.datetime.strptime(date_from, '%Y-%m-%d')
    date_to = request.GET.get("date_to")
    if date_to == None:
        date_to = str(datetime.datetime.now()).split(" ")[0]
    date_to = datetime.datetime.strptime(
        date_to, '%Y-%m-%d') + datetime.timedelta(days=1)
    design_list = PurchaseItem.objects.filter(purchase__user=request.user).filter(
        product__icontains='"type": "design"').filter(created_at__gte=date_from).filter(created_at__lte=date_to)
    print(date_to)
    return render(request, "shop_user/design_list.html", {"design_list": design_list, "q": request.GET.get("q"), "selected": request.GET.get("selected")})


@csrf_exempt
@login_required
def refund(request):
    if request.method == "POST":
        request.user.refund_bank = request.POST.get("refund_bank")
        request.user.refund_account = request.POST.get("refund_account")
        request.user.nickname = request.POST.get("nickname")
        request.user.save()
        refund = Refund.objects.filter(user=request.user)
        return render(request, "shop_user/refund.html", {"refund": refund})
    else:
        refund = Refund.objects.filter(user=request.user)

        return render(request, "shop_user/refund.html", {"refund": refund})


@csrf_exempt
def resign(request):
    if request.method == "POST":
        u = User.objects.get(id=request.user.id)
        user = authenticate(username=u.username,
                            password=request.POST.get("pw"))
        if user is not None:
            u.is_deleted = True
            u.save()
            return JsonResponse({"result": "ok"})
        else:
            return JsonResponse({"result": "invalid password"})
    else:
        return render(request, "shop_user/resign.html",)


@login_required
def custom_purchase(request):
    custom_purchase_list = Purchase.objects.filter(user=request.user).filter(purchaseitem__is_private=True)
    return render(request, "shop_user/custom_purchase.html", {"custom_purchase_list": custom_purchase_list})


@login_required
def custom_purchase_by_id(request, id):
    custom_purchase = CustomPurchase.objects.filter(id=id).values()
    return JsonResponse({"result": list(custom_purchase)})


@csrf_exempt
@login_required
def address_list(request):
    if request.method == "POST":
        if request.POST.get("type") == "get":
            return JsonResponse({"result": list(Address.objects.filter(id=request.POST.get("id")).values())})
        if request.POST.get("type") == "delete":
            return JsonResponse({"result": list(Address.objects.get(id=request.POST.get("id")).delete())})

        if request.POST.get("type") == "post":
            if request.POST.get("id") == "undefined" or request.POST.get("id") == "":
                address = Address()
            else:
                address = Address.objects.get(id=request.POST.get("id"))
            address.user = request.user
            address.name = request.POST.get("name")
            address.postal_code = request.POST.get("postal_code")
            address.simple = request.POST.get("simple")
            address.detail = request.POST.get("detail")
            address.save()
            return JsonResponse({"result": ""})

    else:
        address_list = Address.objects.filter(user=request.user)
        return render(request, "shop_user/address_list.html", {"address_list": address_list})


@csrf_exempt
@login_required
def ship_address_list(request):
    if request.method == "POST":
        if request.POST.get("type") == "get":
            return JsonResponse({"result": list(ShipAddress.objects.filter(id=request.POST.get("id")).values())})
        if request.POST.get("type") == "delete":
            return JsonResponse({"result": list(ShipAddress.objects.get(id=request.POST.get("id")).delete())})

        if request.POST.get("type") == "post":
            if request.POST.get("id") == "undefined" or request.POST.get("id") == "":
                address = ShipAddress()
            else:
                address = ShipAddress.objects.get(id=request.POST.get("id"))
            address.user = request.user
            address.name = request.POST.get("name")
            address.postal_code = request.POST.get("postal_code")
            address.simple = request.POST.get("simple")
            address.detail = request.POST.get("detail")
            address.receiver_name = request.POST.get("receiver_name")
            address.receiver_email = request.POST.get("receiver_email")
            address.receiver_tel = request.POST.get("receiver_tel")
            address.receiver_phone = request.POST.get("receiver_phone")
            address.save()
            return JsonResponse({"result": ""})

    else:
        address_list = ShipAddress.objects.filter(user=request.user)
        return render(request, "shop_user/ship_address_list.html", {"address_list": address_list})
