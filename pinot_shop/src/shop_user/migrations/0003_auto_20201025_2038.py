# Generated by Django 3.1 on 2020-10-25 11:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop_user', '0002_basket_is_payed'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='address_detail',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='address_simple',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='postal_code',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
