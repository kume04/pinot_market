# Generated by Django 3.1 on 2021-07-21 12:51

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop_user', '0021_auto_20210717_2006'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShipAddress',
            fields=[
                ('id', models.AutoField(auto_created=True,
                 primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('simple', models.CharField(blank=True, max_length=300, null=True)),
                ('detail', models.CharField(blank=True, max_length=300, null=True)),
                ('postal_code', models.CharField(
                    blank=True, max_length=100, null=True)),
                ('user', models.ForeignKey(
                    on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
