# Generated by Django 3.1 on 2021-07-13 13:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop_user', '0019_customer_customertag'),
    ]

    operations = [
        migrations.AddField(
            model_name='basket',
            name='file_0',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='basket',
            name='file_1',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='basket',
            name='file_2',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
    ]
