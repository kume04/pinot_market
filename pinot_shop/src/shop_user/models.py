from product.models import ShopProduct
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

# Create your models here.


class UserManager(BaseUserManager):

    use_in_migrations = True

    def create_user(self, username, password=None):

        if not username:
            raise ValueError('must have user username(email)')
        user = self.model(
            email=self.normalize_email(username),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, password, username):
        user = self.create_user(
            username=username,
            password=password
        )
        user.is_admin = True
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()
    username = models.EmailField(
        max_length=255, unique=True
    )
    nickname = models.CharField(
        max_length=20,
        null=True, blank=True
    )
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    # 기업 인증 여부
    is_certified = models.BooleanField(default=False)
    phone = models.CharField(max_length=100, null=True, blank=True)
    address_simple = models.CharField(max_length=300, null=True, blank=True)
    address_detail = models.CharField(max_length=300, null=True, blank=True)
    postal_code = models.CharField(max_length=100, null=True, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    refund_account_owner = models.CharField(
        max_length=300, null=True, blank=True)
    refund_account = models.CharField(max_length=300, null=True, blank=True)
    refund_bank = models.CharField(max_length=50, null=True, blank=True)
    sns_login_type = models.CharField(max_length=50, null=True, blank=True)
    etc_field = models.CharField(max_length=500, null=True, blank=True)
    # 인증에 대한 부분
    proof_file = models.FileField(blank=True, null=True)
    proof_email = models.CharField(blank=True, null=True, max_length=100)
    proof_tel = models.CharField(blank=True, null=True, max_length=100)
    proof_name = models.CharField(blank=True, null=True, max_length=100)
    company_type = models.CharField(blank=True, null=True, max_length=100)
    is_deleted = models.BooleanField(default=False)
    proof_company_name = models.CharField(
        blank=True, null=True, max_length=100)
    proof_company_num = models.CharField(blank=True, null=True, max_length=100)
    proof_sns = models.TextField(blank=True, null=True)
    tax_person = models.CharField(blank=True, null=True, max_length=100)
    tax_person_tel = models.CharField(blank=True, null=True, max_length=100)
    favorite_product = models.CharField(blank=True, null=True, max_length=200)
    USERNAME_FIELD = 'username'
    marketing_mail = models.BooleanField(default=True, blank=True, null=True,)
    marketing_sms = models.BooleanField(default=True, blank=True, null=True,)

    def __str__(self):
        return self.username

    def proof_file_url(self):
        if self.proof_file:
            return self.proof_file.url
        else:
            return ""


class Basket(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.JSONField()
    file_0 = models.FileField(blank=True, null=True, upload_to="basket")
    file_1 = models.FileField(blank=True, null=True, upload_to="basket")
    file_2 = models.FileField(blank=True, null=True, upload_to="basket")
    created_at = models.DateTimeField(auto_now_add=True)
    is_private = models.BooleanField(default=False)

    def get_additional_product(self):
        text = []
        try:
            for item in self.product["additional_product"]:
                append_text = item["name"] + "*" + str(item["count"])
                text.append(append_text)
            if len(text) == 0:
                return "없음"
            else:
                return "["+"] [".join(text)+"]"
        except Exception as e:

            return "없음"

    def get_product_image(self):
        product = ShopProduct.objects.get(
            id=self.product["original_product_id"])
        img = product.shopproductimage_set.all().first()
        return img.image.url


class PointLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    point = models.IntegerField(default=0)
    point_message = models.CharField(max_length=1000)


class PasswordChangeToken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)
    resolved_at = models.DateTimeField(blank=True, null=True)


class Address(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, null=True, blank=True)
    simple = models.CharField(max_length=300, null=True, blank=True)
    detail = models.CharField(max_length=300, null=True, blank=True)
    postal_code = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name


class ShipAddress(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, null=True, blank=True)
    receiver_name = models.CharField(max_length=100, null=True, blank=True)
    receiver_email = models.CharField(max_length=100, null=True, blank=True)
    receiver_tel = models.CharField(max_length=100, null=True, blank=True)
    receiver_phone = models.CharField(max_length=100, null=True, blank=True)
    simple = models.CharField(max_length=300, null=True, blank=True)
    detail = models.CharField(max_length=300, null=True, blank=True)
    postal_code = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    code = models.CharField(max_length=50)  # 거래처 코드
    name = models.CharField(max_length=50)  # 이름
    email = models.CharField(max_length=50)  # 이메일
    local = models.CharField(max_length=50)  # 지역 구분
    payment = models.CharField(max_length=50)  # 결제방식
    address_0 = models.CharField(max_length=500)  # 주소
    address_1 = models.CharField(max_length=500)  # 세부주소
    president = models.CharField(max_length=50)  # 대표자 명
    phone = models.CharField(max_length=50)  # 대표 폰
    tel = models.CharField(max_length=50)  # 대표 전화
    fax = models.CharField(max_length=50)  # 대표 팩스
    type = models.CharField(max_length=50)  # 업태
    kind = models.CharField(max_length=50)  # 업종
    category = models.CharField(max_length=50)  # 고객 분류
    comment = models.CharField(max_length=50)  # 회사 특징
    tag = models.ManyToManyField("CustomerTag")  # 관련 태그

    def __str__(self):
        return self.name


class CustomerTag(models.Model):
    name = models.CharField(max_length=50)  # 태그 이름

    def __str__(self):
        return super().name


class Purchase(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, blank=True, null=True)
    sender_name = models.CharField(max_length=50, blank=True, null=True,)
    sender_tel = models.CharField(max_length=50, blank=True, null=True,)
    sender_phone = models.CharField(max_length=50, blank=True, null=True,)

    sender_postal_code = models.CharField(
        max_length=50, blank=True, null=True,)
    sender_address_simple = models.CharField(
        max_length=500, blank=True, null=True,)
    sender_address_detail = models.CharField(
        max_length=500, blank=True, null=True,)
    deliver_type = models.CharField(max_length=50, blank=True, null=True,)
    comment = models.CharField(max_length=500, blank=True, null=True,)
    name = models.CharField(max_length=50, blank=True, null=True,)
    email = models.CharField(max_length=50, blank=True, null=True,)
    tel = models.CharField(max_length=50, blank=True, null=True,)
    phone = models.CharField(max_length=50, blank=True, null=True,)
    address_simple = models.CharField(max_length=50, blank=True, null=True,)
    address_detail = models.CharField(max_length=50, blank=True, null=True,)
    postal_code = models.CharField(max_length=50, blank=True, null=True,)

    iamport_id = models.CharField(max_length=50, blank=True, null=True,)
    created_at = models.DateTimeField(auto_now_add=True)
    payment_method = models.CharField(max_length=50, null=True, blank=True)
    proof_method = models.CharField(max_length=50, null=True, blank=True)

    total_price = models.CharField(max_length=50, blank=True, null=True,)
    vat_price = models.CharField(max_length=50, blank=True, null=True,)
    supply_price = models.CharField(max_length=50, blank=True, null=True,)
    delivery_price = models.CharField(max_length=50, blank=True, null=True,)
    coupon_price = models.CharField(max_length=50, blank=True, null=True,)

    ORDER_STATE = (
        ("before-withdraw", "입금 대기중"),
        ("after-withdraw", "결제 완료"),
        ("creating", "상품 준비중"),
        ("delivering", "출고 완료"),
        ("cancel", "취소"),
    )
    purchase_status = models.CharField(
        choices=ORDER_STATE, max_length=100, blank=True, null=True, default="creating")
    deliver = models.CharField(max_length=50, blank=True, null=True,)
    deliver_num = models.CharField(max_length=50, blank=True, null=True, )

    def get_purchase_name(self):
        if self.purchaseitem_set.all().count() > 0:
            if (self.purchaseitem_set.all().count()) > 1:
                if self.purchaseitem_set.all()[0].product["original_product_id"]:
                    p = ShopProduct.objects.get(id=self.purchaseitem_set.all()[
                                                0].product["original_product_id"])
                    return p.name + "외 " + str(self.purchaseitem_set.all().count() - 1) + "건"
                else:
                    p = ShopProduct.objects.get(id=self.purchaseitem_set.all()[
                                                0].product["private_product_name"])
                    return "<b>[개인결제]</b>"+p + "외 " + str(self.purchaseitem_set.all().count() - 1) + "건"
            else:
                if self.purchaseitem_set.all()[0].product["original_product_id"]:
                    p = ShopProduct.objects.get(id=self.purchaseitem_set.all()[
                                                0].product["original_product_id"])
                    return p.name
                else:
                    p = self.purchaseitem_set.all(
                    )[0].product["private_product_name"]
                    return "<b>[개인결제]</b>"+p


class PurchaseItem(models.Model):
    purchase = models.ForeignKey(
        Purchase, on_delete=models.CASCADE, blank=True, null=True,)
    product = models.JSONField()
    file_0 = models.FileField(blank=True, null=True, upload_to="basket")
    file_1 = models.FileField(blank=True, null=True, upload_to="basket")
    file_2 = models.FileField(blank=True, null=True, upload_to="basket")
    DESIGN_STATUS = (
        ("디자인 처리중", "디자인 처리중"),
        ("디자인 입력", "디자인 입력"),
        ("클라이언트 컨펌", "클라이언트 컨펌"),
    )
    search_text = models.CharField(max_length=1000, blank=True, null=True)
    design_title = models.CharField(max_length=200, blank=True, null=True)
    design_status = models.CharField(
        choices=DESIGN_STATUS, max_length=100, blank=True, null=True, default="디자인 처리중")
    admin_design_file = models.FileField(
        blank=True, null=True, upload_to="design_confirm")
    is_private = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    confirmed_at = models.DateTimeField(blank=True, null=True)

    def get_additional_product(self):
        text = []
        try:
            for item in self.product["additional_product"]:
                append_text = item["name"] + "*" + str(item["count"])
                text.append(append_text)
            if len(text) == 0:
                return "없음"
            else:
                return ",".join(text)
        except Exception as e:

            return "없음"

    def get_product_image(self):
        if self.product["original_product_id"]:
            product = ShopProduct.objects.get(
                id=self.product["original_product_id"])
            img = product.shopproductimage_set.all().first()
            return img.image.url
        else:
            return "/static/img/custom_purchase.jpg"


class DesignCustom(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    purchase_item = models.ForeignKey(
        PurchaseItem,  on_delete=models.CASCADE, blank=True, null=True)
    attached_file_0 = models.FileField(
        blank=True, null=True, upload_to="custom/")
    attached_file_1 = models.FileField(
        blank=True, null=True, upload_to="custom/")
    attached_file_2 = models.FileField(
        blank=True, null=True, upload_to="custom/")
    tel = models.CharField(max_length=100, blank=True, null=True,)
    name = models.CharField(max_length=100, blank=True, null=True,)
    content = models.TextField()
    created_at = models.DateField(auto_now_add=True, blank=True, null=True)
