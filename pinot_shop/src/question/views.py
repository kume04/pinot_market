from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import *


@csrf_exempt
def question(request, product_id, id):
    if request.method == "DELETE":
        Question.objects.get(product_id=product_id, id=id).delete()
        return JsonResponse({"result": "ok"})
