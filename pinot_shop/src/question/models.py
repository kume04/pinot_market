from django.db import models
from product.models import *
from shop_user.models import *
# Create your models here.


class Question(models.Model):
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=20, blank=True, null=True)
    product = models.ForeignKey(
        ShopProduct, on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=500, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_locked = models.BooleanField(default=False)
    password = models.CharField(max_length=500, blank=True, null=True)
    attached_file = models.FileField(
        upload_to="question", blank=True, null=True)

    def __str__(self):
        return self.title

    def previous_question(self):
        current_id = self.id
        prev = Question.objects.filter(
            id__lt=current_id).order_by("-id").first()
        if prev:
            return {"title": prev.title, "id": prev.id, "is_locked": prev.is_locked}
        else:
            return {"title": "이전글이 없습니다.", "id": "", "is_locked": False}

    def next_question(self):
        current_id = self.id
        next = Question.objects.filter(
            id__gt=current_id).order_by("id").first()
        if next:
            return {"title": next.title, "id": next.id, "is_locked": next.is_locked}
        else:
            return {"title": "다음글이 없습니다.", "id": "", "is_locked": False}


class QuestionReply(models.Model):
    origin_question = models.ForeignKey(
        Question, on_delete=models.CASCADE, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    view_count = models.IntegerField(default=0)
