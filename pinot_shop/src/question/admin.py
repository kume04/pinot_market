from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
# Register your models here.
from .models import *


@admin.register(Question)
class QuestionAdmin(SummernoteModelAdmin):
    pass
