"""pino_acryl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static
from django.http import HttpResponse

urlpatterns = [
    path('', include('menu.urls')),
    path('', include('shop_management.urls')),
    path('shop_admin/', include('shop_admin.urls')),
    path('sample_image/', include('sample_image.urls')),
    path('product/', include('product.urls')),
    path('account/', include('shop_user.urls')),
    path('question/', include('question.urls')),
    path('', include('custom.urls')),
    path('admin/', admin.site.urls),
    path('summernote/', include('django_summernote.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('robots.txt/', lambda x: HttpResponse("User-Agent: *\nDisallow:",
         content_type="text/plain")),
    # path('admin/log_viewer/', include('log_viewer.urls')),
    # path("__reload__/", include("django_browser_reload.urls")),
    # path('__debug__/', include('debug_toolbar.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
