from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('banner_click/<int:id>', views.banner_click),
    path('list/<int:id>', views.list),
    path('all/<int:id>', views.all),
    path('all', views.all),
    path('select', views.select),
    path('estimate/<str:id>', views.estimate),
    path('search', views.search),
    path('detail/<int:id>', views.detail),
    path("get_delievery_type", views.get_delievery_type),
]
