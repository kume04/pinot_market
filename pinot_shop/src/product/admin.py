from django.contrib import admin
from .models import *
from django_json_widget.widgets import JSONEditorWidget
from django.contrib.postgres import fields
from prettyjson import PrettyJSONWidget
from django import forms
from django_summernote.admin import SummernoteModelAdmin

# Register your models here.


class ShopProductImageInline(admin.TabularInline):
    model = ShopProductImage
    extra = 3


@admin.register(ShopProduct)
class ShopProductAdmin(SummernoteModelAdmin):
    summernote_fields = ('detail',)
    inlines = [ShopProductImageInline]


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    pass


@admin.register(DelieveryType)
class DelieveryTypeAdmin(admin.ModelAdmin):
    pass

@admin.register(ShopProductGroup)
class ShopProductGroupAdmin(admin.ModelAdmin):
    pass
