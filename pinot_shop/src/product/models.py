from django.db import models

# Create your models here.


class ShopProduct(models.Model):
    category = models.ForeignKey(
        "menu.Category", blank=True, null=True, on_delete=models.SET_NULL, verbose_name="상품 카테고리")
    name = models.CharField(max_length=500, verbose_name="상품 이름")
    description_summary = models.TextField(verbose_name="상품 간단 소개")
    showing_price = models.IntegerField(
        default=0, verbose_name="상품 원래 가격(리스트 노출)")
    detail = models.TextField(verbose_name="상품 상세 정보")
    created_at = models.DateTimeField(auto_now_add=True)
    is_hot = models.BooleanField(default=False)
    is_new = models.BooleanField(default=False)
    discount_price = models.IntegerField(
        default="0", verbose_name="상품 할인가격", blank=True, null=True, max_length=20)
    count_convention = models.CharField(
        default="", verbose_name="상품 판매단위", blank=True, null=True, max_length=20)
    selling_range = models.CharField(
        default="", verbose_name="상품 판매범위", blank=True, null=True, max_length=20)
    theme = models.ManyToManyField("Theme", blank=True,  verbose_name="상품 테마")
    option_json_data = models.TextField(
        verbose_name="상품 옵션 json 데이터 ", blank=True, null=True)
    additional_product_json_data = models.TextField(
        verbose_name="상품 옵션 json 데이터 ", blank=True, null=True)
    point = models.IntegerField(default=0, verbose_name="상품 포인트")
    delivery_duration = models.CharField(max_length=30, blank=True, null=True)
    need_to_file_upload = models.BooleanField(default=False)
    direct_receive_price = models.IntegerField(
        default=0, verbose_name="방문수령 가격")
    pre_parcel_price = models.IntegerField(
        default=0, verbose_name="선불택배 수령 가격")
    after_parcel_price = models.IntegerField(
        default=0, verbose_name="후불택배 수령 가격")
    pre_quick_price = models.IntegerField(default=0, verbose_name="선불퀵 수령 가격")
    after_quick_price = models.IntegerField(
        default=0, verbose_name="후불퀵 수령 가격")
    pre_carton_price = models.IntegerField(
        default=0, verbose_name="선불 화물 수령 가격")
    after_carton_price = models.IntegerField(
        default=0, verbose_name="후불 화물 수령 가격")
    local_delivery_price = models.IntegerField(
        default=0, verbose_name="지역배송 가격")
    self_delivery = models.IntegerField(default=0, verbose_name="방문수령 가격")
    is_show = models.BooleanField(default=True)
    is_main_display = models.BooleanField(default=False)
    sell_count = models.IntegerField(default=0)  # 판매량
    view_count = models.IntegerField(default=0)  # 판매량
    template_file = models.CharField(max_length=1000, blank=True, null=True)
    show_index = models.IntegerField(default=0)  # 보이는 순서

    group = models.ForeignKey("ShopProductGroup", blank=True, null=True, verbose_name="상품 그룹", on_delete=models.SET_NULL)
    group_specific_name = models.CharField(max_length=1000, blank=True, null=True)
    group_specific_index = models.IntegerField(default=0)
    is_group_main_product = models.BooleanField(default=False)
    def __str__(self):
        return self.name

    def image_url(self):
        img = ShopProductImage.objects.filter(product=self.id).order_by("index").first().image.url
        return img
    class Meta:
        ordering = ['-id']


class ShopProductGroup(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    def __str__(self):
        return self.name

    def group_product_set_id_list(self):
        return list(ShopProduct.objects.filter(group=self).order_by("group_specific_index").values("id","group_specific_name"))
    def product_set(self):
        return ShopProduct.objects.filter(group=self).order_by("-show_index")
        

class ShopProductImage(models.Model):
    product = models.ForeignKey(
        ShopProduct, on_delete=models.CASCADE, verbose_name="상품", blank=True, null=True)
    image = models.ImageField(
        upload_to="product/%Y/%m/%d/", verbose_name="상품이미지")
    index = models.IntegerField(default=0)

    class Meta:
        ordering = ['index']


class Theme(models.Model):
    name = models.CharField(max_length=100, verbose_name="상품 테마")

    def __str__(self):
        return self.name


class DelieveryType(models.Model):
    DELIVER_CHOICES = (
        ("선불", "선불"),
        ("후불", "후불"),
    )
    delievery_type = models.CharField(
        max_length=10, choices=DELIVER_CHOICES, blank=True, null=True)
    text = models.CharField(max_length=100, blank=True, null=True)
    price = models.IntegerField(default=0)

    def get_name(self):
        return self.name+":"+str(self.price)


class TempFile(models.Model):
    temp_file = models.FileField(blank=True, null=True, verbose_name="임시 파일")
