from datetime import datetime
from django.shortcuts import render
from .models import *
from menu.models import *
from review.models import *
from question.models import *
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q, F
import json
from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from sample_image.models import *
# Create your views here.

def all(request, id=0):
    parent_id = ""
    current_main_category = ""
    if(id==0):
        category= Category.objects.filter( parent_category__isnull=False).order_by("index")
        total_category_id_list = Category.objects.all().values_list("id")

    else:
        category = Category.objects.get(id=id)
        if category.parent_category:
            parent_id = category.parent_category.id
        total_category_id_list = []
        total_category_id_list.append(id)
        for item in category.category_set.all():
            total_category_id_list.append(item.id)
    main_category = Category.objects.filter(
        parent_category=None).order_by("index")
    if parent_id:
        current_main_category = Category.objects.get(id=parent_id)
    elif id != 0:
        current_main_category = Category.objects.get(id=id)

    page = int(request.GET.get("page")) if request.GET.get("page") else 1
    per_count = int(request.GET.get("count")
                    ) if request.GET.get("count") else 20
    order_by = request.GET.get("order_by") if request.GET.get("order_by") else "-show_index"
    print(order_by)


    included_product = ShopProduct.objects.filter(category__in=total_category_id_list).filter(is_show=True).order_by(
        order_by)[(page - 1) * per_count: page * per_count]
    all_product_length = ShopProduct.objects.filter(category__in=total_category_id_list).filter(is_show=True).count()
    image_id = request.GET.get("image_id")
    product_id = request.GET.get("product_id")
    image = ""
    if image_id :
        image = SampleImage.objects.get(id=image_id)
    product=""
    if product_id :
        product = ShopProduct.objects.get(id=product_id)

    return render(request, "product/all.html", {
        "current_main_category":current_main_category,
        "parent_id":parent_id,
        "id":id,
        "category": category,
        "included_product": included_product,
        "main_category": main_category,
        "id": id,
        "order_by": order_by,
        "page": page,
        "per_count": per_count,
        "product_id":product_id,
        "product":product,
        "image_id": image_id, "image": image,
        "all_product_length": all_product_length
    })


def list(request, id=1):
    category = Category.objects.get(id=id)
    total_category_id_list = []
    total_category_id_list.append(id)
    main_category = Category.objects.filter(
        parent_category=None).order_by("index")

    page = int(request.GET.get("page")) if request.GET.get("page") else 1
    per_count = int(request.GET.get("count")
                    ) if request.GET.get("count") else 20
    order_by = request.GET.get("order_by") if request.GET.get("order_by") else "-show_index"
    print(order_by)
    for item in category.category_set.all():
        total_category_id_list.append(item.id)

    included_product = ShopProduct.objects.filter(category__in=total_category_id_list).filter(is_show=True).order_by(
        order_by)[(page-1)*per_count: page*per_count]
    image_id = request.GET.get("image_id")
    image = ""
    if image_id:
        image = SampleImage.objects.get(id=image_id)
    return render(request, "product/list.html", {
        "category": category,
        "included_product": included_product,
        "main_category": main_category,
        "id": id,
        "order_by": order_by,
        "page": page,
        "per_count": per_count,
        "image_id":image_id,"image":image
    })


def search(request, id=1):
    keyword = request.GET.get("q")
    included_product = ShopProduct.objects.filter(
        Q(name__icontains=keyword) | Q(description_summary__icontains=keyword))
    search_banner = Banner.objects.filter(type="search").first()
    return render(request, "product/search.html", {"included_product": included_product, "id": id, "search_banner": search_banner})


@csrf_exempt
def detail(request, id=1):

    if request.method == "POST":
        # 문의 내용 남기기
        Question(
            author=request.user,
            name=request.user.nickname,
            product_id=request.POST.get("id"),
            content=request.POST.get("content"),
            title=request.POST.get("title"),
        ).save()
        return JsonResponse({"result": "ok"})
    else:
        image_id = request.GET.get("image_id")
        product = ShopProduct.objects.get(id=id)
        ShopProduct.objects.filter(id=id).update(view_count=F('view_count')+1)
        qna = Question.objects.filter(product_id=id).order_by("-created_at")
        if product.additional_product_json_data != None:
            additional_product = json.loads(product.additional_product_json_data)[
                "additional_product"]
        else:
            additional_product = []
        return render(request, "product/detail.html", {"image_id":image_id,"product": product, "qna": qna, "additional_product": additional_product})


def get_delievery_type(request):
    delievery_type = DelieveryType.objects.all().values()
    return JsonResponse({"result": list(delievery_type)})


@csrf_exempt
def estimate(request, id="1"):
    if request.method == "GET":
        id = id.split(",")
        basket_product = Basket.objects.filter(id__in=id)
        return render(request, "product/estimate.html", {"basket_product": basket_product})
    else:
        htmly = request.POST.get("html")
        str_format = str(datetime.now().year)+"년 " + \
            str(datetime.now().month)+"월 " + str(datetime.now().day)+"일 "
        print(str_format)
        mail.send_mail("[피노마켓] " + str_format + " 견적서를 전달드립니다.", strip_tags(htmly),
                       ' pinot <market@pinotmarket.co.kr>', [request.POST.get("email")], html_message=htmly)
        return JsonResponse({"RESULT": "OK"})


@csrf_exempt
def banner_click(request, id=1):
    Banner.objects.filter(id=id).update(click_count=F('click_count')+1)
    return JsonResponse({"result": "OK"})



def select(request, id=0):

  
    image_id = request.GET.get("image_id")
    product_id = request.GET.get("product_id")
    image = ""
    if image_id :
        image = SampleImage.objects.get(id=image_id)
    product=""
    if product_id :
        product = ShopProduct.objects.get(id=product_id)

    return render(request, "product/select.html", {
        "id":id,    
        "id": id,      
        "product_id":product_id,
        "product":product,
        "image_id": image_id, "image": image,     
    })