from django import template
register = template.Library()
from product.models import *
from menu.models import *

@register.filter(name="has_option")
def has_option(product, name ):
    has_print_option  = product.option.filter(name=name) 
    if has_print_option.count() > 0 :
        return True
    else: 
        return False

@register.filter(name="get_print_option_list")
def get_print_option_list(product, name ):
    
    return list(product.option.get(name=name).option["select"])
    
@register.simple_tag(name="get_option")
def get_option(product, name ):
    
    return (product.option.get(name=name))

@register.simple_tag(name="get_name_by_id")
def get_name_by_id(basket):    
    if basket.product["original_product_id"]:
        if ShopProduct.objects.filter(id=basket.product["original_product_id"]).count() > 0 :
            return ShopProduct.objects.get(id=basket.product["original_product_id"]).name
        else:
            print(basket)
            print(basket.product)
            if "private_product_name" in basket.product:
                return basket.product["private_product_name"]
            else:
                return "삭제된 제품입니다."
    else: 
        return basket.product["private_product_name"]
@register.simple_tag(name="get_product_by_id")
def get_product_by_id(basket, name):    
    value = getattr( ShopProduct.objects.get(id=basket.product["original_product_id"]) , name)
    return value

import math
@register.simple_tag(name="get_product_discount_sale")
def get_product_discount_sale(product):    
    before_value = int( product.showing_price )
    after_value = int( product.discount_price )
    percent = math.ceil( ( before_value - after_value )*100 / before_value ) 
    return percent

@register.simple_tag(name="is_same_price")
def is_same_price(product):    
    before_value = int( product.showing_price )
    after_value = int( product.discount_price )
    return before_value == after_value


@register.simple_tag(name="split_url")
def split_url(url):        
    return url.split("?")[0]

@register.simple_tag(name="space_escape")
def space_escape(val):
    return val.replace("!!"," ")

@register.simple_tag(name="get_additonal_item_url")
def get_additonal_item_url(val):
    return TempFile.objects.get(id=val).temp_file.url

@register.filter
def get_vat( value ):
    '''
    Divides the value; argument is the divisor.
    Returns empty string on any error.
    '''
    try:
        value = int( value )
        return value *0.1
    except: pass
    return ''

@register.filter
def get_image( value ):  
    try:
        value = int( value )
        print(value)
        return ShopProductImage.objects.get(id=value).image.url
    except: pass
    return ''

    