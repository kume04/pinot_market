import requests,json
import datetime

zone_endpoint = "https://oapi.ecounterp.com/OAPI/V2/Zone"
login_endpoint = "https://oapi{}.ecounterp.com/OAPI/V2/OAPILogin"
order_endpoint = "https://oapi{}.ecounterp.com/OAPI/V2/SaleOrder/SaveSaleOrder?SESSION_ID={}"

headers = {'Content-Type': 'application/json; charset=utf-8'}
zone = ""

def get_zone():
    response = requests.post(zone_endpoint,headers=headers,data=json.dumps({
        "COM_CODE":"189935"
    }))    
    res = response.json()
    return res["Data"]["ZONE"]

def login():
    data = {
        "COM_CODE":"189935",
        "USER_ID" : "pinot",
        "API_CERT_KEY": "2bd53dcf4762a4bce951d6a27957618429",
        "LAN_TYPE":"ko-KR",
        "ZONE": get_zone()
    }
    response = requests.post(login_endpoint.format(get_zone()),headers=headers,data=json.dumps(data))
    res = (response.json())
    print(res["Data"]["Datas"]["SESSION_ID"])
    return res["Data"]["Datas"]["SESSION_ID"]

def write_order():
    data = {
        "SaleOrderList":[
            {
                "Line": "0",
                "BulkDatas": {           
                    "PROD_CD":"S_BOX_A_150_3T_BLACK",
                    "PROD_DES":"더미데이터",
                    # "SIZE_DES": "1",
                    "QTY":"1",
                    "PRICE": "11000",
                    "SUPPLY_AMT":"11000",
                    "USER_PRICE_VAT":"12100",
                    "VAT_AMT" : "1100"
                }
            }
        ]
    }
    response = requests.post(order_endpoint.format(get_zone(),login()),headers=headers,data=json.dumps(data))
    res = (response.json())
    print(res)