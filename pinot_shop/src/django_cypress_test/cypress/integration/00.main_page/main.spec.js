/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe("메인페이지 접속", () => {
  beforeEach(() => {
    cy.visit("http://127.0.0.1:8000/");
  });

  it("메인페이지 접속시 BODY 가 나타나야 함", () => {
    cy.get("body").should("have.length", 1);
  });
});
