describe("로그인 기능 확인", () => {
  beforeEach(() => {
    cy.visit("http://127.0.0.1:8000/account/login");
  });

  it("로그인 화면 표시", () => {
    cy.get("body").should("have.length", 1);
  });
  it("로그인 유효성 검사 - 아이디 이메일 형식", () => {
    cy.get("#username").type("dltpgus86");
    cy.get("#password").type("dltpgus86");
    cy.get("#user_login").click();
    cy.on("window:alert", (text) => {
      expect(text).to.contains("아이디(이메일) 형식을 확인해주세요.");
    });
  });
  it("로그인 유효성 검사 - 아이디 미입력", () => {
    cy.get("#password").type("dltpgus86");
    cy.get("#user_login").click();
    cy.on("window:alert", (text) => {
      expect(text).to.contains("아이디(이메일) 형식을 확인해주세요.");
    });
  });
  it("로그인 유효성 검사 - 로그인 성공시 환영 모달 노출후 메인페이지 이동", () => {
    cy.get("#username").type("sehn_86@naver.com");
    cy.get("#password").type("dltpgus86");
    cy.get("#user_login").click();
    cy.wait(3000); // 3초 웨이팅 시간
    cy.get(".modal.show").should("have.length", 1); // 로그인 성공, 환영 모달 디스플레이
    cy.get(".modal.show #welcome_message").contains("환영합니다.");
    cy.get("#move_to_main").click();
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq("/"); // 로그인 성공 - > 메인페이지로 이동
    });
  });
});
