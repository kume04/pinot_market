/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

function reverse(s) {
  return s.split("").reverse().join("");
}

describe("메인페이지 접속", () => {
  beforeEach(() => {
    cy.visit("http://localhost:8000/account/login");
  });

  it("아이디/비밀번호 재발급 클릭시 해당 페이지로 이동 후 아이디 찾기 성공", () => {
    cy.get("#find_account").click();
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq("/account/find"); // 아이디 찾기/비밀번호 변경 클릭 - > 페이지로 이동
    });
    cy.get("#name").type("이세현");
    cy.get("#phone").type("01032535365");
    cy.get("#user_find").click();
    cy.get("#find_id_area b").contains("se*****@naver.com");
  });

  it("해당 계정정보가 없는 경우 일치하는 계정 없음 안내", () => {
    cy.get("#find_account").click();
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq("/account/find"); // 아이디 찾기/비밀번호 변경 클릭 - > 페이지로 이동
    });
    cy.get("#name").type("이세");
    cy.get("#phone").type("01032535365");
    cy.get("#user_find").click();
    cy.get("#find_id_area").contains("해당 정보와 일치하는 계정이 없습니다.");
  });

  it("비밀번호 재발급 시 이메일로 변경 링크 전송", () => {
    cy.get("#find_account").click();
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq("/account/find"); // 아이디 찾기/비밀번호 변경 클릭 - > 페이지로 이동
    });
    cy.get("#nonuser-tab").click();
    cy.get("#name2").type("이세현");
    cy.get("#email2").type("sehn_86@naver.com");
    cy.get("#password_change").click();
    cy.wait(3000);
    cy.get("#find_pw_area").contains("비밀번호 변경에 대한 안내를");
  });

  it("비밀번호 변경 확인", () => {
    cy.get("#find_account").click();
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq("/account/find"); // 아이디 찾기/비밀번호 변경 클릭 - > 페이지로 이동
    });
    cy.get("#nonuser-tab").click();
    cy.get("#name2").type("이세현");
    cy.get("#email2").type("sehn_86@naver.com");
    cy.intercept("POST", "/account/find").as("ajax_password_change");
    cy.get("#password_change").click();
    cy.wait(3000);
    cy.get("#password_change").click();
    cy.wait(3000);
    let data_id;
    // cy.get("#find_pw_area").then(($elem) => {
    //   data_id = reverse($elem.attr("data-id"));
    //   cy.visit(`http://localhost:8000/account/password_change/${data_id}`); // 비밀번호 변경 링크 이동
    //   cy.get("#password1").type("dltpgus86!@"); // 비밀번호 입력
    //   cy.get("#password2").type("dltpgus86!@"); // 비밀번호 확인
    //   cy.get("#user_login").click(); // 비밀번호 변경 버튼 클릭
    //   cy.location().should((loc) => {
    //     expect(loc.pathname).to.eq("/"); // 메인 페이지 이동
    //   });
    // });
    let data;

    cy.wait("@ajax_password_change").then((res) => {
      cy.log(res.response.body["data-id"]);
      data = reverse(res.response.body["data-id"]);
      cy.log(data);
      cy.visit(`http://localhost:8000/account/password_change/${data}`); // 비밀번호 변경 링크 이동
      cy.get("#password1").type("dltpgus86"); // 비밀번호 입력
      cy.get("#password2").type("dltpgus86"); // 비밀번호 확인
      cy.get("#user_login").click(); // 비밀번호 변경 버튼 클릭
      cy.on("window:alert", (text) => {
        expect(text).to.contains(
          "비밀번호가 성공적으로 변경되었습니다. 로그인을 진행해주세요"
        );
      });
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq("/"); // 메인 페이지 이동
      });
    });
  });
  it("비밀번호 번호 복구", () => {
    cy.get("#find_account").click();
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq("/account/find"); // 아이디 찾기/비밀번호 변경 클릭 - > 페이지로 이동
    });
    cy.get("#nonuser-tab").click();
    cy.get("#name2").type("이세현");
    cy.get("#email2").type("sehn_86@naver.com");
    cy.get("#password_change").click();
    cy.wait(3000);
    let data_id = cy.get("#find_pw_area").invoke("attr", "data-id");
    cy.log(data_id);
    cy.log(data_id);
    cy.get("#find_pw_area").then(($elem) => {
      data_id = reverse($elem.attr("data-id"));
      cy.visit(`http://localhost:8000/account/password_change/${data_id}`); // 비밀번호 변경 링크 이동
      cy.get("#password1").type("dltpgus86"); // 비밀번호 입력
      cy.get("#password2").type("dltpgus86"); // 비밀번호 확인
      cy.get("#user_login").click(); // 비밀번호 변경 버튼 클릭
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq("/"); // 메인 페이지 이동
      });
    });
  });
});
