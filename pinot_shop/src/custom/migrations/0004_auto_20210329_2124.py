# Generated by Django 3.1 on 2021-03-29 12:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom', '0003_auto_20201228_1843'),
    ]

    operations = [
        migrations.AlterField(
            model_name='custominquiry',
            name='attached_file',
            field=models.FileField(blank=True, null=True, upload_to='custom/'),
        ),
    ]
