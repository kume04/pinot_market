from django.shortcuts import render
from review.models import *
from .models import *
from django.http import JsonResponse
# Create your views here.
import json
from django.views.decorators.csrf import csrf_exempt
import math
from django.db.models import Q
import requests
import json

@csrf_exempt
def all(request):
    per = request.GET.get("per")
    if per == "" or per == None:
        per = 20
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    per = int(per)
    page = int(page)
    keyword = request.GET.get("q")
    q = Q()
    type = request.GET.get("type")
    if type == "all":
        q |= Q(title__icontains=keyword)
        q |= Q(name__icontains=keyword)
    elif type == "title":
        q |= Q(title__icontains=keyword)
    elif type == "content":
        q |= Q(content__icontains=keyword)
    elif type == "writer":
        q |= Q(writer__nickname__icontains=keyword)

    total_page = math.ceil(CustomInquiry.objects.filter(
        q).filter(is_deleted=False).count() / per)
    question_list = CustomInquiry.objects.filter(
        q).filter(is_deleted=False).order_by("-id")[(page-1)*per:page*per]
    return render(request, "custom/all.html", {"question_list": question_list, "total_page": range(1, total_page+1), "page": page, "keyword": keyword, "type": type})


@csrf_exempt
def detail(request, id):
    if request.method == "POST":
        question = CustomInquiry.objects.get(id=id)
        question.view_count = question.view_count+1
        question.save()
        question = CustomInquiry.objects.filter(id=id).filter(
            password=request.POST.get("password")).values()
        return JsonResponse({"result": list(question),
                             "file_0": CustomInquiry.objects.filter(id=id).filter(password=request.POST.get("password")).first().attached_file_0_url(),
                             "file_1": CustomInquiry.objects.filter(id=id).filter(password=request.POST.get("password")).first().attached_file_1_url(),
                             "file_2": CustomInquiry.objects.filter(id=id).filter(password=request.POST.get("password")).first().attached_file_2_url(),


                             })
    question = CustomInquiry.objects.get(id=id)
    writer = CustomInquiry.objects.get(id=id).writer
    if request.user == writer:
        question = CustomInquiry.objects.get(id=id)
        question.view_count = question.view_count+1
        question.save()
    return render(request, "custom/detail.html", {"question": question, "writer": writer})


@csrf_exempt
def write(request):
    if request.method == "POST":
        print(request.POST)
        ci = CustomInquiry()
        if request.POST.get("writer"):
            ci.writer = User.objects.get(id=request.POST.get("writer"))
        ci.title = request.POST.get("title")
        ci.attached_file_0 = request.FILES.get("attached_file_0")
        ci.attached_file_1 = request.FILES.get("attached_file_1")
        ci.attached_file_2 = request.FILES.get("attached_file_2")
        ci.email = request.POST.get("email")
        ci.name = request.POST.get("name")
        ci.inquiry_type = request.POST.get("inquiry_type")
        ci.password = request.POST.get("password")
        ci.is_private = json.loads(request.POST.get("is_private", 'false'))
        ci.content = request.POST.get("content")
        ci.save()
        #  pinot smart factory module 과 연계
        target_url = "http://13.124.52.145:5004/api/questions"
        headers = {'Content-Type': 'application/json; charset=utf-8'}

        data = {
            'title': "[피노마켓|"+request.POST.get("inquiry_type")+"] "+request.POST.get("title"), 
            'co_name': User.objects.get(id=request.POST.get("writer")).nickname ,
            'person': User.objects.get(id=request.POST.get("writer")).nickname ,
            "content": request.POST.get("content"),
            "status":"응답 대기",
            "contact": User.objects.get(id=request.POST.get("writer")).phone,
            "email": User.objects.get(id=request.POST.get("writer")).username
            }
        res = requests.post(target_url, data=json.dumps(data),headers=headers)
      
        return JsonResponse({"result": "ok"})
    return render(request, "custom/write.html")


@csrf_exempt
def delete_custom_article(request, id):
    if request.method == "DELETE" and CustomInquiry.objects.get(id=id).writer == request.user:
        cu = CustomInquiry.objects.get(id=id)
        cu.is_deleted = True
        cu.save()
        return JsonResponse({"result": "ok"})


@csrf_exempt
def blind_custom_article(request, id):
    if request.method == "POST":
        cu = CustomInquiry.objects.get(id=id)
        cu.is_blind = json.loads(request.POST.get("status"))
        cu.save()
        return JsonResponse({"result": "ok"})


@csrf_exempt
def modify(request, id):
    if request.method == "POST":
        ci = CustomInquiry.objects.get(id=id)
        ci.title = request.POST.get("title")
        # ci.attached_file_0 = request.FILES.get("attached_file_0")
        # ci.attached_file_1 = request.FILES.get("attached_file_1")
        # ci.attached_file_2 = request.FILES.get("attached_file_2")
        # ci.email = request.POST.get("email")
        # ci.name = request.POST.get("name")
        # ci.password = request.POST.get("password")
        # ci.is_private = json.loads(request.POST.get("is_private", 'false'))
        ci.content = request.POST.get("content")
        ci.save()
        return JsonResponse({"result": "ok"})
    # return render(request, "custom/write.html")
