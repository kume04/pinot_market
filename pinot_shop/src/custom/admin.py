from django.contrib import admin
from .models import *

# Register your models here.
from django_summernote.admin import SummernoteModelAdmin


@admin.register(CustomInquiry)
class CustomInquiryAdmin(SummernoteModelAdmin):
    list_display = ("title", "writer", "status")
    pass
