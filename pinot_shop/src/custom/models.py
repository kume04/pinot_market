from shop_user.models import User
from django.db import models
from datetime import datetime
from django.utils.dateformat import DateFormat
# Create your models here.


class CustomInquiry(models.Model):
    writer = models.ForeignKey(
        User, on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=100)
    attached_file_0 = models.FileField(
        blank=True, null=True, upload_to="custom/")
    attached_file_1 = models.FileField(
        blank=True, null=True, upload_to="custom/")
    attached_file_2 = models.FileField(
        blank=True, null=True, upload_to="custom/")
    email = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    password = models.CharField(max_length=100, blank=True, null=True)
    is_private = models.BooleanField(default=False)
    content = models.TextField()
    STATUS = (("pending", "처리중"), ("complete", "처리완료"))
    created_at = models.DateField(auto_now_add=True, blank=True, null=True)
    view_count = models.IntegerField(default=0)
    status = models.CharField(
        choices=STATUS, max_length=100, default="pending", blank=True, null=True)
    answered_at = models.DateField(blank=True, null=True)
    is_deleted = models.BooleanField(default=False)
    is_blind = models.BooleanField(default=False)
    inquiry_type = models.CharField(default="제작문의",max_length=100, blank=True, null=True)

    def __str__(self):
        return self.title

    def previous_question(self):
        current_id = self.id
        prev = CustomInquiry.objects.filter(
            id__lt=current_id).order_by("-id").first()
        if prev:
            return {"title": prev.title, "id": prev.id, "is_locked": prev.is_private}
        else:
            return {"title": "이전글이 없습니다.", "id": "",  "is_locked": False}

    def next_question(self):
        current_id = self.id
        next = CustomInquiry.objects.filter(
            id__gt=current_id).order_by("id").first()
        if next:
            return {"title": next.title, "id": next.id, "is_locked": next.is_private}
        else:
            return {"title": "다음글이 없습니다.", "id": "",  "is_locked": False}

    def attached_file_0_url(self):
        if self.attached_file_0:
            return self.attached_file_0.url
        else:
            return ""

    def attached_file_1_url(self):
        if self.attached_file_1:
            return self.attached_file_1.url
        else:
            return ""

    def attached_file_2_url(self):
        if self.attached_file_2:
            return self.attached_file_2.url
        else:
            return ""

    def is_new(self):
        today = DateFormat(datetime.now()).format('Ymd')
        created_at = DateFormat(self.created_at).format('Ymd')
        if (today == created_at):
            return True
        else:
            return False


class CustomInquiryAnswer(models.Model):
    inquiry = models.ForeignKey(CustomInquiry, on_delete=models.CASCADE)
    writer = models.ForeignKey(
        User, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    content = models.TextField()
    attached_file_0 = models.FileField(
        blank=True, null=True, upload_to="custom/")
    attached_file_1 = models.FileField(
        blank=True, null=True, upload_to="custom/")
    attached_file_2 = models.FileField(
        blank=True, null=True, upload_to="custom/")
    created_at = models.DateField(auto_now_add=True, blank=True, null=True)
