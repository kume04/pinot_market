from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
# Register your models here.
from .models import *
# Register your models here.
admin.site.register(Review)


@admin.register(Inquiry)
class InquiryAdmin(admin.ModelAdmin):
    pass
