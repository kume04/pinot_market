# Generated by Django 3.2.13 on 2022-06-15 05:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('review', '0017_auto_20210315_1922'),
    ]

    operations = [
        migrations.RenameField(
            model_name='inquiry',
            old_name='autor',
            new_name='author',
        ),
        migrations.RenameField(
            model_name='questionreply',
            old_name='autor',
            new_name='author',
        ),
    ]
