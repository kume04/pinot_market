# Generated by Django 3.0.6 on 2020-05-21 19:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('product', '0003_auto_20200521_1521'),
        ('review', '0004_auto_20200521_1714'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewreply',
            name='review',
            field=models.ForeignKey(
                default='', on_delete=django.db.models.deletion.CASCADE, to='review.Review'),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='ReviewImage',
            fields=[
                ('id', models.AutoField(auto_created=True,
                 primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='')),
                ('review', models.ForeignKey(
                    on_delete=django.db.models.deletion.CASCADE, to='review.Review')),
            ],
        ),
        migrations.CreateModel(
            name='QuestionReply',
            fields=[
                ('id', models.AutoField(auto_created=True,
                 primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=1000)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('content', models.TextField()),
                ('autor', models.ForeignKey(
                    on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True,
                 primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=500)),
                ('content', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('autor', models.ForeignKey(
                    on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('product', models.ForeignKey(
                    on_delete=django.db.models.deletion.CASCADE, to='product.ShopProduct')),
            ],
        ),
    ]
