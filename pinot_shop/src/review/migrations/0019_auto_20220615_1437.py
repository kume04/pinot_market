# Generated by Django 3.2.13 on 2022-06-15 05:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('review', '0018_auto_20220615_1436'),
    ]

    operations = [
        migrations.RenameField(
            model_name='review',
            old_name='autor',
            new_name='author',
        ),
        migrations.RenameField(
            model_name='reviewreply',
            old_name='autor',
            new_name='author',
        ),
    ]
