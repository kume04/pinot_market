from django.db import models
from product.models import *
from shop_user.models import *
# Create your models here.


class Review(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(ShopProduct, on_delete=models.CASCADE)
    starpoint = models.IntegerField(default=0)
    title = models.CharField(max_length=1000)
    view_count = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField()


class ReviewImage(models.Model):
    review = models.ForeignKey(Review, on_delete=models.CASCADE)
    image = models.ImageField()


class ReviewReply(models.Model):
    review = models.ForeignKey(Review, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField()


class Inquiry(models.Model):
    STATUS_CHOICES = (
        ("0", "처리중"),
        ("1", "답변완료")
    )
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    status = models.CharField(choices=STATUS_CHOICES, max_length=100)
    inquiry_attached_file = models.FileField(blank=True, null=True)
    reply = models.TextField(blank=True, null=True, default="")
    reply_attached_file = models.FileField(blank=True, null=True)
    replyed_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.title
