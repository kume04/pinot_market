from django.shortcuts import render

# Create your views here.


def admin_menu(request):
    return render(request, "adminlte/admin_menu.html")
