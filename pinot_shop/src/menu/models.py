from django.db import models
from product.models import *
import datetime
from pytz import timezone as pytzone


class Category(models.Model):
    parent_category = models.ForeignKey(
        "Category", blank=True, null=True, on_delete=models.CASCADE, verbose_name="부모 카테고리")
    name = models.CharField(max_length=100, blank=True,
                            null=True, verbose_name="카테고리 이름")
    icon = models.CharField(max_length=100, blank=True,
                            null=True, verbose_name="카테고리 아이콘")
    thumbnail = models.ImageField(
        blank=True, null=True, verbose_name="카테고리 이미지")
    is_public = models.BooleanField(default=True)
    index = models.IntegerField(default=0)
    list_thumbnail = models.ImageField(
        blank=True, null=True, verbose_name="카테고리 리스트 페이지 이미지")

    class Meta:
        ordering = ['parent_category_id', "index"]

    def __str__(self):
        if self.parent_category:
            return self.parent_category.name + ">" + self.name
        else:
            return self.name

    def html_name(self):
        if self.parent_category:
            return self.parent_category.name + "  >  " + self.name
        else:
            return self.name

    @property
    def image_url(self):
        if self.thumbnail and hasattr(self.thumbnail, 'url'):
            return self.thumbnail.url
        else:
            return ""

    def list_thumbnail_url(self):
        if self.list_thumbnail and hasattr(self.list_thumbnail, 'url'):
            return self.list_thumbnail.url
        else:
            return ""

    @property
    def child_product(self):
        id_list = Category.objects.filter(
            parent_category=self).values_list("id")
        product_list = ShopProduct.objects.filter(category_id__in=id_list)
        return product_list[0:5]

    @property
    def main_display_product(self):
        product_list = ShopProduct.objects.filter(
            category__parent_category_id=self.id).filter(is_main_display=True).filter(is_show=True)
        return product_list[0:8]

    @property
    def product_count(self):
        if self.category_set.all().count() > 0:
            id_list = []
            for item in self.category_set.all():
                for product in item.shopproduct_set.all().filter(is_show=True):
                    id_list.append(product.id)
            return len(set(id_list))
        else:
            return len(self.shopproduct_set.all().filter(is_show=True))


class Banner(models.Model):
    BANNER_TYPE = (
        ("common", "전체 페이지(상단)"),
        ("category", "각 카테고리페이지"),
    )
    type = models.CharField(choices=BANNER_TYPE,
                            max_length=20, blank=True, null=True)
    category = models.ForeignKey(
        "Category", blank=True, null=True, on_delete=models.CASCADE)
    pc_image = models.FileField(upload_to="banner", blank=True, null=True)
    mobile_image = models.FileField(upload_to="banner", blank=True, null=True)
    name = models.CharField(max_length=30, blank=True, null=True)
    link = models.CharField(max_length=100, blank=True, null=True)
    background_color = models.CharField(max_length=30, blank=True, null=True)
    start_dt = models.DateField(blank=True, null=True)
    end_dt = models.DateField(blank=True, null=True)
    click_count = models.IntegerField(default=0)
    view_count = models.IntegerField(default=0)

    def pc_image_url(self):
        if self.pc_image:
            if self.start_dt is not None and self.end_dt is not None:
                if datetime.date.today() >= self.start_dt and datetime.date.today() <= self.end_dt:
                    return self.pc_image.url
                else:
                    return ""
            else:
                return ""
        else:
            return ""

    def mobile_image_url(self):
        if self.mobile_image:
            return self.mobile_image.url
        else:
            return ""


class MainShortcut(models.Model):
    pc_image = models.FileField(
        upload_to="main_shortcut", blank=True, null=True)
    mobile_image = models.FileField(
        upload_to="main_shortcut", blank=True, null=True)
    name = models.CharField(max_length=30, blank=True, null=True)
    link = models.CharField(max_length=100, blank=True, null=True)
    index = models.IntegerField(default=0)
    is_margin_top_exist = models.BooleanField()

    def pc_image_url(self):
        if self.pc_image:
            return self.pc_image.url
        else:
            return ""

    def mobile_image_url(self):
        if self.mobile_image:
            return self.mobile_image.url
        else:
            return ""

    def recent_click_count(self):
        target_date = datetime.datetime.now(
            pytzone('Asia/Seoul')) - datetime.timedelta(30)

        return self.sc.filter(created_at__gt=target_date).count()


class MainShortcutClickCount(models.Model):
    shortcut = models.ForeignKey(
        MainShortcut, on_delete=models.CASCADE, verbose_name="메인 숏컷", blank=True, null=True, related_name="sc")
    ua = models.CharField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
