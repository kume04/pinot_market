import time
import json
import datetime
from django import template
from django.core.serializers import serialize
from menu.models import Banner
from django.db.models import F
register = template.Library()
# @register.filter
# def filtering_delayed_project(value):
#     return value.filter(project_due__lt=str(datetime.datetime.now().strftime("%Y-%m-%d"))).exclude(is_complete=True).count()


@register.filter
def split_time(value):
    try:
        return value.split("T")[1]
    except:
        return ""


@register.filter
def split_day(value):
    try:
        return value.split("T")[0]
    except:
        return ""


@register.filter
def to_ko_day(value):
    try:
        value = value.split("T")[0]
        day = ["월", "화", "수", "목", "금", "토", "일"]
        date = datetime.datetime.strptime(value, "%Y-%m-%d").weekday()

        return day[date]
    except Exception as e:

        return ""


@register.filter
def loadjson(data):
    return json.loads(data)


@register.filter
def json(queryset):
    return serialize('json', queryset)

@register.filter
def count_up_banner(id):
    Banner.objects.filter(id = id).update(view_count = F("view_count") + 1)
    return ""

