# Generated by Django 3.1 on 2021-01-18 12:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop_admin', '0006_auto_20210117_2320'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='design',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='directory',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='memo',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='operator',
            field=models.ManyToManyField(
                blank=True, null=True, to='shop_admin.Employee'),
        ),
        migrations.AlterField(
            model_name='project',
            name='packaging_status',
            field=models.CharField(
                blank=True, default='waiting', max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='post_processing_status',
            field=models.CharField(
                blank=True, default='waiting', max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='process_type',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='processing_status',
            field=models.CharField(
                blank=True, default='waiting', max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='project_due',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='project_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='project_type',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
