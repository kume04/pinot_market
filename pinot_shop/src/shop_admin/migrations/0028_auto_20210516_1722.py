# Generated by Django 3.1 on 2021-05-16 08:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop_admin', '0027_accesslog_target_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='operator',
            field=models.ManyToManyField(blank=True, to='shop_admin.Employee'),
        ),
    ]
