# Generated by Django 3.1 on 2021-01-17 14:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop_admin', '0005_auto_20210117_2235'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='packaging_text',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='post_processing_text',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='processing_machine',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
