from django.db import models

# Create your models here.


class AccessLog(models.Model):
    ip = models.CharField(max_length=30, null=True, blank=True)
    user_agent = models.CharField(max_length=300, null=True, blank=True)
    target_url = models.CharField(max_length=600, null=True, blank=True)
    accessed_at = models.DateTimeField(auto_now_add=True)


class Employee(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    scope = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    tel = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    extra_address = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Receipt(models.Model):
    receipt_number = models.CharField(max_length=100, blank=True, null=True)
    receipt_employee = models.ManyToManyField(Employee)
    receipt_type = models.CharField(max_length=100)
    receipt_date_time = models.CharField(max_length=100, blank=True, null=True)
    client_company_name = models.CharField(
        max_length=100, blank=True, null=True)
    client_person_name = models.CharField(
        max_length=100, blank=True, null=True)
    client_person_phone = models.CharField(
        max_length=100, blank=True, null=True)
    delivery_charge_type = models.CharField(max_length=100)
    is_urgent = models.BooleanField(default=False)
    delivery_type = models.CharField(max_length=100)
    local = models.CharField(max_length=50, blank=True, null=True)
    sender_name = models.CharField(max_length=50, blank=True, null=True)
    receipter_name = models.CharField(max_length=50, blank=True, null=True)
    receipt_location = models.CharField(max_length=500, blank=True, null=True)
    receipt_comment = models.TextField(blank=True, null=True)

    def receipt_employee_text(self):
        list = self.receipt_employee.all().values_list("name", flat=True)

        return ",".join(list)


class Project(models.Model):
    project_number = models.CharField(max_length=100, blank=True, null=True)
    receipt = models.ForeignKey("Receipt", on_delete=models.CASCADE)
    project_name = models.CharField(max_length=100, blank=True, null=True,)
    project_type = models.CharField(max_length=100, blank=True, null=True,)
    directory = models.CharField(max_length=100, blank=True, null=True,)
    is_project_description_ready = models.BooleanField(default=False)
    description = models.TextField(blank=True, null=True,)
    process_type = models.CharField(max_length=100, blank=True, null=True,)
    design = models.CharField(max_length=100, blank=True, null=True,)
    count = models.CharField(blank=True, null=True,
                             max_length=100, default="0")
    operator = models.ManyToManyField(Employee, blank=True,)
    memo = models.CharField(max_length=500, blank=True, null=True,)
    fault_memo = models.CharField(max_length=1000, blank=True, null=True,)
    processing_status = models.CharField(
        max_length=500, blank=True, null=True, default="waiting")
    processing_machine = models.CharField(
        max_length=500, blank=True, null=True)
    post_processing_status = models.CharField(
        max_length=500, blank=True, null=True, default="waiting")
    design_status = models.CharField(
        max_length=500, blank=True, null=True, default="waiting")
    post_processing_text = models.CharField(
        max_length=500, blank=True, null=True)
    packaging_status = models.CharField(
        max_length=500, blank=True, null=True, default="waiting")
    packaging_text = models.CharField(max_length=500, blank=True, null=True)
    is_complete = models.BooleanField(default=False)
    is_ready_to_complete = models.BooleanField(default=False)
    project_due = models.CharField(blank=True, null=True, max_length=50)
    due = models.CharField(blank=True, null=True, max_length=50)
    is_deleted = models.BooleanField(default=False)
    expected_time = models.CharField(blank=True, null=True, max_length=50)
    real_manufactured_time = models.CharField(
        blank=True, null=True, max_length=50)

    def operator_employee_text(self):
        list = self.operator.all().values_list("name", flat=True)

        return ",".join(list)

    def project_due_text(self):
        return self.project_due.split(" ")[0].split("T")[0]

    def due_text(self):
        try:
            return self.due.split(" ")[0].split("T")[0]
        except:
            return ""

    def total_project_count(self):
        return self.receipt.project_set.all().count()
