from sre_parse import CATEGORIES
from rest_framework import serializers
from shop_management.models import *
from .models import *
from shop_user.models import *
from shop_admin.models import *
from menu.models import *
from order.models import *
from rest_framework.exceptions import ValidationError
from rest_framework.parsers import MultiPartParser, FormParser


class NoticeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notice
        fields = ("id", "title", "content", "created_at")


class FaqSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faq
        fields = '__all__'


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CustomPurchaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomPurchase
        fields = '__all__'

    def is_valid(self, raise_exception=False):
        ret = super(CustomPurchaseSerializer, self).is_valid(False)
        if self._errors:
            if raise_exception:
                raise ValidationError(self.errors)
        return ret


class ReceiptSerializer(serializers.ModelSerializer):
    class Meta:
        model = Receipt
        fields = ('__all__')

    def is_valid(self, raise_exception=False):
        ret = super(ReceiptSerializer, self).is_valid(False)
        if self._errors:
            print("Serialization failed due to {}".format(self.errors))
            if raise_exception:
                raise ValidationError(self.errors)
        return ret


class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = ('__all__')

    def is_valid(self, raise_exception=False):
        ret = super(ProjectSerializer, self).is_valid(False)
        if self._errors:
            if raise_exception:
                raise ValidationError(self.errors)
        return ret
