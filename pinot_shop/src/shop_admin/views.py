import re
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from copy import deepcopy
from django.shortcuts import render
from django.http import JsonResponse
from menu.models import *
from product.models import *
from order.models import *
from shop_management.models import *
from custom.models import *
from sample_image.models import *
import boto3
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from .serializers import *
from rest_framework.parsers import JSONParser
import datetime
from django.views.decorators.csrf import csrf_exempt
import math
from django.db.models import Q
from django.contrib.auth.decorators import login_required
# Create your views here.
import json
from django.core import serializers as df_serializers


# Create your views here.

@csrf_exempt
def admin_login(request):
    error = []
    message = []
    return render(request, "shop_admin/pages/login.html", {"error": error, "message": message})


@csrf_exempt
@login_required
def index(request):
    per = request.GET.get("per")
    if per == "" or per == None:
        per = 20
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    per = int(per)
    page = int(page)
    search = ""
    search = request.GET.get("search")
    if search == None:
        search = ""
    due = request.GET.get("due")
    project_list = ""
    today = datetime.datetime.now()
    tomorrow = today + datetime.timedelta(days=1)

    if due == "" or due == None or due == "total":
        due = "total"
        project_list = Project.objects.filter(is_deleted=False).filter(is_complete=False).filter(Q(description__icontains=search) | Q(project_name__icontains=search) | Q(
            receipt__client_company_name__icontains=search)).order_by("-receipt__is_urgent", "-receipt__receipt_date_time")[(page-1)*per:(page)*per]
    elif due == "today":
        project_list = Project.objects.filter(is_deleted=False).filter(is_complete=False).filter(project_due__contains=str(today.strftime("%Y-%m-%d"))).filter(Q(description__icontains=search) | Q(
            project_name__icontains=search) | Q(receipt__client_company_name__icontains=search)).order_by("-receipt__is_urgent", "-receipt__receipt_date_time")[(page-1)*per:(page)*per]
    elif due == "tomorrow":
        project_list = Project.objects.filter(is_deleted=False).filter(is_complete=False).filter(project_due__contains=str(tomorrow.strftime("%Y-%m-%d"))).filter(Q(description__icontains=search) | Q(
            project_name__icontains=search) | Q(receipt__client_company_name__icontains=search)).order_by("-receipt__is_urgent", "-receipt__receipt_date_time")[(page-1)*per:(page)*per]

    employee_list = Employee.objects.all()
    all_project_list = Project.objects.filter(
        is_deleted=False).filter(is_complete=False)
    project_type_list = []
    project_type_list.append(
        all_project_list.filter(project_type="실사출력").count())
    project_type_list.append(
        all_project_list.filter(project_type="사인물").count())
    project_type_list.append(
        all_project_list.filter(project_type="후레임간판").count())
    project_type_list.append(
        all_project_list.filter(project_type="채널간판").count())
    project_type_list.append(
        all_project_list.filter(project_type="지주간판").count())
    project_type_list.append(
        all_project_list.filter(project_type="포인트간판").count())
    project_type_list.append(
        all_project_list.filter(project_type="어닝").count())
    project_type_list.append(
        all_project_list.filter(project_type="광고자재").count())
    project_type_list.append(
        all_project_list.filter(project_type="기타").count())
    orderer_list = all_project_list.values_list(
        'receipt__client_company_name', flat=True).distinct()
    orderer_list = ",".join(list(set(orderer_list)))

    distinct_post_processing_text = ""
    distinct_post_processing = all_project_list.filter(
        post_processing_status="action").values_list('post_processing_text', flat=True).distinct()
    total_page = math.ceil(all_project_list.count()/per)
    filtering_delayed_project = all_project_list.filter(project_due__lt=str(
        datetime.datetime.now().strftime("%Y-%m-%d"))).exclude(is_complete=True).count()
    filter_due_today = all_project_list.filter(project_due__contains=str(
        datetime.datetime.now().strftime("%Y-%m-%d"))).exclude(is_complete=True).count()
    filter_due_tomorrow = all_project_list.filter(project_due__contains=str(
        tomorrow.strftime("%Y-%m-%d"))).exclude(is_complete=True).count()

    try:
        distinct_post_processing_text = ",".join(
            list(set(distinct_post_processing)))
    except:
        distinct_post_processing_text = ""

    return render(request, "shop_admin/pages/dashboard.html", {"filtering_delayed_project": filtering_delayed_project, "filter_due_today": filter_due_today, "filter_due_tomorrow": filter_due_tomorrow,
                                                               "project_list": project_list, "employee_list": employee_list, "project_type_list": project_type_list, "all_project_list_count": all_project_list.count(),
                                                               "orderer_list": orderer_list, "distinct_post_processing": distinct_post_processing_text, "page": page, "total_page": range(1, total_page+1), "per": per,
                                                               "due": due, "search": search
                                                               })


@csrf_exempt
@login_required
def complete(request):
    per = request.GET.get("per")
    if per == "" or per == None:
        per = 20
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    per = int(per)
    page = int(page)
    search = ""
    search = request.GET.get("search")
    recent = ""
    recent = request.GET.get("recent")
    start = request.GET.get("start")
    if start == None:
        start = str(datetime.datetime.now()).split(" ")[0]

    end = request.GET.get("end")
    if end == None:
        end = "2021-01-18"

    if search == None:
        search = ""
    project_list = Project.objects.filter(is_deleted=False).filter(Q(description__icontains=search) | Q(project_name__icontains=search)).filter(is_complete=True).filter(
        receipt__receipt_date_time__gte=end).filter(receipt__receipt_date_time__lte=start).order_by("-receipt__receipt_date_time")[(page-1)*per:(page)*per]
    employee_list = Employee.objects.all()
    all_project_list = Project.objects.filter(is_deleted=False).filter(Q(description__icontains=search) | Q(project_name__icontains=search)).filter(
        is_complete=True).filter(receipt__receipt_date_time__gte=end).filter(receipt__receipt_date_time__lte=start).order_by("-receipt__receipt_date_time")
    total_page = math.ceil(all_project_list.count()/per)

    return render(request, "shop_admin/pages/complete.html", {
        "project_list": project_list, "employee_list": employee_list,
        "page": page, "total_page": range(1, total_page+1), "per": per, "search": search, "recent": recent, "start": start, "end": end
    })


@csrf_exempt
@login_required
def user_list(request):
    if request.method == "GET":
        page = int(request.GET.get("page", 1))
        user_list = User.objects.all()
        if request.GET.get("q"):
            user_list = user_list.filter(
                Q(username__icontains=request.GET.get("q")) |
                Q(nickname__icontains=request.GET.get("q")) |
                Q(phone__icontains=request.GET.get("q"))
            )
        if request.GET.get("type"):
            if request.GET.get("type") == "true":
                user_list = user_list.filter(is_certified=True)
            else:
                user_list = user_list.filter(is_certified=False)
        date_from = request.GET.get("date_from")
        if date_from == "ALL":
            date_from = "2021-01-01"
        if date_from == None:
            date_from = "2021-01-01"
        date_to = request.GET.get("date_to")
        if date_to == None:
            date_to = str(datetime.datetime.now()).split(" ")[0]
        date_to = datetime.datetime.strptime(
            date_to, '%Y-%m-%d') + datetime.timedelta(days=1)
        user_list = user_list.filter(
            date_joined__gte=date_from).filter(date_joined__lte=date_to)
        total_page = math.ceil(user_list.count() / 20)
        user_list = user_list.order_by("-id")[(page - 1) * 20:page * 20]
        return render(request, "shop_admin/pages/user_list.html", {"user_list": user_list, "total_page": range(1, total_page+1), "page": page, "type": request.GET.get("type", ""), "q": request.GET.get("q", "")})
    elif request.method == "POST":
        user = User.objects.get(id=request.POST.get("id"))
        setattr(user, request.POST.get("data-target"),
                json.loads(request.POST.get("data-value")))
        print(user.is_certified)
        user.save()
        print(user)
        return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def product_list(request):
    if request.method == "POST":
        if request.POST.get("method") == "delete":
            ShopProduct.objects.get(id=request.POST.get("id")).delete()
            return JsonResponse({"result": "ok"})
        elif request.POST.get("method") == "COPY":
            sh = ShopProduct.objects.get(id=request.POST.get("id"))
            copy_obj = deepcopy(sh)
            copy_obj.id = None
            copy_obj.name = copy_obj.name+"_복사본"
            copy_obj.save()
            return JsonResponse({"result": "ok"})
    else:
        category = (request.GET.get("cat"))
        if category == "None":
            category = ""
        page = int(request.GET.get("page", 1))
        product_list = ShopProduct.objects.all()
        if category:
            cat_list = Category.objects.filter(
                parent_category_id=category).values_list("id", flat=True)
            print(cat_list)
            product_list = product_list.filter(category_id__in=cat_list)
        if request.GET.get("q"):
            product_list = product_list.filter(
                name__icontains=request.GET.get("q"))
        total_page = math.ceil(product_list.count() / 20)
        product_list = product_list.order_by(
            "-show_index")[(page-1)*20:page*20]
        category_list = Category.objects.filter(parent_category=None)
        return render(request, "shop_admin/pages/product_list.html", {"product_list": product_list, "total_page": range(1, total_page+1), "page": page,
                                                                      "category_list": category_list, "category": category, "q": request.GET.get("q", "")})


@csrf_exempt
@login_required
def change_product_index(request):

    if request.POST.get("direction") == "upward":
        index_target = ShopProduct.objects.get(
            show_index=request.POST.get("show_index"))
        origin_index = index_target.show_index
        to_target = ShopProduct.objects.filter(
            show_index__gt=request.POST.get("show_index")).order_by("show_index")[0]
        index_target.show_index = to_target.show_index
        index_target.save()
        to_target.show_index = origin_index
        to_target.save()
    elif request.POST.get("direction") == "downward":
        index_target = ShopProduct.objects.get(
            show_index=request.POST.get("show_index"))
        origin_index = index_target.show_index
        to_target = ShopProduct.objects.filter(
            show_index__lt=request.POST.get("show_index")).order_by("-show_index")[0]
        index_target.show_index = to_target.show_index
        index_target.save()
        to_target.show_index = origin_index
        to_target.save()
    else:
        print(request.POST)
        sh = ShopProduct.objects.get(id=request.POST.get("id"))
        sh.show_index = int(request.POST.get("show_index"))
        sh.save()
    return JsonResponse({"result": "ok"})


@login_required
def review(request):
    return render(request, "shop_admin/pages/review.html")


@login_required
def abuser_list(request):
    return render(request, "shop_admin/pages/abuser_list.html")


@csrf_exempt
@login_required
def add_user(request):
    return render(request, "shop_admin/pages/add_user.html")


@csrf_exempt
@login_required
def user_detail(request):
    return render(request, "shop_admin/pages/user_detail.html")


@csrf_exempt
@login_required
def order(request):
    if request.method == "GET":
        per = 30
        page = request.GET.get("page")
        if page == "" or page == None:
            page = 1
        page = int(page)
        order_list = Purchase.objects.all()
        if request.GET.get("q"):
            order_list = order_list.filter(
                Q(user__username__icontains=request.GET.get("q")) |
                Q(user__nickname__icontains=request.GET.get("q")) |
                Q(user__phone__icontains=request.GET.get("q"))
            )
        # if request.GET.get("type"):
        #     if request.GET.get("type") == "true":
        #         user_list = user_list.filter( is_certified=True )
        #     else:
        #         user_list = user_list.filter(is_certified=False)
        #

        date_from = request.GET.get("date_from")
        if date_from == "ALL":
            date_from = "2021-01-01"
        if date_from == None:
            date_from = "2021-01-01"
        date_to = request.GET.get("date_to")
        if date_to == None:
            date_to = str(datetime.datetime.now()).split(" ")[0]
        date_to = datetime.datetime.strptime(
            date_to, '%Y-%m-%d') + datetime.timedelta(days=1)

        order_list = order_list.exclude(purchase_status="complete").filter(created_at__gte=date_from).filter(created_at__lte=date_to).order_by(
            "-created_at")[(page-1)*per:page*per]
        total = Purchase.objects.exclude(purchase_status="complete").filter(
            created_at__gte=date_from).filter(created_at__lte=date_to).count()
        total_page = math.ceil(total/per)
        return render(request, "shop_admin/pages/order.html", {"order_list": order_list, "page": page, "total_page": range(1, total_page+1), "per": per, })


@csrf_exempt
@login_required
def ajax_order_status(request):
    pur = Purchase.objects.get(id=request.GET.get("id"))
    pur.purchase_status = request.GET.get("status")
    pur.save()

    return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def add_order(request):
    employee_list = Employee.objects.all()
    return render(request, "shop_admin/pages/add_order.html", {"employee_list": employee_list})


@csrf_exempt
@api_view(['GET', 'POST', "PUT", "DELETE"])
@permission_classes((permissions.AllowAny,))
@login_required
def ajax_order(request):
    if request.method == "GET":
        receipt_serializer = ReceiptSerializer(
            Receipt.objects.get(id=request.GET.get("id")))
        return Response(receipt_serializer.data)
    elif request.method == "POST":
        # request.data._mutable = True
        employee_list = request.data["receipt_data"]["receipt"]

        serializer = ReceiptSerializer(
            data=request.data["receipt_data"], partial=True)
        if serializer.is_valid():
            rec_result = serializer.save()

            employ_set = Employee.objects.filter(id__in=employee_list)
            Receipt.objects.get(
                id=rec_result.id).receipt_employee.set(employ_set)
            rec = Receipt.objects.get(id=rec_result.id)
            count = Receipt.objects.filter(receipt_date_time__contains=(
                rec_result.receipt_date_time.split("T")[0])).count()

            rec.receipt_number = rec_result.receipt_date_time.split(
                "T")[0].replace("-", "")[2:] + "-" + str(count)
            rec.save()
            for item in request.data["project_data"]:
                item["receipt"] = rec_result.id
                if "operator" not in item:
                    item["operator"] = []

                operator = Employee.objects.filter(
                    id__in=item["operator"]).values_list("id", flat=True)
                item["operator"] = operator
                project_serializer = ProjectSerializer(data=item, partial=True)
                if project_serializer.is_valid():
                    result = project_serializer.save()
                    num = rec.project_set.all().count()
                    project_result = project_serializer.save()
                    operator_set = Employee.objects.filter(
                        id__in=item["operator"])
                    Project.objects.get(
                        id=project_result.id).operator.set(operator_set)
                    prj = Project.objects.get(id=project_result.id)
                    prj.project_number = num
                    prj.save()
                else:
                    print(project_serializer.is_valid())

            return JsonResponse({"result": "ok"})
        else:

            # print(request.data)
            print(serializer)
    elif request.method == "PUT":
        id = request.data["receipt_data"]["receipt_id"]
        receipt = Receipt.objects.get(id=(id))
        serializer = ReceiptSerializer(
            receipt, data=request.data["receipt_data"], partial=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({"result": "ok"})
    elif request.method == "DELETE":
        Receipt.objects.get(id=request.POST.get("id")).delete()
        return JsonResponse({"result": "ok"})


@csrf_exempt
# @api_view(['GET','POST',"PUT","DELETE"])
# @permission_classes((permissions.AllowAny,))
@login_required
def ajax_project(request):
    body_unicode = request.body.decode('utf-8')
    received_json = json.loads(body_unicode)

    employee_list = received_json["project_data"]["operator"]
    if employee_list is None:
        employee_list = []
    operator = Employee.objects.filter(
        id__in=employee_list).values_list("id", flat=True)
    received_json["project_data"]["operator"] = operator

    project = Project.objects.get(id=received_json["project_data"]["id"])
    serializer = ProjectSerializer(
        project, data=received_json["project_data"], partial=True)
    if serializer.is_valid():
        serializer.save()
        return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def get_project(request):
    project_serializer = ProjectSerializer(
        Project.objects.get(id=request.GET.get("id")))
    return JsonResponse({"result": project_serializer.data})


@csrf_exempt
@login_required
def modify_project(request):

    value = request.POST.get("value")
    if request.method == "POST":
        if value == "true":
            value = True
        elif value == "false":
            value = False
        if request.POST.get("target") != "operator":

            project = Project.objects.get(id=request.POST.get("id"))
            setattr(project, request.POST.get("target"), value)
            project.save()

            return JsonResponse({"result": "ok"})
        else:
            project = Project.objects.get(id=(request.POST.get("id")))
            emp_id_list = Employee.objects.filter(
                id__in=value.split(",")).values_list("id", flat=True)
            project.operator.set(emp_id_list)
            project.save()
            return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def modify_receipt(request):

    receipt = Receipt.objects.get(id=request.POST.get("id"))
    value = request.POST.get("value")
    if value == "true":
        value = True
    elif value == "false":
        value = False
    # print(receipt.is_urgent)

    setattr(receipt, request.POST.get("target"), value)
    receipt.save()

    return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def delete_project(request):
    project = Project.objects.get(id=(request.POST.get("id")))
    project.is_deleted = True
    project.save()
    return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def delivery_list(request):
    if request.method == "POST":
        if request.POST.get("method") == "delete":
            DelieveryType.objects.get(id=request.POST.get("id")).delete()
            return JsonResponse({"result": "ok"})
        if request.POST.get("method") == "add_supplement":
            DelieveryType(
                text=request.POST.get("name"),
                price=int(request.POST.get("per_price", 0)),
            ).save()
            return JsonResponse({"result": "ok"})
    else:
        page = int(request.GET.get("page", 1))
        total_page = math.ceil(DelieveryType.objects.all().count() / 20)
        delivery_list = DelieveryType.objects.all()[(page-1)*20:page*20]
        return render(request, "shop_admin/pages/delivery_list.html", {"delivery_list": delivery_list, "total_page": range(1, total_page+1), "page": page, })


@csrf_exempt
@login_required
def confirm(request):
    return render(request, "shop_admin/pages/confirm.html")


@csrf_exempt
@login_required
def waiting(request):
    return render(request, "shop_admin/pages/waiting.html")


@csrf_exempt
def cancel(request):
    return render(request, "shop_admin/pages/cancel.html")


@csrf_exempt
@login_required
def refund(request):
    return render(request, "shop_admin/pages/refund.html")


@csrf_exempt
@login_required
def change(request):
    return render(request, "shop_admin/pages/change.html")


@csrf_exempt
@login_required
def deto(request):
    return render(request, "shop_admin/pages/deto.html")


@csrf_exempt
@login_required
def inquiry(request):
    return render(request, "shop_admin/pages/inquiry.html")


@csrf_exempt
@login_required
def specification_sheet(request):
    return render(request, "shop_admin/pages/specification_sheet.html")


@csrf_exempt
@login_required
def add_specification_sheet(request):
    return render(request, "shop_admin/pages/add_specification_sheet.html")


@csrf_exempt
@login_required
def faq_list(request):
    per = request.GET.get("per")
    if per == "" or per == None:
        per = 20
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    per = int(per)
    page = int(page)
    total_page = math.ceil(Faq.objects.all().count()/per)

    faq_list = Faq.objects.all().order_by(
        "-created_at")[(page-1)*per: page*per]

    category_list = list(Faq.CATEGORY)

    return render(request, "shop_admin/pages/faq_list.html", {"faq_list": faq_list, "category_list": category_list, "total_page": range(1, total_page+1)})


@csrf_exempt
@login_required
@api_view(['GET', 'POST', "PUT", "DELETE"])
@permission_classes((permissions.AllowAny,))
def ajax_faq(request, id='None'):
    if request.method == "GET":
        faq_serializer = FaqSerializer(Faq.objects.get(id=id))
        return Response(faq_serializer.data)
    elif request.method == "POST":
        serializer = FaqSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({"result": "ok"})
    elif request.method == "PUT":
        faq = Faq.objects.get(id=request.POST.get("id"))
        serializer = FaqSerializer(faq, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({"result": "ok"})
    elif request.method == "DELETE":
        Faq.objects.get(id=request.POST.get("id")).delete()
        return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def notice_list(request):
    notice = Notice.objects.all().order_by("-created_at")
    return render(request, "shop_admin/pages/notice_list.html", {"notice": notice})


@csrf_exempt
@login_required
@api_view(['GET', 'POST', "PUT", "DELETE"])
@permission_classes((permissions.AllowAny,))
def ajax_notice(request, id=1):
    if request.method == "GET":
        notice_serializer = NoticeSerializer(Notice.objects.get(id=id))
        return Response(notice_serializer.data)
    elif request.method == "POST":
        serializer = NoticeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({"result": "ok"})
    elif request.method == "PUT":
        notice = Notice.objects.get(id=request.POST.get("id"))
        serializer = NoticeSerializer(notice, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({"result": "ok"})
    elif request.method == "DELETE":
        Notice.objects.get(id=request.POST.get("id")).delete()
        return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def add_product(request):
    if request.method == "POST":
        product = ShopProduct()
        product.category_id = request.POST.get("category_list").split(",")[0]
        product.name = request.POST.get("name")
        product.description_summary = request.POST.get(
            "description_summary")  # todo: 추가할것
        product.showing_price = request.POST.get(
            "showing_price").replace(",", "")
        product.detail = request.POST.get("detail")
        product.is_hot = json.loads(request.POST.get("is_hot", False))
        product.is_new = json.loads(request.POST.get("is_new", False))
        product.is_show = json.loads(request.POST.get("is_show", False))
        product.is_main_display = json.loads(
            request.POST.get("is_main_display", False))
        if request.POST.get("group", None):
            product.group_id = request.POST.get("group", None)
            product.group_specific_name = ( request.POST.get("group_specific_name", ""))
            print( int(request.POST.get("group_specific_index")) )
            product.group_specific_index = int(request.POST.get("group_specific_index"))
            product.is_group_main_product = json.loads(
                request.POST.get("is_group_main_product", False))

        product.template_file = request.POST.get("template_file")
        product.discount_price = request.POST.get(
            "discount_price", "0").replace(",", "")
        product.option_json_data = request.POST.get("option_data")
        product.point = request.POST.get("point", "0").replace(",", "")
        product.need_to_file_upload = json.loads(
            request.POST.get("need_to_file_upload", False))
        product.additional_product_json_data = request.POST.get(
            "additional_product_json_data")
        product.count_convention = request.POST.get(
            "count_convention").replace(",", "")
        product.selling_range = request.POST.get(
            "selling_range").replace(",", "")
        product.direct_receive_price = request.POST.get(
            "direct_receive_price").replace(",", "")
        product.pre_parcel_price = request.POST.get(
            "pre_parcel_price").replace(",", "")
        product.after_parcel_price = request.POST.get(
            "after_parcel_price").replace(",", "")
        product.pre_quick_price = request.POST.get(
            "pre_quick_price").replace(",", "")
        product.after_quick_price = request.POST.get(
            "after_quick_price").replace(",", "")
        product.pre_carton_price = request.POST.get(
            "pre_carton_price").replace(",", "")
        product.after_carton_price = request.POST.get(
            "after_carton_price").replace(",", "")
        product.local_delivery_price = request.POST.get(
            "local_delivery_price").replace(",", "")
        product.delivery_duration = request.POST.get(
            "delivery_duration").replace(",", "")
        product.save()
        ShopProductImage.objects.filter(
            product_id=product.id).update(product_id=None)
        if request.POST.get("img_id_list") != "null":
            for (idx, val) in enumerate(request.POST.get("img_id_list").split(",")):
                img = ShopProductImage.objects.get(id=int(val))
                img.product_id = product.id
                img.index = idx
                img.save()
        return JsonResponse({"result": "ok"})
    else:
        return render(request, "shop_admin/pages/add_product.html", {})


@csrf_exempt
@login_required
def modify_product(request, id):
    if request.method == "POST":

        product = ShopProduct.objects.get(id=id)
        product.category_id = request.POST.get("category_list").split(",")[0]
        product.name = request.POST.get("name")
        product.description_summary = request.POST.get(
            "description_summary")  # todo: 추가할것
        product.showing_price = request.POST.get(
            "showing_price").replace(",", "")
        product.detail = request.POST.get("detail")
        product.is_hot = json.loads(request.POST.get("is_hot", False))
        product.is_new = json.loads(request.POST.get("is_new", False))
        product.is_show = json.loads(request.POST.get("is_show", False))
        product.is_main_display = json.loads(
            request.POST.get("is_main_display", False))
        if request.POST.get("group", None):
            product.group_id = request.POST.get("group", None)
            product.group_specific_name = (request.POST.get("group_specific_name", ""))
            print(int(request.POST.get("group_specific_index")))
            product.group_specific_index = int(request.POST.get("group_specific_index"))
            product.is_group_main_product = json.loads(
                request.POST.get("is_group_main_product", False))

        product.group_specific_name = request.POST.get("group_specific_name")
        product.is_group_main_product = json.loads(
            request.POST.get("is_group_main_product", False))

        product.template_file = request.POST.get("template_file")
        product.discount_price = request.POST.get(
            "discount_price", "0").replace(",", "")
        product.option_json_data = request.POST.get("option_data")
        product.point = request.POST.get("point", "0").replace(",", "")
        product.need_to_file_upload = json.loads(
            request.POST.get("need_to_file_upload", False))
        product.count_convention = request.POST.get(
            "count_convention").replace(",", "")
        product.selling_range = request.POST.get(
            "selling_range").replace(",", "")
        product.direct_receive_price = request.POST.get(
            "direct_receive_price").replace(",", "")
        product.pre_parcel_price = request.POST.get(
            "pre_parcel_price").replace(",", "")
        product.after_parcel_price = request.POST.get(
            "after_parcel_price").replace(",", "")
        product.pre_quick_price = request.POST.get(
            "pre_quick_price").replace(",", "")
        product.after_quick_price = request.POST.get(
            "after_quick_price").replace(",", "")
        product.pre_carton_price = request.POST.get(
            "pre_carton_price").replace(",", "")
        product.after_carton_price = request.POST.get(
            "after_carton_price").replace(",", "")
        product.local_delivery_price = request.POST.get(
            "local_delivery_price").replace(",", "")
        product.delivery_duration = request.POST.get(
            "delivery_duration").replace(",", "")
        product.additional_product_json_data = request.POST.get(
            "additional_product_json_data")
        product.save()

        ShopProductImage.objects.filter(
            product_id=product.id).update(product_id=None)
        print(request.POST.get("img_id_list"))
        if request.POST.get("img_id_list") != '':
            for (idx, val) in enumerate(request.POST.get("img_id_list").split(",")):
                img = ShopProductImage.objects.get(id=int(val))
                img.product_id = product.id
                img.index = idx
                img.save()

        return JsonResponse({"result": "ok"})
    else:
        product = ShopProduct.objects.get(id=id)
        if product.option_json_data != None:
            text_option = json.loads(product.option_json_data)["text_option"]
        else:
            text_option = []

        if product.option_json_data != None:
            select_option = json.loads(product.option_json_data)[
                "select_option"]
        else:
            select_option = []
        if product.option_json_data != None:
            design_option = json.loads(product.option_json_data)[
                "design_option"]
        else:
            design_option = []
        if product.option_json_data != None:
            textarea_option = json.loads(product.option_json_data)[
                "textarea_option"]
        else:
            textarea_option = []
        if product.option_json_data != None and json.loads(product.option_json_data).get("number_input_option"):
            number_input_option = json.loads(product.option_json_data)[
                "number_input_option"]
        else:
            number_input_option = []

        if product.option_json_data != None:
            if "template_file" in json.loads(product.option_json_data):
                template_file = json.loads(product.option_json_data)[
                    "template_file"]
            else:
                template_file = ""
        else:
            template_file = ""

        if product.additional_product_json_data != None:
            additional_product = json.loads(
                product.additional_product_json_data)
        else:
            additional_product = []
        group = ShopProductGroup.objects.all()
        return render(request, "shop_admin/pages/modify_product.html", {"product": product, "additional_product": additional_product, "text_option": text_option, "number_input_option": number_input_option,
                                                                        "select_option": select_option, "design_option": design_option, "textarea_option": textarea_option, "template_file": template_file, "group": group})


@csrf_exempt
@login_required
def get_category_data(request):
    main_category = Category.objects.filter(parent_category=None)
    data = []
    for item in main_category:
        sub_data = {}
        sub_data["name"] = item.name
        sub_data["id"] = item.id
        sub_data["is_public"] = "노출" if item.is_public else "비노출"
        sub_data["count"] = item.product_count
        sub_data["_children"] = []
        for item_depth_2 in item.category_set.all():
            depth_2_data = {}
            depth_2_data["name"] = item_depth_2.name
            depth_2_data["id"] = item_depth_2.id
            depth_2_data["is_public"] = "노출" if item_depth_2.is_public else "비노출"
            depth_2_data["count"] = item_depth_2.product_count
            sub_data["_children"].append(depth_2_data)
        data.append(sub_data)

    return JsonResponse({"data": data})


@csrf_exempt
@login_required
def upload_product_img(request):
    data = request.FILES.get("file")
    product_img = ShopProductImage()
    product_img.image = data
    product_img.save()
    return JsonResponse({"result": {"id": product_img.id, "url": product_img.image.url}})


@csrf_exempt
@login_required
def get_product_img(request, id):
    product_img = ShopProductImage.objects.get(id=id)
    product_img.image.url
    return JsonResponse({"result": {"url": product_img.image.url}})


@csrf_exempt
@login_required
def upload_product_template_file(request):
    if request.method == "POST":
        data = request.FILES.get("file")
        product_file = TempFile()
        product_file.temp_file = data
        product_file.save()
        return JsonResponse({"result": {"url": product_file.temp_file.url, "id": product_file.id}})
    else: 
        tp = TempFile.objects.get(id= request.GET.get("id"))
        return JsonResponse({"result": {"url": tp.temp_file.url}})

@csrf_exempt
@login_required
def dev_log(request):
    import logging
    logger = logging.getLogger('LoggerName')
    logger.info('The info message')
    logger.warning('The warning message')
    logger.error('The error message')
    logs = LogEntry.objects.exclude(
        change_message="No fields changed.").order_by('-action_time')[:20]
    logCount = LogEntry.objects.exclude(
        change_message="No fields changed.").order_by('-action_time')[:20].count()

    return render(request, "shop_admin/dev/log.html", {"logs": logs, "logCount": logCount})


@csrf_exempt
@login_required
def employee_list(reqeust):
    if reqeust.method == "POST":
        Employee.objects.get(id=reqeust.POST.get("id")).delete()
        return JsonResponse({"result": "ok"})

    employee_list = Employee.objects.all()
    return render(reqeust, "shop_admin/pages/employee.html", {"employee_list": employee_list})


@csrf_exempt
@login_required
def jumbotron_list(reqeust):
    if reqeust.method == "POST":
        if reqeust.POST.get("type") == "detail":
            return JsonResponse({"result": list(Jumbotron.objects.filter(id=reqeust.POST.get("id")).values())})
        if reqeust.POST.get("type") == "delete":
            Jumbotron.objects.get(id=int(reqeust.POST.get("id"))).delete()
            return JsonResponse({"result": list(Jumbotron.objects.filter(id=reqeust.POST.get("id")).values())})
        if reqeust.POST.get("type") == "add_modify":
            if reqeust.POST.get("id") != "undefined" and reqeust.POST.get("id") != "":
                jb = Jumbotron.objects.get(id=int(reqeust.POST.get("id")))
            else:
                jb = Jumbotron()
            jb.link = reqeust.POST.get("link")
            jb.name = reqeust.POST.get("name")
            jb.index = reqeust.POST.get("index")
            if reqeust.FILES.get("wide_img") != None:
                jb.wide_img = reqeust.FILES.get("wide_img")
            if reqeust.FILES.get("thumb_img") != None:
                jb.small_img = reqeust.FILES.get("thumb_img")
            jb.save()

    jumbotron_list = Jumbotron.objects.all().order_by("index")
    return render(reqeust, "shop_admin/pages/jumbotron_list.html", {"jumbotron_list": jumbotron_list})


@csrf_exempt
def jumbotron_click(request, id):
    JumbotronClickCount(
        jumbotron_id=id, ua=request.META['HTTP_USER_AGENT']).save()
    return JsonResponse({})


@csrf_exempt
def main_shortcut_click(request, id):
    MainShortcutClickCount(
        shortcut_id=id, ua=request.META['HTTP_USER_AGENT']).save()
    return JsonResponse({})


@csrf_exempt
@login_required
def banner_list(reqeust):
    if reqeust.method == "POST":
        if reqeust.POST.get("type") == "detail":
            return JsonResponse({"result": list(Banner.objects.filter(id=reqeust.POST.get("id")).values())})
        if reqeust.POST.get("type") == "delete":
            Banner.objects.get(id=int(reqeust.POST.get("id"))).delete()
            return JsonResponse({"result": list(Banner.objects.filter(id=reqeust.POST.get("id")).values())})
        if reqeust.POST.get("type") == "add_modify":
            if reqeust.POST.get("id") != "undefined" and reqeust.POST.get("id") != "":
                jb = Banner.objects.get(id=int(reqeust.POST.get("id")))
            else:
                jb = Banner()
            jb.link = reqeust.POST.get("link")
            jb.name = reqeust.POST.get("name")
            jb.start_dt = reqeust.POST.get("start_dt")
            jb.end_dt = reqeust.POST.get("end_dt")

            jb.background_color = reqeust.POST.get("background_color")

            if reqeust.POST.get("banner_type") == "common":
                jb.type = "common"
            elif reqeust.POST.get("banner_type") == "search":
                jb.type = "search"
            else:
                jb.type = "category"
                jb.category_id = reqeust.POST.get("banner_type")

            if reqeust.FILES.get("pc_image") != None:
                jb.pc_image = reqeust.FILES.get("pc_image")
            if reqeust.FILES.get("mobile_image") != None:
                jb.mobile_image = reqeust.FILES.get("mobile_image")
            jb.save()

    banner_list = Banner.objects.all()
    category_list = Category.objects.all()
    return render(reqeust, "shop_admin/pages/banner_list.html", {"banner_list": banner_list, "category_list": category_list})


@csrf_exempt
@login_required
def main_shortcut_list(reqeust):
    if reqeust.method == "POST":
        if reqeust.POST.get("type") == "detail":
            return JsonResponse({"result": list(MainShortcut.objects.filter(id=reqeust.POST.get("id")).values())})
        if reqeust.POST.get("type") == "delete":
            MainShortcut.objects.get(id=int(reqeust.POST.get("id"))).delete()
            return JsonResponse({"result": list(MainShortcut.objects.filter(id=reqeust.POST.get("id")).values())})
        if reqeust.POST.get("type") == "add_modify":
            if reqeust.POST.get("id") != "undefined" and reqeust.POST.get("id") != "":
                jb = MainShortcut.objects.get(id=int(reqeust.POST.get("id")))
            else:
                jb = MainShortcut()
            jb.link = reqeust.POST.get("link")
            jb.name = reqeust.POST.get("name")
            jb.index = reqeust.POST.get("index")
            if reqeust.POST.get("is_margin_top_exist") == "true":
                jb.is_margin_top_exist = True
            else:
                jb.is_margin_top_exist = False
            if reqeust.FILES.get("pc_image") != None:
                jb.pc_image = reqeust.FILES.get("pc_image")
            if reqeust.FILES.get("mobile_image") != None:
                jb.mobile_image = reqeust.FILES.get("mobile_image")
            jb.save()

    main_shortcut_list = MainShortcut.objects.all()

    return render(reqeust, "shop_admin/pages/main_shortcut_list.html", {"main_shortcut_list": main_shortcut_list})


@csrf_exempt
@login_required
@api_view(['GET', 'POST', "PUT", "DELETE"])
@permission_classes((permissions.AllowAny,))
def ajax_employee(request, id=1):
    if request.method == "GET":
        notice_serializer = EmployeeSerializer(Employee.objects.get(id=id))
        return Response(notice_serializer.data)
    elif request.method == "POST":
        serializer = EmployeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({"result": "ok"})
    elif request.method == "PUT":
        employee = EmployeeSerializer.objects.get(id=request.POST.get("id"))
        serializer = EmployeeSerializer(
            employee, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({"result": "ok"})
    elif request.method == "DELETE":
        Employee.objects.get(id=request.POST.get("id")).delete()
        return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
def access_log(request):
    per = 30
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    page = int(page)
    log_list = AccessLog.objects.all().order_by(
        "-accessed_at")[(page-1)*per: page*per]
    total = AccessLog.objects.all().count()
    total_page = math.ceil(total/per)
    return render(request, "shop_admin/dev/access_log.html", {"log_list": log_list, "page": page, "total_page": range(1, total_page+1), "per": per, })


@csrf_exempt
@login_required
def custom_purchase(request):
    per = 30
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    page = int(page)
    custom_purchase_list = Basket.objects.filter(
        is_private=True).order_by("-created_at")[(page-1)*per:(page)*per]
    total = Basket.objects.filter(
        is_private=True).count()
    total_page = math.ceil(total/per)
    user_list = User.objects.all()
    return render(request, "shop_admin/pages/custom_purchase.html", {"user_list": user_list, "custom_purchase_list": custom_purchase_list, "page": page, "total_page": range(1, total_page+1), "per": per, })


@csrf_exempt
@login_required
def custom_purchase_history(request):
    per = 30
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    page = int(page)
    custom_purchase_list = CustomPurchase.objects.filter(
        purchase_status="complete").order_by("-created_at")[(page-1)*per:(page)*per]
    total = CustomPurchase.objects.filter(purchase_status="complete").count()
    total_page = math.ceil(total/per)
    user_list = User.objects.all()
    return render(request, "shop_admin/pages/custom_purchase_history.html", {"user_list": user_list, "custom_purchase_list": custom_purchase_list, "page": page, "total_page": range(1, total_page+1), "per": per, })


@csrf_exempt
@login_required
@api_view(['GET', 'POST', "PUT", "DELETE"])
@permission_classes((permissions.AllowAny,))
def custom_purchase_detail(request, id=1):
    data = request
    if request.method == "GET":
        custom_purchase_serializer = CustomPurchaseSerializer(
            CustomPurchase.objects.get(id=id))
        return Response(custom_purchase_serializer.data)
    elif request.method == "POST":
        print((request.data))
        print(request.data.get("user"))
        ba = Basket()
        ba.is_private = True
        ba.product = request.data
        ba.user_id = request.data.get("user")
        ba.save()
        return JsonResponse({"result": "ok"})
    elif request.method == "PUT":
        custom_purchase = CustomPurchase.objects.get(id=request.POST.get("id"))
        serializer = CustomPurchaseSerializer(
            custom_purchase, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse({"result": "ok"})
    elif request.method == "DELETE":
        CustomPurchase.objects.get(id=request.POST.get("id")).delete()
        return JsonResponse({"result": "ok"})


@csrf_exempt
@login_required
@api_view(['GET', 'POST', "PUT", "DELETE"])
@permission_classes((permissions.AllowAny,))
def custom_inpuiry(request):
    custom_inquiry_list = CustomInquiry.objects.all().filter(
        is_deleted=False).order_by("-created_at").order_by("-id")
    return render(request, "shop_admin/pages/custom_inquiry.html", {"custom_inquiry_list": custom_inquiry_list})


@login_required
@api_view(['GET', 'POST', "PUT", "DELETE"])
@permission_classes((permissions.AllowAny,))
@csrf_exempt
def ajax_custom_inquiry(request, id):
    if request.method == "GET":
        ci = CustomInquiry.objects.filter(id=id).values()
        for item in ci:
            if CustomInquiry.objects.get(id=id).attached_file_0:
                item["am_url_0"] = CustomInquiry.objects.get(
                    id=id).attached_file_0.url
            if CustomInquiry.objects.get(id=id).attached_file_1:
                item["am_url_1"] = CustomInquiry.objects.get(
                    id=id).attached_file_1.url
            if CustomInquiry.objects.get(id=id).attached_file_2:
                item["am_url_2"] = CustomInquiry.objects.get(
                    id=id).attached_file_2.url

        reply = CustomInquiryAnswer.objects.filter(
            inquiry_id=ci[0]["id"]).values()
        for item in reply:
            if CustomInquiryAnswer.objects.get(id=item["id"]).attached_file_0:
                item["am_url_0"] = CustomInquiryAnswer.objects.get(
                    id=item["id"]).attached_file_0.url
            if CustomInquiryAnswer.objects.get(id=item["id"]).attached_file_1:
                item["am_url_1"] = CustomInquiryAnswer.objects.get(
                    id=item["id"]).attached_file_1.url
            if CustomInquiryAnswer.objects.get(id=item["id"]).attached_file_2:
                item["am_url_2"] = CustomInquiryAnswer.objects.get(
                    id=item["id"]).attached_file_2.url
        return JsonResponse({"ci": list(ci),
                             "reply": list(reply)
                             })
    else:
        pass


@csrf_exempt
@login_required
@api_view(['GET', 'POST', "PUT", "DELETE"])
@permission_classes((permissions.AllowAny,))
def ajax_custom_inquiry_answer(request, id):
    if request.method == "GET":
        ci = CustomInquiry.objects.filter(id=id).values()
        return JsonResponse({"ci": list(ci)})
    elif request.method == "POST":
        ca = CustomInquiryAnswer()
        ca.writer = request.user
        ca.inquiry_id = request.POST.get("inquiry_id")
        ca.name = request.POST.get("name")
        ca.content = request.POST.get("content")
        if request.FILES:
            ca.attached_file_0 = request.FILES.get("attached_file_0")
            ca.attached_file_1 = request.FILES.get("attached_file_1")
            ca.attached_file_2 = request.FILES.get("attached_file_2")
        ca.save()
        ci = CustomInquiry.objects.get(id=request.POST.get("inquiry_id"))
        if request.POST.get("status"):
            ci.status = request.POST.get("status")
        ci.save()
        return JsonResponse({"result": True})
    elif request.method == "DELETE":
        ci = CustomInquiryAnswer.objects.get(id=id)
        ci.delete()
        return JsonResponse({"result": True})
    elif request.method == "PUT":
        ci = CustomInquiryAnswer.objects.get(id=id)
        ci.content = request.POST.get("content")
        ci.save()
        return JsonResponse({"result": True})


@csrf_exempt
@login_required
def design_list(request):
    if request.method == "GET":
        design_list = PurchaseItem.objects.filter(
            product__icontains='"type": "design"').order_by('-created_at')
        return render(request, "shop_admin/pages/design_list.html", {"design_list": design_list})
    elif request.method == "POST":
        pi = PurchaseItem.objects.get(id=request.POST.get("id"))
        if (request.POST.get("attr") == "admin_design_file"):
            pi.admin_design_file = request.FILES.get("value")
        pi.save()
        return JsonResponse({"result": True})


@csrf_exempt
def category_list(request):
    print(request.method)
    if request.method == "GET":
        category_list = Category.objects.filter(
            parent_category_id=None).order_by("index")

        return render(request, "shop_admin/pages/category_list.html", {"category_list": category_list})
    elif request.method == "POST":
        if request.POST.get("pk") == "":  # 새로 생성
            Category(name=request.POST.get("name"),
                     parent_category_id=request.POST.get("parent_category_id"), index=request.POST.get("index", 0)).save()
            return JsonResponse({})
        else:
            ca = Category.objects.get(id=request.POST.get("pk"))
            ca.name = request.POST.get("name")
            ca.parent_category_id = request.POST.get("parent_category_id")
            ca.index = request.POST.get("index", 0)
            ca.save()
            return JsonResponse({})
    elif request.method == "DELETE":
        print((request.body).decode('utf-8').replace('%2C', ','))
        # print(request.body.get("list"))
        # print(request.POST.get("list").split(","))
        Category.objects.filter(
            id__in=(request.body).decode('utf-8').replace('%2C', ',').split("=")[1].split(",")).delete()
        return JsonResponse({})


@csrf_exempt
def category_detail(request, id):
    ca = Category.objects.filter(id=id).values()
    return JsonResponse({"result": list(ca)})


@ csrf_exempt
def ajax_design_custom(request, id=None):

    if request.method == "GET":
        data = DesignCustom.objects.filter(id=id).values()
        return JsonResponse({"result": data})
    elif request.method == "POST":
        dc = DesignCustom()
        if request.POST.get("title"):
            dc.title = request.POST.get("title")
        dc.purchase_item_id = request.POST.get("purchase_item_id")
        dc.attached_file_0 = request.FILES.get("attached_file_0")
        dc.attached_file_1 = request.FILES.get("attached_file_1")
        dc.attached_file_2 = request.FILES.get("attached_file_2")
        dc.content = request.POST.get("content")
        dc.name = request.POST.get("name")
        dc.save()
        pi = PurchaseItem.objects.get(id=request.POST.get("purchase_item_id"))
        pi.design_status = request.POST.get("design_status")
        pi.design_title = request.POST.get("design_title")
        pi.save()
        return JsonResponse({"result": "ok"})


@ csrf_exempt
def ajax_design_custom_list(request, id):

    design_custom_list = DesignCustom.objects.filter(
        purchase_item_id=id).order_by('created_at').values()
    for item in design_custom_list:
        if DesignCustom.objects.get(id=item["id"]).attached_file_0:
            item["am_url_0"] = DesignCustom.objects.get(
                id=item["id"]).attached_file_0.url
        if DesignCustom.objects.get(id=item["id"]).attached_file_1:
            item["am_url_1"] = DesignCustom.objects.get(
                id=item["id"]).attached_file_1.url
        if DesignCustom.objects.get(id=item["id"]).attached_file_2:
            item["am_url_2"] = DesignCustom.objects.get(
                id=item["id"]).attached_file_2.url
    return JsonResponse({"design_custom_list": list(design_custom_list)})


@csrf_exempt
def purchase_design_item(request, id):
    pi = PurchaseItem.objects.get(id=id)
    pi.design_status = request.POST.get("data")
    pi.confirmed_at = datetime.datetime.now()
    pi.save()

    return JsonResponse({"result": "ok"})


@csrf_exempt
def partner_inquiry_list(request):
    partner_inquiry_list = PartnerInquiry.objects.all()
    return render(request, "shop_admin/pages/partner_inquiry_list.html", {"partner_inquiry_list": partner_inquiry_list})


@csrf_exempt
def order_history(request):
    per = 30
    page = request.GET.get("page")
    if page == "" or page == None:
        page = 1
    page = int(page)

    order_list = Purchase.objects.filter(purchase_status="complete").order_by(
        "-created_at")[(page-1)*per:page*per]
    total = Purchase.objects.filter(purchase_status="complete").count()
    total_page = math.ceil(total/per)
    return render(request, "shop_admin/pages/order_history.html", {"order_list": order_list, "page": page, "total_page": range(1, total_page+1), "per": per, })


@csrf_exempt
def change_deliver(request, id):
    print(request.POST)
    pur = Purchase.objects.get(id=id)
    pur.deliver = request.POST.get("deliver")
    pur.deliver_num = request.POST.get("deliver_num")
    pur.save()
    return JsonResponse({"result": "ok"})


@csrf_exempt
def product_group(request, id=""):
    if id:
        if request.method == "POST":
            sh = ShopProductGroup.objects.get(id=id)
            sh.name = request.POST.get("name")
            sh.save()
            return JsonResponse({"result": "ok"})
        if request.method == "DELETE":

            ShopProductGroup.objects.get(id=id).delete()
            return JsonResponse({"result": "ok"})
    else:
        # 최초 등록
        if request.method == "POST":
            sh = ShopProductGroup()
            sh.name = request.POST.get("name")
            sh.save()
            return JsonResponse({"result": "ok"})
        else:
            group_list = ShopProductGroup.objects.all()
            return render(request,
                          "shop_admin/pages/product_group.html",
                          {"group_list": group_list})


@csrf_exempt
def sample_image_bulk_upload(request):
    if request.method == "GET":
        cat_0 = SampleImageCategory0.objects.all().order_by("index")
        cat_1 = SampleImageCategory1.objects.all().order_by("index")

        return render(request,
                      "shop_admin/pages/sample_image_bulk_upload.html", {
                          "cat_0": cat_0, "cat_1": cat_1}
                      )
    elif request.method == "POST":
        print(request.FILES)
        print(request.FILES.getlist("upload-file"))
        for i in request.FILES.getlist("upload-file"):
            print(i)
            si = SampleImage()
            si.category0_id = request.POST.get("cat_0")
            si.category1_id = request.POST.get("cat_1")
            si.image_sample_id = i.name.split(".")[0]
            si.image = i
            si.save()
        return JsonResponse({"result": "ok"})



@csrf_exempt
def image_list(request, id=""):
    if request.method == "GET":
        page=1
        if request.GET.get("page"):
            page = int(request.GET.get("page"))
        per_count = 20
        if request.GET.get("per_count"):
            per_count = int(request.GET.get("per_count"))
        q = ""
        if request.GET.get("q"):
            q = (request.GET.get("q"))
        category0 = request.GET.get("category0")
        category1 = request.GET.get("category1")
        category0_list = SampleImageCategory0.objects.all()
        category1_list = SampleImageCategory1.objects.filter(parent_category=None).order_by("index")
        category1_detail = request.GET.get("category1_detail")
        category1_detail_list = []
        q = Q()
        if category0 :
            q &= Q(category0=category0)
        if category1_detail :
            q &= Q(category1=category1_detail)
        elif category1 :
            id_list = SampleImageCategory1.objects.filter(parent_category=category1).values_list("id")
            q &= Q(category1__in=id_list)
        img_list = SampleImage.objects.filter(q).order_by("-id")[ (page-1)*per_count:page*per_count ]
        total = SampleImage.objects.filter(q).count()
        total_page = math.ceil(SampleImage.objects.filter(q).count() / per_count)+1
        return render(request, "shop_admin/pages/image_list.html",
                  {"category1":category1,"category0":category0,"category1_detail":category1_detail,"category1_detail_list":category1_detail_list,
                      "category0_list":category0_list,"category1_list":category1_list,
                    "img_list":img_list,"per_count":per_count,
                   "total":total,"total_page": range(1, total_page)})
    else:
        if request.method=="POST":
            if request.POST.get("method") == "update":
                print("asdf")
                si = SampleImage.objects.get(id=request.POST.get("id"))
                print(si)
                print(si.category0_id)
                print(request.POST.get("target"))
                print(request.POST.get("value")  )
                setattr( si, request.POST.get("target"), request.POST.get("value") )
                si.save()
                return JsonResponse({"result": "ok"})
            if request.POST.get("method") == "delete":
                SampleImage.objects.get(id=request.POST.get("id")).delete()
                return JsonResponse({"result": "ok"})